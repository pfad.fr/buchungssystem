// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

//go:build tools

package tools

import (
	_ "code.pfad.fr/devf"
	_ "github.com/kyleconroy/sqlc/pkg/cli"
	_ "src.agwa.name/snid"
)
