CREATE TABLE users(
  id TEXT NOT NULL PRIMARY KEY,
  email TEXT NOT NULL,
  name TEXT NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
  ,
  authorized_at TIMESTAMP DEFAULT NULL,
  role TEXT NOT NULL DEFAULT ''
);
CREATE TABLE houses(
  id TEXT NOT NULL PRIMARY KEY,
  name TEXT NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  max_person INTEGER NOT NULL DEFAULT 0,
  start_minute INTEGER NOT NULL DEFAULT 1020,
  end_minute INTEGER NOT NULL DEFAULT 900,
  reply_to TEXT NOT NULL DEFAULT '',
  days_for_user_confirmation INTEGER NOT NULL DEFAULT 7,
  months_for_public_booking INTEGER NOT NULL DEFAULT 13,
  public_booking_type TEXT NOT NULL DEFAULT 'overnight',
  CHECK(id <> ''),
  CHECK(name <> '')
);
CREATE TABLE house_user(
  user_id TEXT NOT NULL,
  house_id TEXT NOT NULL,
  role TEXT NOT NULL,
  PRIMARY KEY(house_id, user_id),
  FOREIGN KEY(house_id) REFERENCES houses(id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE INDEX houseuserindex ON house_user(user_id);
CREATE TABLE prices(
  id TEXT NOT NULL PRIMARY KEY,
  name TEXT NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  hidden_at TIMESTAMP DEFAULT NULL,
  CHECK(id <> ''),
  CHECK(name <> '')
);
CREATE TABLE charges(
  id TEXT NOT NULL PRIMARY KEY,
  name TEXT NOT NULL,
  description TEXT NOT NULL,
  house_id TEXT NOT NULL,
  price_id TEXT NOT NULL,
  unit TEXT NOT NULL,
  amount INTEGER NOT NULL,
  min_person_charged INTEGER NOT NULL,
  charged_over_person_count INTEGER NOT NULL,
  exempted_under_age INTEGER NOT NULL,
  optionality TEXT NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  deleted_at TIMESTAMP DEFAULT NULL,
  FOREIGN KEY(house_id) REFERENCES houses(id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(price_id) REFERENCES prices(id) ON UPDATE CASCADE ON DELETE CASCADE,
  CHECK(id <> ''),
  CHECK(name <> ''),
  CHECK(unit <> '')
);
CREATE TABLE bookable_days(
  date TEXT NOT NULL,
  house_id TEXT NOT NULL,
  price_id TEXT NOT NULL,
  restriction_reason TEXT NOT NULL,
  FOREIGN KEY(house_id) REFERENCES houses(id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(price_id) REFERENCES prices(id) ON UPDATE CASCADE ON DELETE CASCADE,
  -- add column booking_id
  PRIMARY KEY(date, house_id),
  CHECK(date > '2000-01-01'),
  CHECK(date < '2100-01-01')
  -- `date` DATE CHECK(date IS strftime('%Y-%m-%d', date)) not supported by pgsql
);
CREATE TABLE bookings(
  id INTEGER PRIMARY KEY,
  house_id TEXT NOT NULL,
  start_at TIMESTAMP NOT NULL,
  end_at TIMESTAMP NOT NULL,
  people_over JSON NOT NULL,
  contact TEXT NOT NULL,
  email TEXT NOT NULL,
  phone TEXT NOT NULL,
  comment TEXT NOT NULL,
  invoice JSON NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  admin_confirmed_at TIMESTAMP DEFAULT NULL,
  user_confirmed_at TIMESTAMP DEFAULT NULL,
  cancelled_at TIMESTAMP DEFAULT NULL,
  hashed_token TEXT NOT NULL DEFAULT '',
  landline TEXT NOT NULL DEFAULT '',
  FOREIGN KEY(house_id) REFERENCES houses(id) ON UPDATE CASCADE ON DELETE CASCADE,
  CHECK(start_at < end_at)
);
CREATE TRIGGER prevent_update_of_overlapping_bookings
BEFORE UPDATE OF house_id, cancelled_at, start_at, end_at ON bookings
WHEN NEW.cancelled_at IS NULL AND EXISTS (SELECT * FROM bookings WHERE house_id = NEW.house_id AND cancelled_at IS NULL AND id <> NEW.id AND (
    case when unixepoch(NEW.start_at) < unixepoch(start_at)
        then (unixepoch(NEW.end_at) > unixepoch(start_at))
        else (unixepoch(NEW.start_at) < unixepoch(end_at))
    end)
)
BEGIN
    SELECT RAISE(FAIL, "found an overlapping booking");
END;
CREATE TABLE emails(
  id TEXT NOT NULL PRIMARY KEY,
  booking_id INTEGER DEFAULT 0 NOT NULL,
  -- header_ prefix to prevent sql keyword conflicts
  header_from TEXT NOT NULL,
  header_to TEXT NOT NULL,
  header_subject TEXT NOT NULL,
  text_plain TEXT NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  sent_at TIMESTAMP DEFAULT NULL,
  fatal_error TEXT NOT NULL DEFAULT '',
  FOREIGN KEY(booking_id) REFERENCES bookings(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE booking_extra_charges(
  booking_id INTEGER NOT NULL,
  charge_id TEXT NOT NULL,
  percent INTEGER NOT NULL,
  FOREIGN KEY(booking_id) REFERENCES bookings(id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(charge_id) REFERENCES charges(id) ON UPDATE CASCADE ON DELETE CASCADE,
  PRIMARY KEY(booking_id, charge_id),
  CHECK(percent <> 0)
);
CREATE TABLE log_events(
  id INTEGER PRIMARY KEY,
  booking_id INTEGER NOT NULL,
  user_id TEXT,
  message TEXT NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  FOREIGN KEY(booking_id) REFERENCES bookings(id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TRIGGER prevent_insertion_of_overlapping_bookings
BEFORE INSERT ON bookings
WHEN NEW.cancelled_at IS NULL AND EXISTS (SELECT * FROM bookings WHERE house_id = NEW.house_id AND cancelled_at IS NULL AND (
    case when unixepoch(NEW.start_at) < unixepoch(start_at)
        then (unixepoch(NEW.end_at) > unixepoch(start_at))
        else (unixepoch(NEW.start_at) < unixepoch(end_at))
    end)
)
BEGIN
    SELECT RAISE(FAIL, "found an overlapping booking");
END;
CREATE TABLE extra_requests(
  id INTEGER PRIMARY KEY,
  weight INTEGER NOT NULL DEFAULT 5,
  house_id TEXT NOT NULL,
  description TEXT NOT NULL,
  pricing TEXT NOT NULL,
  FOREIGN KEY(house_id) REFERENCES houses(id) ON UPDATE CASCADE ON DELETE CASCADE,
  CHECK(description <> '')
);
