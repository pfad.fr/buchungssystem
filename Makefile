# SPDX-FileCopyrightText: 2022 Olivier Charvin
#
# SPDX-License-Identifier: CC0-1.0


ifneq (,$(wildcard ./.env))
    include .env
    export
endif

TAILWIND_VERSION=v3.2.7

tailwindcss-${TAILWIND_VERSION}:
	-rm tailwindcss-v*
	wget -O tailwindcss-${TAILWIND_VERSION} https://github.com/tailwindlabs/tailwindcss/releases/download/${TAILWIND_VERSION}/tailwindcss-linux-x64
	chmod +x tailwindcss-${TAILWIND_VERSION}

dev:
	$(MAKE) --no-print-directory -j3 dev-tailwind dev-devf dev-backend

dev-backend:
	pid="nonexisting" ; \
	(echo "initial generation" && inotifywait -m -e close_write --include "\.(go)$$" -r .) | \
	while read -r path; do \
		echo "$$path and $$(timeout 0.1 cat | wc -l) other changes" ; \
		pkill -TERM -P $$pid ; \
		wait $$pid ; \
		( \
			DEV_MODE=true BASE_URL=http://localhost:8000 go run ./cmd/server \
		) & \
		pid=$$! ; \
	done

dev-devf:
	inotifywait -m -e close_write --include "\.(is_running|gohtml|js|css)$$" -r . | \
	COLUMNS=$$(tput cols) go run code.pfad.fr/devf -addr localhost:8000 http://localhost:8989

dev-tailwind: tailwindcss-${TAILWIND_VERSION}
	./tailwindcss-${TAILWIND_VERSION} -w -i ./internal/css/style.css -c ./internal/css/tailwind.config.js -o ./internal/assets/style.css

build: build-tailwind build-server
	CGO_ENABLED=0 go build -o deploy/snid src.agwa.name/snid
	CGO_ENABLED=0 go build -o deploy/http-redirect ./cmd/http-redirect

build-tailwind: tailwindcss-${TAILWIND_VERSION}
	./tailwindcss-${TAILWIND_VERSION} --minify -i ./internal/css/style.css -c ./internal/css/tailwind.config.js -o ./internal/assets/style.css

build-server:
	# https://www.arp242.net/static-go.html
	go build -ldflags="-extldflags=-static" -tags osusergo,netgo,sqlite_omit_load_extension -o deploy/buchungsserver ./cmd/server

data/sqlc: go.sum
	go build -o data/sqlc github.com/kyleconroy/sqlc/cmd/sqlc

sqlc: data/sqlc
	find . -type f -regex '.*/zqlc?\..*\.go' -delete

	./data/sqlc generate

	# rename generated files to zql.*.go
	@find . -type f -name '*.sql.go' | while read f ; do \
		name=$$(basename $${f} .sql.go) ;\
		mv "$${f}" "$${f%/*}/zql.$${name}.go" ;\
	done

	# export schema
	echo -e "\n.schema --indent" | cat database/migrations/*.sql - | sqlite3  > schema.sql
	find . -type f -regex '.*/zqlc?\..*\.go' -exec git add {} +

reset-db:
	-rm data/db.sqlite
	touch data/db.sqlite
	EXIT_AFTER_MIGRATION=1 DEV_MODE=true go run ./cmd/server
	sqlite3 data/db.sqlite < database/seed/initial_data.sql

dev-reset-db:
	-rm data/db.sqlite
	touch data/db.sqlite
	cd internal/app && TESTS_ON_ACTUAL_DB="../../data/db.sqlite" go test -count=1 .
	$(MAKE) dev

db:
	sqlite3 data/db.sqlite

test:
	go test ./... -race -cover

test-actual-smtp:
	cd internal/app && TESTS_ON_ACTUAL_SMTP="1" SMTP_ADDR=127.0.0.1:1025 go test -count=1 .

upgrade:
	go get -u ./...

mailhog:
	mailhog -ui-bind-addr "127.0.0.1:8025" -api-bind-addr "127.0.0.1:8025" -smtp-bind-addr "127.0.0.1:1025"

lint:
	golangci-lint run ./...

deploy: build
	rsync -r --delete deploy/ dvmuf-buchung..hcloud:~/deploy
	ssh -t dvmuf-buchung..hcloud "cd deploy && sudo -S ./deploy.sh"

spdx-lint:
	reuse lint

import-backup:
	go run ./cmd/import
