// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package database

import (
	"database/sql"
	"embed"

	"github.com/pressly/goose/v3"
)

//go:embed migrations/*.sql
var embedMigrations embed.FS

func MissingMigrations(db *sql.DB, dialect string) (func() error, error) {
	goose.SetBaseFS(embedMigrations)
	// Note, make sure to update the dialect as required.
	if err := goose.SetDialect(dialect); err != nil {
		return nil, err
	}
	version, err := goose.EnsureDBVersion(db)
	if err != nil {
		return nil, err
	}
	m, err := goose.CollectMigrations("migrations", version, goose.MaxVersion)
	if err != nil && err != goose.ErrNoMigrationFiles { // https://github.com/pressly/goose/issues/599
		return nil, err
	}
	if len(m) == 0 {
		return nil, nil //nolint:nilnil
	}

	return func() error {
		return goose.Up(db, "migrations")
	}, nil
}
