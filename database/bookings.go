// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package database

func (c Charge) ChargedPeople(totalPeople int, ages map[int32]int) int {
	switch c.Unit {
	default:
		return 0
	case "per_stay":
		if c.ChargedOverPersonCount > 0 && totalPeople < int(c.ChargedOverPersonCount) {
			return 0
		}
		return 1
	case "per_person_night":
		n := totalPeople
		if c.ExemptedUnderAge > 0 {
			n = 0
			for age, number := range ages {
				if age >= c.ExemptedUnderAge {
					n += number
				}
			}
		}
		if n < int(c.MinPersonCharged) {
			n = int(c.MinPersonCharged)
		}
		return n
	}
}
