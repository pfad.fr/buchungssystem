-- USER

-- name: SettingUserIndex :many
SELECT * FROM users
ORDER BY name;

-- name: SettingUserFind :one
SELECT * FROM users
WHERE id = ?;

-- name: SettingUserUpdate :exec
UPDATE users SET authorized_at = ?, role = ?
WHERE id = ?;

-- HOUSE

-- name: SettingHouseCreate :exec
INSERT INTO houses (id,  name, max_person, reply_to)
VALUES (?, ?, ?, ?);

-- name: SettingHouseIndex :many
SELECT * FROM houses
ORDER BY id;

-- name: SettingBookableHouseIndex :many
SELECT houses.*, house_user.role FROM houses
JOIN house_user ON houses.id = house_user.house_id and house_user.user_id = ? and house_user.role IN (
    'booking_create',
    'booking_read',
    'booking_admin',
    'admin'
)
ORDER BY id;

-- name: SettingViewableHouseIndex :many
SELECT houses.*, house_user.role FROM houses
JOIN house_user ON houses.id = house_user.house_id and house_user.user_id = ? and house_user.role IN (
    'booking_read',
    'booking_admin',
    'admin'
)
ORDER BY id;

-- name: SettingHouseFind :one
SELECT * FROM houses
WHERE id = ?;

-- name: SettingHouseUpdate :exec
UPDATE houses SET name = ?, max_person = ?, reply_to = ?, start_minute = ?, end_minute = ?, public_booking_type = ?
WHERE id = ?;

-- HOUSE-USER

-- name: SettingHouseUserIndex :many
SELECT * FROM house_user;

-- name: SettingUserHouseRoles :many
SELECT houses.id as house_id, role FROM houses
LEFT JOIN house_user ON houses.id = house_user.house_id and house_user.user_id = ?;

-- name: SettingHouseUserUpdateRole :exec
UPDATE house_user SET role = ?
WHERE house_id = ? AND user_id = ?;

-- name: SettingHouseUserInsertRole :exec
INSERT INTO house_user (house_id, user_id, role)
VALUES (?,?,?);

-- name: SettingHouseUserRole :one
SELECT role FROM house_user
WHERE house_id = ? and user_id = ?;

-- PRICE

-- name: SettingPriceCreate :exec
INSERT INTO prices (id,  name)
VALUES (?, ?);

-- name: SettingPriceIndex :many
SELECT * FROM prices
ORDER BY created_at DESC;

-- name: SettingPriceIndexActive :many
SELECT * FROM prices
WHERE hidden_at IS NULL
ORDER BY created_at ASC;

-- name: SettingPriceFind :one
SELECT * FROM prices
WHERE id = ?;

-- name: SettingPriceUpdate :exec
UPDATE prices SET name = ?, hidden_at = ?
WHERE id = ?;

-- CHARGE

-- name: SettingChargeIndexPerPrice :many
SELECT * FROM charges
WHERE price_id = ?
ORDER BY optionality, name ASC;

-- name: SettingChargeIndexPerHousePrice :many
SELECT * FROM charges
WHERE price_id = ? AND house_id = ?
ORDER BY optionality, name ASC;

-- name: SettingChargeCreate :exec
INSERT INTO charges (
    id, house_id, price_id, name, description, unit, amount, min_person_charged, charged_over_person_count, exempted_under_age, optionality
)
VALUES (
    ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
);

-- name: SettingChargeUpdate :exec
UPDATE charges SET
    name = ?, description = ?, unit = ?, amount = ?, min_person_charged = ?, charged_over_person_count = ?, exempted_under_age = ?, optionality = ?
WHERE id = ?;

-- name: SettingChargeDelete :exec
DELETE FROM charges
WHERE id = ?;

-- BOOKABLE DAYS

-- name: SettingBookableDayIndex :many
SELECT * FROM bookable_days
WHERE house_id = ? AND date >= ?
ORDER BY date ASC;

-- SettingBookableDayCreate see settings.go

-- name: SettingBookableDayUpdate :exec
UPDATE bookable_days SET price_id = ?
WHERE house_id = ? AND date >= @start AND date < @end_date;

-- name: SettingPriceHasBooking :one
SELECT EXISTS(SELECT 1 FROM bookable_days
    JOIN bookings
    ON bookings.house_id = bookable_days.house_id
        AND date(bookable_days.date, '+1 day') >= bookings.start_at AND bookable_days.date < bookings.end_at
    WHERE bookable_days.house_id = ?
        AND bookable_days.price_id = ?
);

-- name: SettingRestDayIndex :many
SELECT * FROM bookable_days
WHERE house_id = ? AND date >= ? AND restriction_reason <> ''
ORDER BY date ASC;

-- name: SettingRestDayUpdate :exec
UPDATE bookable_days SET restriction_reason = ?
WHERE house_id = ? AND date >= @start AND date < @end_date;

-- name: SettingExtraRequestCreate :exec
INSERT INTO extra_requests (
    house_id, description, pricing, weight
)
VALUES (
    ?, ?, ?, ?
);

-- name: SettingExtraRequestUpdate :exec
UPDATE extra_requests SET
    description = ?, pricing = ?, weight = ?
WHERE id = ?;

-- name: SettingExtraRequestDelete :exec
DELETE FROM extra_requests
WHERE id = ?;
