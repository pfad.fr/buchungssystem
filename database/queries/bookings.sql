-- name: BookableDayIndex :many
SELECT * FROM bookable_days
WHERE house_id = ? AND date >= @start AND date < @end_date
ORDER BY date ASC;

-- name: BookableDayIndexUnlimited :many
SELECT * FROM bookable_days
WHERE house_id = ? AND date >= @start
ORDER BY date ASC;

-- name: BookingChargesIndex :many
SELECT * FROM charges
WHERE charges.house_id = @house_id AND price_id IN (
    SELECT price_id FROM bookable_days
    WHERE bookable_days.house_id = @house_id AND date >= @start AND date < @end_date
)
ORDER BY price_id, optionality, name ASC;

-- name: BookingPricesIndex :many
SELECT * FROM prices
WHERE id IN (
    SELECT price_id FROM bookable_days
    WHERE bookable_days.house_id = @house_id AND date >= @start AND date < @end_date
)
ORDER BY created_at DESC;

-- name: BookingCreate :one
INSERT INTO bookings (house_id,  start_at, end_at, people_over, contact, email, phone, landline, comment, invoice)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
RETURNING *;

-- name: BookingUpdateContact :exec
UPDATE bookings
SET contact = ?, email = ?, phone = ?, landline = ?, invoice = ?
WHERE id = ?;

-- name: BookingOverlapping :many
SELECT * FROM bookings
WHERE house_id = @house_id AND cancelled_at IS NULL AND
case when start_at > @start_at
    then ( start_at < @end_at)
    else ( end_at > @start_at)
END
ORDER BY start_at;

-- name: BookingEndingAfter :many
SELECT * FROM bookings
WHERE house_id = ? AND cancelled_at IS NULL AND end_at > ?
ORDER BY start_at;

-- name: BookingFind :one
SELECT * FROM bookings
WHERE id = ?;

-- name: BookingDelete :exec
UPDATE bookings
SET cancelled_at = CURRENT_TIMESTAMP
WHERE id = ? AND cancelled_at IS NULL;

-- name: BookingAdminConfirm :exec
UPDATE bookings
SET admin_confirmed_at = CURRENT_TIMESTAMP, hashed_token = ?
WHERE id = ? AND cancelled_at IS NULL AND admin_confirmed_at IS NULL;

-- name: BookingUserConfirm :exec
UPDATE bookings
SET user_confirmed_at = CURRENT_TIMESTAMP
WHERE id = ? AND cancelled_at IS NULL AND user_confirmed_at IS NULL AND admin_confirmed_at IS NOT NULL;

-- name: BookingIndex :many
SELECT bookings.*, COUNT() OVER() AS total_count
FROM bookings
WHERE bookings.house_id = @house_id
AND start_at < @start_at
AND end_at > @end_at
AND (CASE WHEN @search::string = ''
    THEN 1
    ELSE (
        contact LIKE '%'||(@search::string)||'%' COLLATE NOCASE
        OR email LIKE (@search::string) COLLATE NOCASE -- don't add % to email
        OR phone LIKE '%'||(@search::string)||'%' COLLATE NOCASE
        OR invoice LIKE '%'||(@search::string)||'%' COLLATE NOCASE
    )
END)
AND (
    (@include_unconfirmed::bool AND admin_confirmed_at IS NULL AND cancelled_at IS NULL)
    OR (@include_admin_confirmed::bool AND admin_confirmed_at IS NOT NULL AND user_confirmed_at IS NULL AND cancelled_at IS NULL)
    OR (@include_user_confirmed::bool AND user_confirmed_at IS NOT NULL AND cancelled_at IS NULL)
    OR (@include_cancelled::bool AND cancelled_at IS NOT NULL)
)
AND (CASE WHEN @price_id::string = ''
    THEN 1
    ELSE (
        EXISTS (SELECT 1 FROM bookable_days WHERE
            bookable_days.house_id = bookings.house_id
            AND bookable_days.price_id = @price_id::string
            AND date(bookable_days.date, '+1 day') >= bookings.start_at AND bookable_days.date < bookings.end_at
        )
    )
END)
AND (CASE WHEN @admin_charges::string = ''
    THEN 1
    ELSE (
        (SELECT COUNT(*) FROM booking_extra_charges WHERE
            booking_extra_charges.booking_id = bookings.id
            AND instr(@admin_charges::string, booking_extra_charges.charge_id) > 0
        ) >= @admin_charges_count::int --  a bit hacky, but does the job...
    )
END)
ORDER BY start_at ASC
LIMIT @_offset OFFSET @_limit -- switched in sqlc, no idea why...
;

-- name: BookingAdminChargesIndex :many
SELECT id, name FROM charges
WHERE charges.house_id = @house_id
AND optionality = 'admin'
AND EXISTS (SELECT 1 FROM prices WHERE price_id = charges.price_id AND hidden_at IS NULL)
ORDER BY name ASC;

-- name: BookingStatusCount :many
SELECT (CASE
    WHEN cancelled_at IS NOT NULL THEN 'cancelled'
    WHEN user_confirmed_at IS NOT NULL THEN 'user_confirmed'
    WHEN admin_confirmed_at IS NOT NULL THEN 'admin_confirmed'
    ELSE 'unconfirmed'
END) AS status, COUNT(*)
FROM bookings
WHERE house_id = ?
AND end_at > ?
GROUP BY status;

-- name: BookingUpcoming :many
SELECT *
FROM bookings
WHERE house_id = ?
AND cancelled_at IS NULL
AND end_at > ?
ORDER BY start_at ASC
LIMIT 2;

-- name: BookingUpdateDates :exec
UPDATE bookings
SET start_at = ?, end_at = ?
WHERE id = ?;

-- name: BookingUpdatePeopleAges :exec
UPDATE bookings
SET people_over = ?
WHERE id = ?;

-- name: BookingCommentUpdate :exec
UPDATE bookings
SET comment = ?
WHERE id = ?;

-- name: BookingCountForEmail :one
SELECT COUNT(*)
FROM bookings
WHERE email = ? and id <> ? and house_id = ?
AND cancelled_at IS NULL;

-- name: BookingExtraChargesIndex :many
SELECT * FROM booking_extra_charges
WHERE booking_id = ?;

-- name: BookingExtraChargesCreate :exec
INSERT INTO booking_extra_charges (booking_id,  charge_id, percent)
VALUES (?, ?, ?);

-- name: BookingExtraChargesUpdate :exec
UPDATE booking_extra_charges
SET percent = ?
WHERE booking_id = ? AND charge_id = ?;

-- name: BookingExtraChargesDelete :exec
DELETE FROM booking_extra_charges
WHERE booking_id = ? AND charge_id = ?;

-- name: BookingExpired :many
SELECT *
FROM bookings
WHERE admin_confirmed_at IS NOT NULL AND user_confirmed_at IS NULL AND cancelled_at IS NULL
AND (admin_confirmed_at < ? OR start_at < ?)
AND house_id = ?
ORDER BY start_at ASC;

-- name: BookingAnonymizeCancelled :many
UPDATE bookings
SET comment = 'Anonymisiert'
WHERE contact <> '' AND cancelled_at IS NOT NULL AND cancelled_at < ?
RETURNING *;

-- name: BookingAnonymizeOld :many
UPDATE bookings
SET comment = 'Anonymisiert'
WHERE contact <> '' AND end_at < ?
RETURNING *;

-- name: ExtraRequestsIndex :many
SELECT *
FROM extra_requests
WHERE house_id = ?
ORDER BY weight DESC, id ASC;
