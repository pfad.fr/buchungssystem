-- name: AuthLoad :one
SELECT * FROM users
WHERE id = ?;

-- name: AuthSave :exec
UPDATE users SET email = ?
WHERE id = ?;

-- name: AuthInsert :exec
INSERT INTO users (id, email, name)
VALUES (?, ?, ?);
