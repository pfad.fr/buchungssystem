-- name: EmailCreate :one
INSERT INTO emails (id, booking_id, header_from, header_to, header_subject, text_plain, fatal_error)
VALUES (?, ?, ?, ?, ?, ?, ?)
RETURNING *;

-- name: EmailPending :many
SELECT *
FROM emails
WHERE sent_at IS NULL AND fatal_error = '' AND created_at > ?;

-- name: EmailUpdate :exec
UPDATE emails
SET sent_at = ?, fatal_error = ?
WHERE id = ?;

-- name: EmailForBooking :many
SELECT *
FROM emails
WHERE booking_id = ?
ORDER BY created_at DESC, id DESC;

-- name: EmailAnonymize :exec
UPDATE emails
SET header_to = 'anonymous@anonymous.invalid', text_plain = ''
WHERE booking_id = ?;
