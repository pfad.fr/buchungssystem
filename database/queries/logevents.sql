-- name: LogEventCreate :exec
INSERT INTO log_events (booking_id,  user_id, message)
VALUES (?, ?, ?);

-- name: LogEventIndex :many
SELECT log_events.created_at, log_events.message, COALESCE(users.name, log_events.user_id, '')
FROM log_events
LEFT JOIN users ON users.id = log_events.user_id
WHERE booking_id = ?
ORDER BY log_events.created_at DESC, log_events.id DESC;

-- name: LogEventAnonymize :exec
UPDATE log_events
SET message = '· Kommentar wurde anonymisiert'
WHERE booking_id = ? AND user_id <> '';
