// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package database

import "context"

const settingBookableDayCreate = `
WITH RECURSIVE dates(date) AS (
	VALUES(:start)
	UNION ALL
	SELECT date(date, '+1 day')
	FROM dates
	WHERE date < date(:end, '-1 day')
) INSERT INTO bookable_days (
	date, house_id, price_id, restriction_reason
) SELECT date, :house_id, :price_id, '' from dates
WHERE date NOT IN (
	SELECT date from bookable_days WHERE house_id = :house_id AND :start <= date AND date < :end
);
`

func (q *Queries) SettingBookableDayCreate(ctx context.Context, arg SettingBookableDayUpdateParams) error {
	_, err := q.db.ExecContext(ctx, settingBookableDayCreate,
		arg.Start,
		arg.EndDate,
		arg.HouseID,
		arg.PriceID,
	)
	return err
}
