-- SPDX-FileCopyrightText: 2022 Olivier Charvin
--
-- SPDX-License-Identifier: EUPL-1.2+

INSERT INTO users (id, email, name, authorized_at, role) VALUES ('AAAAAAAAAAAAAAAAAAAAAF4Wi8Bbjj9_eCJwcTLPSEU', 'oc@atelier.dev', 'Olivier', CURRENT_TIMESTAMP , 'admin');

INSERT INTO houses (id, name, max_person, start_minute, end_minute, reply_to, public_booking_type) VALUES ('thalhaeusl', 'Thalhäusl', 44, 1020 , 600, 'haeuser@dpsg1300.de', 'overnight'), ('thalhaeusl-1og', 'Thalhäusl 1.OG', 10, 1020 , 600, 'haeuser@dpsg1300.de', 'overnight'), ('seegatterl', 'Seegatterl', 30, 1020 , 600, 'haeuser@dpsg1300.de', 'overnight'), ('bootshaus', 'Bootshaus Schliersee', 10, 420 , 1200, 'haeuser@dpsg1300.de', 'overday');

INSERT INTO prices (id, name) VALUES ('prix', 'Prix');

INSERT INTO extra_requests (description, pricing, house_id) VALUES ('Frühere Anreise (z. B. ab 9 Uhr)', '100 € Pauschale', 'thalhaeusl'), ('Tagesgäste ohne Übernachtung', '7.50 € pro Gast', 'thalhaeusl'), ('Leihbettwäsche', '8 € pro Garnitur und Woche', 'thalhaeusl'), ('Waschmaschinenbenutzung (nur nach Absprache möglich)', '4 €  pro Waschgang', 'thalhaeusl'), ('Reinigen von verunreinigter Bettwäsche', '8 €  pro Maschine', 'thalhaeusl'), ('Geschirrtuchverleih', '10 € (2-3 Tage)\n15 € (4-7 Tage)', 'thalhaeusl'), ('Lagerfeuerholz', 'je nach Verbrauch, mind. 10€', 'thalhaeusl'), ('Tischdecken, weiß, groß (für Speisesaal)', '3,50 €', 'thalhaeusl'), ('Deckenservietten (80 x 80 cm)', '2 €', 'thalhaeusl'), ('Bildungsveranstaltung (bitte Nachweis per E-Mail mitteilen)', 'Kurtaxe Befreiung', 'thalhaeusl');

INSERT INTO prices (id,  name) VALUES ('pr_sommer22', 'Sommer 2022'), ('pr_winter22', 'Winter 2022-23'), ('pr_sommer23', 'Sommer 2023'), ('pr_winter23', 'Winter 2023-24');

WITH RECURSIVE dates(date) AS (
		VALUES('2022-04-01')
		UNION ALL
		SELECT date(date, '+1 day')
		FROM dates
		WHERE date < date('2022-10-01', '-1 day')
	) INSERT INTO bookable_days (
		date, house_id, price_id, restriction_reason
	) SELECT date, 'seegatterl', 'pr_sommer22', '' from dates;

WITH RECURSIVE dates(date) AS (
		VALUES('2022-04-01')
		UNION ALL
		SELECT date(date, '+1 day')
		FROM dates
		WHERE date < date('2022-10-01', '-1 day')
	) INSERT INTO bookable_days (
		date, house_id, price_id, restriction_reason
	) SELECT date, 'thalhaeusl', 'pr_sommer22', '' from dates;

WITH RECURSIVE dates(date) AS (
		VALUES('2022-10-01')
		UNION ALL
		SELECT date(date, '+1 day')
		FROM dates
		WHERE date < date('2023-04-01', '-1 day')
	) INSERT INTO bookable_days (
		date, house_id, price_id, restriction_reason
	) SELECT date, 'seegatterl', 'pr_winter22', '' from dates;
WITH RECURSIVE dates(date) AS (
		VALUES('2022-10-01')
		UNION ALL
		SELECT date(date, '+1 day')
		FROM dates
		WHERE date < date('2023-04-01', '-1 day')
	) INSERT INTO bookable_days (
		date, house_id, price_id, restriction_reason
	) SELECT date, 'thalhaeusl', 'pr_winter22', '' from dates;

WITH RECURSIVE dates(date) AS (
		VALUES('2023-04-01')
		UNION ALL
		SELECT date(date, '+1 day')
		FROM dates
		WHERE date < date('2023-10-01', '-1 day')
	) INSERT INTO bookable_days (
		date, house_id, price_id, restriction_reason
	) SELECT date, 'seegatterl', 'pr_sommer23', '' from dates;
WITH RECURSIVE dates(date) AS (
		VALUES('2023-04-01')
		UNION ALL
		SELECT date(date, '+1 day')
		FROM dates
		WHERE date < date('2023-10-01', '-1 day')
	) INSERT INTO bookable_days (
		date, house_id, price_id, restriction_reason
	) SELECT date, 'thalhaeusl', 'pr_sommer23', '' from dates;

WITH RECURSIVE dates(date) AS (
		VALUES('2023-10-01')
		UNION ALL
		SELECT date(date, '+1 day')
		FROM dates
		WHERE date < date('2024-04-01', '-1 day')
	) INSERT INTO bookable_days (
		date, house_id, price_id, restriction_reason
	) SELECT date, 'seegatterl', 'pr_winter23', '' from dates;
WITH RECURSIVE dates(date) AS (
		VALUES('2023-10-01')
		UNION ALL
		SELECT date(date, '+1 day')
		FROM dates
		WHERE date < date('2024-04-01', '-1 day')
	) INSERT INTO bookable_days (
		date, house_id, price_id, restriction_reason
	) SELECT date, 'thalhaeusl', 'pr_winter23', '' from dates;

delete from charges;

INSERT INTO charges (id, house_id, price_id, name, description, unit, amount, min_person_charged, charged_over_person_count, exempted_under_age, optionality) VALUES
	('ch_see_sommer22_ubernachtung', 'seegatterl', 'pr_sommer22', 'Übernachtung', '', 'per_person_night', 1200, 18, 0, 3, ''),
	('ch_see_sommer22_kurtaxe', 'seegatterl', 'pr_sommer22', 'Kurtaxe', 'Kurtaxe der Gemeinde Reit im Winkl', 'per_person_night', 60, 0, 0, 7, ''),
	('ch_see_sommer22_extra_heizung', 'seegatterl', 'pr_sommer22', 'Extra Heizung', '', 'per_person_night', 75, 18, 0, 3, 'admin'),
	('ch_see_sommer22_extra_kurtaxe', 'seegatterl', 'pr_sommer22', 'Extra Kurtaxe', '', 'per_stay', 60, 0, 0, 7, 'admin'),
	('ch_see_sommer22_vorzeitige_anreise', 'seegatterl', 'pr_sommer22', 'Vorzeitige Anreise', '', 'per_stay', 10800, 0, 0, 7, 'admin'),
	('ch_see_sommer22_rabatt_kurtaxe', 'seegatterl', 'pr_sommer22', 'Kurtaxe Befreiung (Bildungsveranstaltung)', '', 'per_person_night', -60, 0, 0, 7, 'admin'),
	('ch_see_sommer22_rabatt_dpsg', 'seegatterl', 'pr_sommer22', 'DPSG Rabatt', '', 'per_person_night', -200, 18, 0, 3, 'admin'),
	('ch_see_sommer22_rabatt_sonder', 'seegatterl', 'pr_sommer22', 'Sonder Rabatt', '', 'per_stay', -100, 18, 0, 3, 'admin'),

	('ch_see_winter22_ubernachtung', 'seegatterl', 'pr_winter22', 'Übernachtung', '', 'per_person_night', 1275, 18, 0, 3, ''),
	('ch_see_winter22_kurtaxe', 'seegatterl', 'pr_winter22', 'Kurtaxe', 'Kurtaxe der Gemeinde Reit im Winkl', 'per_person_night', 60, 0, 0, 7, ''),
	('ch_see_winter22_extra_kurtaxe', 'seegatterl', 'pr_winter22', 'Extra Kurtaxe', '', 'per_stay', 60, 0, 0, 7, 'admin'),
	('ch_see_winter22_rabatt_kurtaxe', 'seegatterl', 'pr_winter22', 'Kurtaxe Befreiung (Bildungsveranstaltung)', '', 'per_person_night', -60, 0, 0, 7, 'admin'),
	('ch_see_winter22_rabatt_dpsg', 'seegatterl', 'pr_winter22', 'DPSG Rabatt', '', 'per_person_night', -200, 18, 0, 3, 'admin'),

	('ch_see_sommer23_ubernachtung', 'seegatterl', 'pr_sommer23', 'Übernachtung', '', 'per_person_night', 1500, 18, 0, 3, ''),
	('ch_see_sommer23_kurtaxe', 'seegatterl', 'pr_sommer23', 'Kurtaxe', 'Kurtaxe der Gemeinde Reit im Winkl', 'per_person_night', 60, 0, 0, 7, ''),
	('ch_see_sommer23_rabatt_ubernachtung_3', 'seegatterl', 'pr_sommer23', 'Übernachtung Rabatt', '', 'per_person_night', -300, 18, 0, 3, 'admin'),
	('ch_see_sommer23_rabatt_kurtaxe', 'seegatterl', 'pr_sommer23', 'Kurtaxe Befreiung (Bildungsveranstaltung)', '', 'per_person_night', -60, 0, 0, 7, 'admin'),
	('ch_see_sommer23_rabatt_dpsg', 'seegatterl', 'pr_sommer23', 'DPSG Rabatt', '', 'per_person_night', -200, 18, 0, 3, 'admin'),

	('ch_see_winter23_ubernachtung', 'seegatterl', 'pr_winter23', 'Übernachtung', '', 'per_person_night', 1600, 18, 0, 3, ''),
	('ch_see_winter23_kurtaxe', 'seegatterl', 'pr_winter23', 'Kurtaxe', 'Kurtaxe der Gemeinde Reit im Winkl', 'per_person_night', 60, 0, 0, 7, ''),
	('ch_see_winter23_rabatt_kurtaxe', 'seegatterl', 'pr_winter23', 'Kurtaxe Befreiung (Bildungsveranstaltung)', '', 'per_person_night', -60, 0, 0, 7, 'admin'),
	('ch_see_winter23_rabatt_dpsg', 'seegatterl', 'pr_winter23', 'DPSG Rabatt', '', 'per_person_night', -200, 18, 0, 3, 'admin');

INSERT INTO charges (id, house_id, price_id, name, description, unit, amount, min_person_charged, charged_over_person_count, exempted_under_age, optionality) VALUES
	('ch_thal_sommer22_ubernachtung', 'thalhaeusl', 'pr_sommer22', 'Übernachtung', '', 'per_person_night', 1300, 20, 0, 3, ''),
	('ch_thal_sommer22_kurtaxe', 'thalhaeusl', 'pr_sommer22', 'Kurtaxe', 'Kurtaxe der Gemeinde Fischbachau', 'per_person_night', 100, 0, 0, 7, ''),
	('ch_thal_sommer22_reinigung', 'thalhaeusl', 'pr_sommer22', 'Reinigungspauschale (1.OG)', '', 'per_stay', 8000, 0, 0, 3, ''),
	('ch_thal_sommer22_reinigung_2', 'thalhaeusl', 'pr_sommer22', 'Reinigungspauschale (2.OG)', '', 'per_stay', 8000, 0, 25, 3, ''),
	('ch_thal_sommer22_rabatt_kurtaxe', 'thalhaeusl', 'pr_sommer22', 'Kurtaxe Befreiung (Bildungsveranstaltung)', '', 'per_person_night', -100, 0, 0, 7, 'admin'),
	('ch_thal_sommer22_rabatt_dpsg', 'thalhaeusl', 'pr_sommer22', 'DPSG Rabatt', '', 'per_person_night', -400, 20, 0, 3, 'admin'),

	('ch_thal_winter22_ubernachtung', 'thalhaeusl', 'pr_winter22', 'Übernachtung', '', 'per_person_night', 1400, 20, 0, 3, ''),
	('ch_thal_winter22_kurtaxe', 'thalhaeusl', 'pr_winter22', 'Kurtaxe', 'Kurtaxe der Gemeinde Fischbachau', 'per_person_night', 100, 0, 0, 7, ''),
	('ch_thal_winter22_reinigung', 'thalhaeusl', 'pr_winter22', 'Reinigungspauschale (1.OG)', '', 'per_stay', 8000, 0, 0, 3, ''),
	('ch_thal_winter22_reinigung_2', 'thalhaeusl', 'pr_winter22', 'Reinigungspauschale (2.OG)', '', 'per_stay', 8000, 0, 25, 3, ''),
	('ch_thal_winter22_reinigung_extra', 'thalhaeusl', 'pr_winter22', 'Reinigungspauschale (2.OG)', '', 'per_stay', 8000, 0, 0, 3, 'admin'),
	('ch_thal_winter22_besondere_an_abreise', 'thalhaeusl', 'pr_winter22', 'Besondere An-/Abreisezeit', '', 'per_stay', 10000, 0, 0, 7, 'admin'),
	('ch_thal_winter22_geschirr', 'thalhaeusl', 'pr_winter22', 'Geschirrtuchverleih', '', 'per_stay', 1000, 0, 0, 3, 'admin'),
	('ch_thal_winter22_rabatt_kurtaxe', 'thalhaeusl', 'pr_winter22', 'Kurtaxe Befreiung (Bildungsveranstaltung)', '', 'per_person_night', -100, 0, 0, 7, 'admin'),
	('ch_thal_winter22_rabatt_dpsg', 'thalhaeusl', 'pr_winter22', 'DPSG Rabatt', '', 'per_person_night', -400, 20, 0, 3, 'admin'),

	('ch_thal_sommer23_ubernachtung', 'thalhaeusl', 'pr_sommer23', 'Übernachtung', '', 'per_person_night', 1500, 20, 0, 3, ''),
	('ch_thal_sommer23_kurtaxe', 'thalhaeusl', 'pr_sommer23', 'Kurtaxe', 'Kurtaxe der Gemeinde Fischbachau', 'per_person_night', 100, 0, 0, 7, ''),
	('ch_thal_sommer23_reinigung', 'thalhaeusl', 'pr_sommer23', 'Reinigungspauschale (1.OG)', '', 'per_stay', 9000, 0, 0, 3, ''),
	('ch_thal_sommer23_reinigung_2', 'thalhaeusl', 'pr_sommer23', 'Reinigungspauschale (2.OG)', '', 'per_stay', 9000, 0, 25, 3, ''),
	('ch_thal_sommer23_reinigung_extra', 'thalhaeusl', 'pr_sommer23', 'Reinigungspauschale (2.OG)', '', 'per_stay', 9000, 0, 0, 3, 'admin'),
	('ch_thal_sommer23_heizung_extra', 'thalhaeusl', 'pr_sommer23', 'Extra-Heizung', '', 'per_stay', 100, 0, 0, 3, 'admin'),
	('ch_thal_sommer23_geschirr', 'thalhaeusl', 'pr_sommer23', 'Geschirrtuchverleih', '', 'per_stay', 1000, 0, 0, 3, 'admin'),
	('ch_thal_sommer23_rabatt_kurtaxe', 'thalhaeusl', 'pr_sommer23', 'Kurtaxe Befreiung (Bildungsveranstaltung)', '', 'per_person_night', -100, 0, 0, 7, 'admin'),
	('ch_thal_sommer23_rabatt_dpsg', 'thalhaeusl', 'pr_sommer23', 'DPSG Rabatt', '', 'per_person_night', -400, 20, 0, 3, 'admin'),
	('ch_thal_sommer23_rabatt_ubernachtung_1', 'thalhaeusl', 'pr_sommer23', 'Übernachtung Rabatt', '', 'per_person_night', -100, 20, 0, 3, 'admin'),
	('ch_thal_sommer23_rabatt_reinigung', 'thalhaeusl', 'pr_sommer23', 'Reinigung Rabatt', '', 'per_stay', -1000, 0, 0, 3, 'admin'),

	('ch_thal_winter23_ubernachtung', 'thalhaeusl', 'pr_winter23', 'Übernachtung', '', 'per_person_night', 1600, 20, 0, 3, ''),
	('ch_thal_winter23_kurtaxe', 'thalhaeusl', 'pr_winter23', 'Kurtaxe', 'Kurtaxe der Gemeinde Fischbachau', 'per_person_night', 100, 0, 0, 7, ''),
	('ch_thal_winter23_reinigung', 'thalhaeusl', 'pr_winter23', 'Reinigungspauschale (1.OG)', '', 'per_stay', 9000, 0, 0, 3, ''),
	('ch_thal_winter23_reinigung_2', 'thalhaeusl', 'pr_winter23', 'Reinigungspauschale (2.OG)', '', 'per_stay', 9000, 0, 25, 3, ''),
	('ch_thal_winter23_reinigung_extra', 'thalhaeusl', 'pr_winter23', 'Reinigungspauschale (2.OG)', '', 'per_stay', 9000, 0, 0, 3, 'admin'),
	('ch_thal_winter23_rabatt_kurtaxe', 'thalhaeusl', 'pr_winter23', 'Kurtaxe Befreiung (Bildungsveranstaltung)', '', 'per_person_night', -100, 0, 0, 7, 'admin'),
	('ch_thal_winter23_rabatt_dpsg', 'thalhaeusl', 'pr_winter23', 'DPSG Rabatt', '', 'per_person_night', -500, 20, 0, 3, 'admin');