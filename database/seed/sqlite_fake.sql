-- SPDX-FileCopyrightText: 2022 Olivier Charvin
--
-- SPDX-License-Identifier: CC0-1.0

-- to use sqlite.date function
CREATE OR REPLACE FUNCTION public.date(a integer, b integer) RETURNS integer
    AS 'select $1 + $2;'
    LANGUAGE SQL
    IMMUTABLE
    RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION public.instr(a str, b str) RETURNS str
    AS 'select $1 + $2;'
    LANGUAGE SQL
    IMMUTABLE
    RETURNS NULL ON NULL INPUT;
