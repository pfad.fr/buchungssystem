-- +goose Up
CREATE TABLE prices (
    id TEXT NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    hidden_at TIMESTAMP DEFAULT NULL,
    CHECK(id <> ''),
    CHECK(name <> '')
);
