-- +goose Up
CREATE TABLE booking_extra_charges (
    booking_id INTEGER NOT NULL,
    charge_id TEXT NOT NULL,

    percent INTEGER NOT NULL,

    FOREIGN KEY(booking_id) REFERENCES bookings(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY(charge_id) REFERENCES charges(id) ON UPDATE CASCADE ON DELETE CASCADE,

    PRIMARY KEY (booking_id, charge_id),
    CHECK(percent <> 0)
);
