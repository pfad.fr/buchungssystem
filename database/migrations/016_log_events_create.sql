-- +goose Up
CREATE TABLE log_events (
    id INTEGER PRIMARY KEY,

    booking_id INTEGER NOT NULL,
    user_id TEXT,

    message TEXT NOT NULL,

    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,

    FOREIGN KEY(booking_id) REFERENCES bookings(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE
);

ALTER TABLE bookings DROP COLUMN cancellation_reason;
