-- +goose Up
CREATE TABLE extra_requests (
    id INTEGER PRIMARY KEY,
    weight INTEGER NOT NULL DEFAULT 5,

    house_id TEXT NOT NULL,

    description TEXT NOT NULL,
    pricing TEXT NOT NULL,

    FOREIGN KEY(house_id) REFERENCES houses(id) ON UPDATE CASCADE ON DELETE CASCADE,

    CHECK(description <> '')
);
