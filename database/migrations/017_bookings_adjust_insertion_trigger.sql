-- +goose Up

-- migrate:down
-- above is a trick to disable sqlc on the trigger below

-- +goose StatementBegin
DROP TRIGGER IF EXISTS prevent_insertion_of_overlapping_bookings;
CREATE TRIGGER prevent_insertion_of_overlapping_bookings
BEFORE INSERT ON bookings
WHEN NEW.cancelled_at IS NULL AND EXISTS (SELECT * FROM bookings WHERE house_id = NEW.house_id AND cancelled_at IS NULL AND (
    case when unixepoch(NEW.start_at) < unixepoch(start_at)
        then (unixepoch(NEW.end_at) > unixepoch(start_at))
        else (unixepoch(NEW.start_at) < unixepoch(end_at))
    end)
)
BEGIN
    SELECT RAISE(FAIL, "found an overlapping booking");
END;
-- +goose StatementEnd