-- +goose Up
CREATE TABLE bookable_days (
    date TEXT NOT NULL,

    house_id TEXT NOT NULL,
    price_id TEXT NOT NULL,

    restriction_reason TEXT NOT NULL,

    FOREIGN KEY(house_id) REFERENCES houses(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY(price_id) REFERENCES prices(id) ON UPDATE CASCADE ON DELETE CASCADE,

    -- add column booking_id
    PRIMARY KEY (date, house_id),

    CHECK(date > '2000-01-01'),
    CHECK(date < '2100-01-01')
    -- `date` DATE CHECK(date IS strftime('%Y-%m-%d', date)) not supported by pgsql
);
