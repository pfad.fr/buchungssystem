-- +goose Up
CREATE TABLE remember_tokens (
    token TEXT NOT NULL PRIMARY KEY,
    user_id TEXT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);
