-- +goose Up
CREATE TABLE bookings (
    id INTEGER PRIMARY KEY,

    house_id TEXT NOT NULL,

    start_at TIMESTAMP NOT NULL,
    end_at TIMESTAMP NOT NULL,

    people_over JSON NOT NULL,

    contact TEXT NOT NULL,
    email TEXT NOT NULL,
    phone TEXT NOT NULL,
    comment TEXT NOT NULL,

    invoice JSON NOT NULL,

    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    admin_confirmed_at TIMESTAMP DEFAULT NULL,
    user_confirmed_at TIMESTAMP DEFAULT NULL,
    cancelled_at TIMESTAMP DEFAULT NULL,
    cancellation_reason TEXT NOT NULL DEFAULT '',

    FOREIGN KEY(house_id) REFERENCES houses(id) ON UPDATE CASCADE ON DELETE CASCADE,

    CHECK (start_at < end_at)
);

-- migrate:down
-- above is a trick to disable sqlc on the trigger below

-- +goose StatementBegin
DROP TRIGGER IF EXISTS prevent_insertion_of_overlapping_bookings;
CREATE TRIGGER prevent_insertion_of_overlapping_bookings
BEFORE INSERT ON bookings
WHEN EXISTS (SELECT * FROM bookings WHERE house_id = NEW.house_id AND cancelled_at IS NULL AND (
    case when unixepoch(NEW.start_at) < unixepoch(start_at)
        then (unixepoch(NEW.end_at) > unixepoch(start_at))
        else (unixepoch(NEW.start_at) < unixepoch(end_at))
    end)
)
BEGIN
    SELECT RAISE(FAIL, "found an overlapping booking");
END;
-- +goose StatementEnd

-- +goose StatementBegin
DROP TRIGGER IF EXISTS prevent_update_of_overlapping_bookings;
CREATE TRIGGER prevent_update_of_overlapping_bookings
BEFORE UPDATE OF house_id, cancelled_at, start_at, end_at ON bookings
WHEN NEW.cancelled_at IS NULL AND EXISTS (SELECT * FROM bookings WHERE house_id = NEW.house_id AND cancelled_at IS NULL AND id <> NEW.id AND (
    case when unixepoch(NEW.start_at) < unixepoch(start_at)
        then (unixepoch(NEW.end_at) > unixepoch(start_at))
        else (unixepoch(NEW.start_at) < unixepoch(end_at))
    end)
)
BEGIN
    SELECT RAISE(FAIL, "found an overlapping booking");
END;
-- +goose StatementEnd
