-- +goose Up
CREATE TABLE houses (
    id TEXT NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CHECK(id <> ''),
    CHECK(name <> '')
);

CREATE TABLE house_user (
    user_id TEXT NOT NULL,
    house_id TEXT NOT NULL,
    role TEXT NOT NULL,
    PRIMARY KEY (house_id, user_id),
    FOREIGN KEY(house_id) REFERENCES houses(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE INDEX houseuserindex ON house_user(user_id);
