-- +goose Up
CREATE TABLE charges (
    id TEXT NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    description TEXT NOT NULL,

    house_id TEXT NOT NULL,
    price_id TEXT NOT NULL,

    unit TEXT NOT NULL,
    amount INTEGER NOT NULL,
    min_person_charged INTEGER NOT NULL,
    charged_over_person_count INTEGER NOT NULL,
    exempted_under_age INTEGER NOT NULL,

    optionality TEXT NOT NULL,

    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    deleted_at TIMESTAMP DEFAULT NULL,

    FOREIGN KEY(house_id) REFERENCES houses(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY(price_id) REFERENCES prices(id) ON UPDATE CASCADE ON DELETE CASCADE,

    CHECK(id <> ''),
    CHECK(name <> ''),
    CHECK(unit <> '')
);
