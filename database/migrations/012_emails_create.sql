-- +goose Up
CREATE TABLE emails (
    id TEXT NOT NULL PRIMARY KEY,

    booking_id INTEGER DEFAULT 0 NOT NULL,

    -- header_ prefix to prevent sql keyword conflicts
    header_from TEXT NOT NULL,
    header_to TEXT NOT NULL,
    header_subject TEXT NOT NULL,
    text_plain TEXT NOT NULL,

    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    sent_at TIMESTAMP DEFAULT NULL,
    fatal_error TEXT NOT NULL DEFAULT '',

    FOREIGN KEY(booking_id) REFERENCES bookings(id) ON UPDATE CASCADE ON DELETE CASCADE
);
