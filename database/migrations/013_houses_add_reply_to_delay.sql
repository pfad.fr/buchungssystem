-- +goose Up
ALTER TABLE houses ADD COLUMN reply_to TEXT NOT NULL DEFAULT '';
ALTER TABLE houses ADD COLUMN days_for_user_confirmation INTEGER NOT NULL DEFAULT 7;
