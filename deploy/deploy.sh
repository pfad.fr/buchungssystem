#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
#
# SPDX-License-Identifier: CC0-1.0

set -o errexit
set -o nounset
set -o pipefail

mkdir -p /usr/local/lib/systemd/system

rm /usr/local/bin/buchungsserver || echo "buchungsserver already removed"
cp buchungsserver /usr/local/bin/
cp buchungsserver.service /usr/local/lib/systemd/system

rm /usr/local/bin/snid || echo "snid already removed"
cp snid /usr/local/bin/
cp snid.service /usr/local/lib/systemd/system

rm /usr/local/bin/http-redirect || echo "http-redirect already removed"
cp http-redirect /usr/local/bin/
cp http-redirect.service /usr/local/lib/systemd/system

systemctl daemon-reload

systemctl enable --now buchungsserver
systemctl restart buchungsserver || echo " could not restart buchungsserver"

systemctl enable --now snid
systemctl restart snid || echo " could not restart snid"

systemctl enable --now http-redirect
systemctl restart http-redirect || echo " could not restart snid"
