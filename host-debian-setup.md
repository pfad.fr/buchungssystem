<!--
SPDX-FileCopyrightText: 2022 Olivier Charvin

SPDX-License-Identifier: EUPL-1.2+
-->
adduser

chmod u+rw .ssh
chmod u+r .ssh/authorized_keys

/etc/ssh/sshd_config
    PasswordAuthentication no
    PermitRootLogin no

usermod -a -G sudo ocharvin

https://www.debian.org/releases/stable/amd64/ch08s04.en.html

apt install exim4
dpkg-reconfigure exim4-config


https://vernon.wenberg.net/linux/set-up-unattended-upgrades-on-ubuntu-20-04/ without postfix

https://wiki.debian.org/UnattendedUpgrades

apt-daily.timer.d/override.conf
[Timer]
OnCalendar=
OnCalendar=*-*-* 5:00
RandomizedDelaySec=60m


WIREGUARD

wg genkey | tee private.key | wg pubkey > public.key

https://wiki.debian.org/SimplePrivateTunnelVPNWithWireGuard
https://selfhostedfuture.xyz/books/wireguard-vpn/page/ssh-over-wireguard-vpn


NFTABLES
cat /etc/nftables.conf
sudo systemctl restart nftables.service
sudo nft list ruleset
```
#!/usr/sbin/nft -f

flush ruleset

table inet filter {
    chain inbound_ipv4 {
        # accepting ping (icmp-echo-request) for diagnostic purposes.
        # However, it also lets probes discover this host is alive.
        # This sample accepts them within a certain rate limit:
        #
        # icmp type echo-request limit rate 5/second accept
    }

    chain inbound_ipv6 {
        # accept neighbour discovery otherwise connectivity breaks
        #
        icmpv6 type { nd-neighbor-solicit, nd-router-advert, nd-neighbor-advert } accept

        # accepting ping (icmpv6-echo-request) for diagnostic purposes.
        # However, it also lets probes discover this host is alive.
        # This sample accepts them within a certain rate limit:
        #
        # icmpv6 type echo-request limit rate 5/second accept
    }

	chain input {
		# By default, drop all traffic unless it meets a filter
	        # criteria specified by the rules that follow below.
	        type filter hook input priority 0; policy drop;

	        # Allow traffic from established and related packets, drop invalid
        	ct state vmap { established : accept, related : accept, invalid : drop }

	        # Allow loopback traffic.
        	iifname lo accept
		# Allow wireguard traffic
        	iifname wg0 accept

	        # Jump to chain according to layer 3 protocol using a verdict map
        	meta protocol vmap { ip : jump inbound_ipv4, ip6 : jump inbound_ipv6 }

	        # Allow HTTPS TCP/443 and HTTP TCP/80
	        # Gemini TCP/1965
	        # for IPv4 and IPv6.
        	tcp dport { 443, 80, 1965 } accept

	        # accept all WireGuard packets received on a public interface
	        iif eth0 udp dport 55820 accept

		# Allow http3 udp traffic
	        iif eth0 udp dport 443 accept

	        # Uncomment to enable logging of denied inbound traffic
        	# log prefix "[nftables] Inbound Denied: " counter drop
	}
	chain forward {
        	# Drop everything (assumes this device is not a router)
	        type filter hook forward priority 0; policy drop;
	}
	chain output {
		type filter hook output priority 0;
	}
}
```

SNID

cat /etc/network/interfaces.d/10-nat46
auto lo
iface lo inet loopback
	up ip route add local 64:ff9b:1::/96 dev lo



Multiple ips
cat /etc/network/interfaces.d/60-ipv6
iface eth0 inet6 static
	address 2a01:4f8:c0c:9a6d::42/64