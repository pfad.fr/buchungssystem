<!--
SPDX-FileCopyrightText: 2022 Olivier Charvin

SPDX-License-Identifier: EUPL-1.2+
-->
# Personbezogenedaten, die in der Datenbank gespeichert sind

## users (Administratoren)
- email
- name

## emails (versendete E-Mails)
- to (name+email)
- subject
- text

## log_events
- message (eventually personal informations)

## bookings
- contact (name)
- email
- phone
- comment
- invoice (Rechnungsdaten)
- landline (Telefon)

# Andere Personbezogenedaten
- IP-Addresse (technical logs)

# Cookies
Ausschließlich aus technischen Gründen:
- CSRF protection
- Admin login (OAuth)
- ...


---


## DSGVO

- IP-Adresse:
  * truncated
  * add in privacy page
- Cookies:
  * add in privacy page
- Verbraucherwiderrufsformular:
  * beim bestätigen
- Personbezogenedaten:
  * add in privacy page
  * determine lifetime
  * document lifetime
  * enforce lifetime
- "Wenn Sie mit uns einen Belegungsvertrag schließen, dann passiert folgendes"
- Bestätigung
  drei Haken setzen muss, nämlich "Ich habe die Vertragsbedingungen erhalten und bin damit einverstanden", "Ich wurde über mein Widerrufsrecht belehrt." und ein "Ich habe die Datenschutzbedingungen erhalten." Erst wenn alle drei Haken drin sind, kann der Nutzer auf "Zahlungspflichtig buchen" klicken (und das Wort "zahlungspflichtig" muss leider draufstehen).


## Welche Daten (finalité, la base légale et la durée de conservation)
- https://www.cnil.fr/fr/la-cnil-publie-son-registre-rgpd
- https://www.bvdnet.de/muster-fuer-verzeichnisse-gemaess-art-30/

## Lifetime
- Stornierung (Admin-Abgelehnt, Kein Vertragsschluss):
  * Persönnliche Daten nach 7 Tage nach Stornierung gelöscht
- Vetragsschluss:
  * Persönnliche Daten nach 356 Tage nach Abreise gelöscht

## IP anonymisation:
- ipv4: 1 byte less
- ipv6: 80 bites less (5x 16bits)
