// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: CC0-1.0

module.exports = {
  overrides: [
    {
      files: ["*.gohtml"],
      options: {
        // npm install --save-dev prettier-plugin-go-template
        parser: "go-template",
      },
    },
  ],
};
