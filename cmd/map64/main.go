// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"fmt"
	"log"
	"net/netip"
	"os"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	ip, err := netip.ParseAddr(os.Args[1])
	if err != nil {
		ip, _ = netip.ParseAddr("::" + os.Args[1])
		if !ip.IsValid() {
			return err
		}
	}
	buf := ip.AsSlice()
	buf = buf[len(buf)-4:]
	ip4 := netip.AddrFrom4([4]byte(buf))
	fmt.Println(ip4)
	return nil
}
