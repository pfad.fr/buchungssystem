// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"database/sql"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"net"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"github.com/caddyserver/certmagic"
	"github.com/emersion/go-message/mail"

	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
	_ "github.com/mattn/go-sqlite3"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/buchungssystem/internal/app"
	"code.pfad.fr/buchungssystem/internal/auth"
	"code.pfad.fr/buchungssystem/internal/email"
	"code.pfad.fr/buchungssystem/internal/view"
	"code.pfad.fr/gopenidclient/assertion"
	kitlog "github.com/go-kit/log"
)

func main() {
	w := kitlog.NewSyncWriter(os.Stderr)
	logger := kitlog.NewLogfmtLogger(w)
	// logger.Log("commitVersion", commitVersion)

	// systemd sends SIGTERM (KILL cannot be caught)
	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill, syscall.SIGTERM, syscall.SIGHUP)
	go func() {
		<-ctx.Done()
		stop()
	}()

	if err := run(ctx, logger); err != nil {
		logger.Log("run-err", err)

		os.Exit(1)
	}
}

func run(ctx context.Context, logger kitlog.Logger) error {
	cookieKey, _ := base64.StdEncoding.DecodeString(os.Getenv("COOKIE_SECRET"))

	isDevMode := os.Getenv("DEV_MODE") == "true"
	if isDevMode {
		cookieKey = []byte("DEV_MODE--96-byte-long-secret-key-DEV_MODE--96-byte-long-secret-key-DEV_MODE--96-byte-long-secre")
		fmt.Println("RUNNING IN DEV MODE")
	}

	if len(cookieKey) != 64+32 {
		return errors.New("COOKIE_SECRET should be exactly 96 byte long")
	}

	addr := os.Getenv("LISTEN_ADDR")
	if addr == "" {
		addr = ":8989"
	}
	domains := tlsDomains()
	baseURL := "http://localhost" + addr // without trailing slash
	if len(domains) > 0 {
		baseURL = "https://" + domains[0] // without trailing slash
	} else if isDevMode {
		baseURL = "http://localhost:8000"
	}

	stateDir := os.Getenv("STATE_DIRECTORY")
	if stateDir == "" {
		stateDir = "./data/"
	} else {
		stateDir += "/"
	}

	db, dbClose, err := openDatabase(stateDir)
	if err != nil {
		return err
	}
	defer dbClose() //nolint:errcheck

	if os.Getenv("EXIT_AFTER_MIGRATION") == "1" {
		return nil
	}

	var assetsFS fs.FS = internal.EmbedFS
	if isDevMode {
		assetsFS = os.DirFS("internal")
	}

	queue, err := email.NewQueue(ctx, db, mail.Address{
		Address: "buchungstool@dpsg1300.de",
	}, time.Now().Add(-3*24*time.Hour), sendEmail)
	if err != nil {
		return err
	}
	baseHandler := internal.Handler{
		BaseURL: baseURL,
		DB:      db,
		Queue:   queue,
		ErrorHandler: internal.ErrorHandler{
			Debug: isDevMode,
		},
	}

	privateKey, err := readOrCreatePrivateKey(stateDir + ".azure.key")
	if err != nil {
		return err
	}

	oidc := auth.OIDC{
		ClientID:     os.Getenv("OIDC_CLIENT_ID"),
		ClientSecret: "", // now using a Certificate
		Issuer:       os.Getenv("OIDC_ISSUER"),
		Scopes:       []string{"openid", "email", "profile", "offline_access"},
		Certificate:  assertion.NewSesquiennial(privateKey, "DPSG München und Freising"),
	}

	oidc.Assertion = assertion.RS256{
		GetThumbprint: oidc.Certificate.Thumbprint,
		Validity:      time.Minute,
		CliendID:      oidc.ClientID,
		Key:           privateKey,
	}

	view.IsCertificateDowngraded = func() string {
		if oidc.Certificate.IsDowngraded() {
			return oidc.ClientID
		}
		return ""
	}

	r, err := app.Router(cookieKey, oidc, assetsFS, baseHandler)
	if err != nil {
		return err
	}

	var ln net.Listener
	if len(domains) > 0 {
		logger.Log("tls", strings.Join(domains, ","))
		// SETUP TLS
		certmagic.DefaultACME.Email = "git@olivier.pfad.fr"
		certmagic.DefaultACME.Agreed = true
		certmagic.DefaultACME.DisableHTTPChallenge = true
		certmagic.DefaultACME.ListenHost, _, err = net.SplitHostPort(addr)
		if err != nil {
			return err
		}
		logger.Log("listen-host", certmagic.DefaultACME.ListenHost)

		certConfig := certmagic.NewDefault()
		certConfig.Storage = &certmagic.FileStorage{
			Path: filepath.Join(stateDir, "certmagic"),
		}
		tlsConfig := certConfig.TLSConfig()
		tlsConfig.NextProtos = append([]string{"h2", "http/1.1"}, tlsConfig.NextProtos...)

		ln, err = tls.Listen("tcp", addr, tlsConfig)
		if err != nil {
			return err
		}

		if err := certConfig.ManageAsync(ctx, domains); err != nil { // async to prevent systemd restart
			return fmt.Errorf("could not manage TLS certificates: %v", err)
		}
	} else {
		ln, err = net.Listen("tcp", addr)
		if err != nil {
			return err
		}
	}

	srv := &http.Server{
		Handler:           r,
		ReadHeaderTimeout: 20 * time.Second,
		ReadTimeout:       1 * time.Minute,
		WriteTimeout:      2 * time.Minute,
	}
	go srv.Serve(ln) //nolint:errcheck

	cronDone := app.RunPeriodically(ctx, time.Hour, app.NewCronTasks(db, queue, internal.EmbedFS))

	if isDevMode {
		_ = os.WriteFile("server.is_running", nil, os.ModePerm)
		fmt.Println("Go to http://localhost:8000/")
	} else {
		logger.Log("msg", "server listening", "addr", addr)
	}

	<-ctx.Done()

	logger.Log("msg", "stopping email queue")
	<-queue.Done()

	logger.Log("msg", "stopping cron")
	<-cronDone

	ctxShutDown, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		cancel()
	}()

	logger.Log("msg", "stopping server")
	return srv.Shutdown(ctxShutDown) //nolint:contextcheck
}

func readOrCreatePrivateKey(path string) (*rsa.PrivateKey, error) {
	der, err := os.ReadFile(path)

	if err != nil && !errors.Is(err, fs.ErrNotExist) {
		return nil, err
	}

	if err == nil {
		return x509.ParsePKCS1PrivateKey(der)
	}

	// generate key and save it
	rsaBits := 2048
	key, err := rsa.GenerateKey(rand.Reader, rsaBits)
	if err != nil {
		return nil, err
	}
	der = x509.MarshalPKCS1PrivateKey(key)
	if err := os.WriteFile(path, der, 0o600); err != nil {
		return nil, err
	}
	return key, nil
}

func tlsDomains() []string {
	domains := os.Getenv("DOMAINS")
	if len(domains) == 0 {
		return nil
	}
	return strings.Split(domains, " ")
}

func openDatabase(stateDir string) (*database.Queries, func() error, error) {
	dbSrc := stateDir + "db.sqlite"
	db, err := sql.Open("sqlite3", dbSrc+"?_journal_mode=WAL&_busy_timeout=5000&_foreign_keys=on&_loc=auto")
	if err != nil {
		return nil, nil, err
	}

	for _, pragma := range []string{"journal_mode", "busy_timeout", "foreign_keys"} {
		var value string
		if err = db.QueryRow("PRAGMA " + pragma).Scan(&value); err != nil {
			return nil, nil, err
		}
		fmt.Printf("PRAGMA %s: %s\n", pragma, value)
	}

	migrate, err := database.MissingMigrations(db, "sqlite3")
	if err != nil {
		return nil, nil, err
	}

	// make a backup before migrating
	if migrate != nil && os.Getenv("DEV_MODE") != "true" {
		backupDst := dbSrc + "." + time.Now().Format("20060102_150405") + ".bak"
		if out, err := exec.Command("sqlite3", dbSrc, ".backup "+backupDst).CombinedOutput(); err != nil { //nolint:gosec
			return nil, nil, fmt.Errorf("could not backup database: %s \n%w", string(out), err)
		}
	}

	if migrate != nil {
		fmt.Println("GOOSE: migrating")
		if err := migrate(); err != nil {
			return nil, nil, fmt.Errorf("could not migrate database: %w", err)
		}
	} else {
		fmt.Println("GOOSE: nothing to migrate")
	}

	return database.New(db), db.Close, nil
}

func sendEmail(from string, to []string, r io.Reader) error {
	// for office365
	auth := sasl.NewLoginClient(os.Getenv("SMTP_USER"), os.Getenv("SMTP_PASSWORD"))

	return smtp.SendMail(os.Getenv("SMTP_ADDR"), auth, from, to, r)
}
