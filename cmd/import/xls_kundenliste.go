// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"fmt"
	"path/filepath"
	"strings"

	"code.pfad.fr/buchungssystem/internal/app"
)

type KundeID struct {
	Firstname, LastnameAndCompany, Email, LandlinePhone string
}
type Kunde struct {
	KundeID
	Lastname    string
	MobilePhone string
	Company     string
	Street      string
	Number      string
	City        string
	ZipCode     string
	Country     string
}

func (k Kunde) BillingInfo() app.BillingInfo {
	name := k.Company
	if name == "" {
		name = k.Firstname + " " + k.Lastname
	}
	return app.BillingInfo{
		Kind:       "",
		Name:       name,
		Address1:   k.Street + " " + k.Number,
		Address2:   "",
		PostalCode: k.ZipCode,
		City:       k.City,
		Country:    k.Country,
	}
}

var countries = map[string]string{
	"DE": "DEU",
	"AT": "AUT",
	"CH": "CHE",
}

func (imp importer) readKundenlisteXLS(folder string) (map[KundeID]Kunde, error) {
	kunden := make(map[KundeID]Kunde)
	err := readXLS(filepath.Join(folder, "Kundenliste.xls"), func(cells []string) error {
		k, err := parseKundenLine(cells)
		if err != nil {
			return err
		}
		if duplicate, ok := kunden[k.KundeID]; ok {
			if k.MobilePhone == "" {
				k.MobilePhone = duplicate.MobilePhone
			}
		}
		kunden[k.KundeID] = k
		return nil
	})

	return kunden, err

}

func parseKundenLine(cells []string) (Kunde, error) {
	if len(cells) != 15 {
		return Kunde{}, fmt.Errorf("unexpected number of cells: %d", len(cells))
	}

	lastnameAndCompany := cells[2]
	if cells[13] != "" {
		lastnameAndCompany += " (" + cells[13] + ")"
	}
	k := Kunde{
		KundeID: KundeID{
			Firstname:          cells[3],
			LastnameAndCompany: lastnameAndCompany,
			Email:              cells[4],
			LandlinePhone:      strings.ReplaceAll(cells[5], "'", ""),
		},
		Lastname:    cells[2],
		MobilePhone: strings.ReplaceAll(cells[6], "'", ""),
		Company:     cells[13],
		Street:      cells[7],
		Number:      cells[8],
		City:        cells[9],
		ZipCode:     strings.ReplaceAll(cells[10], "'", ""),
		Country:     countries[cells[11]],
	}
	if k.Country == "" {
		return k, fmt.Errorf("unknwon country: %q", cells[11])
	}
	return k, nil
}
