// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.15.0
// source: queries.sql

package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"time"
)

const existingBookings = `-- name: ExistingBookings :many
SELECT id FROM bookings
`

func (q *Queries) ExistingBookings(ctx context.Context) ([]int32, error) {
	rows, err := q.db.QueryContext(ctx, existingBookings)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []int32
	for rows.Next() {
		var id int32
		if err := rows.Scan(&id); err != nil {
			return nil, err
		}
		items = append(items, id)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const existingEmails = `-- name: ExistingEmails :many
SELECT id FROM emails WHERE id LIKE '%@freizeitplan.invalid'
`

func (q *Queries) ExistingEmails(ctx context.Context) ([]string, error) {
	rows, err := q.db.QueryContext(ctx, existingEmails)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []string
	for rows.Next() {
		var id string
		if err := rows.Scan(&id); err != nil {
			return nil, err
		}
		items = append(items, id)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const existingLogs = `-- name: ExistingLogs :many
SELECT booking_id, message FROM log_events WHERE user_id is null or user_id = ''
`

type ExistingLogsRow struct {
	BookingID int32
	Message   string
}

func (q *Queries) ExistingLogs(ctx context.Context) ([]ExistingLogsRow, error) {
	rows, err := q.db.QueryContext(ctx, existingLogs)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []ExistingLogsRow
	for rows.Next() {
		var i ExistingLogsRow
		if err := rows.Scan(&i.BookingID, &i.Message); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const importBooking = `-- name: ImportBooking :exec
INSERT INTO bookings (house_id, start_at, end_at, people_over, contact, email, phone, landline, comment, invoice, created_at, admin_confirmed_at, user_confirmed_at, cancelled_at, hashed_token, id)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
`

type ImportBookingParams struct {
	HouseID          string
	StartAt          time.Time
	EndAt            time.Time
	PeopleOver       json.RawMessage
	Contact          string
	Email            string
	Phone            string
	Landline         string
	Comment          string
	Invoice          json.RawMessage
	CreatedAt        time.Time
	AdminConfirmedAt sql.NullTime
	UserConfirmedAt  sql.NullTime
	CancelledAt      sql.NullTime
	HashedToken      string
	ID               int32
}

func (q *Queries) ImportBooking(ctx context.Context, arg ImportBookingParams) error {
	_, err := q.db.ExecContext(ctx, importBooking,
		arg.HouseID,
		arg.StartAt,
		arg.EndAt,
		arg.PeopleOver,
		arg.Contact,
		arg.Email,
		arg.Phone,
		arg.Landline,
		arg.Comment,
		arg.Invoice,
		arg.CreatedAt,
		arg.AdminConfirmedAt,
		arg.UserConfirmedAt,
		arg.CancelledAt,
		arg.HashedToken,
		arg.ID,
	)
	return err
}

const importEmail = `-- name: ImportEmail :exec
INSERT INTO emails (booking_id, header_from, header_to, header_subject, text_plain, created_at, sent_at, id)
VALUES (?, ?, ?, ?, ?, ?, ?, ?)
`

type ImportEmailParams struct {
	BookingID     int32
	HeaderFrom    string
	HeaderTo      string
	HeaderSubject string
	TextPlain     string
	CreatedAt     time.Time
	SentAt        sql.NullTime
	ID            string
}

func (q *Queries) ImportEmail(ctx context.Context, arg ImportEmailParams) error {
	_, err := q.db.ExecContext(ctx, importEmail,
		arg.BookingID,
		arg.HeaderFrom,
		arg.HeaderTo,
		arg.HeaderSubject,
		arg.TextPlain,
		arg.CreatedAt,
		arg.SentAt,
		arg.ID,
	)
	return err
}

const insertLog = `-- name: InsertLog :exec
INSERT INTO log_events (booking_id, message)
VALUES (?,?)
`

type InsertLogParams struct {
	BookingID int32
	Message   string
}

func (q *Queries) InsertLog(ctx context.Context, arg InsertLogParams) error {
	_, err := q.db.ExecContext(ctx, insertLog, arg.BookingID, arg.Message)
	return err
}

const updateBooking = `-- name: UpdateBooking :exec
UPDATE bookings
SET house_id = ?, start_at = ?, end_at = ?, people_over = ?, contact = ?, email = ?, phone = ?, landline  = ?, comment = ?, invoice = ?, created_at = ?, admin_confirmed_at = COALESCE(?, admin_confirmed_at), user_confirmed_at = COALESCE(?, user_confirmed_at), cancelled_at = COALESCE(?, cancelled_at), hashed_token = ?
WHERE id = ?
`

type UpdateBookingParams struct {
	HouseID          string
	StartAt          time.Time
	EndAt            time.Time
	PeopleOver       json.RawMessage
	Contact          string
	Email            string
	Phone            string
	Landline         string
	Comment          string
	Invoice          json.RawMessage
	CreatedAt        time.Time
	AdminConfirmedAt sql.NullTime
	UserConfirmedAt  sql.NullTime
	CancelledAt      sql.NullTime
	HashedToken      string
	ID               int32
}

func (q *Queries) UpdateBooking(ctx context.Context, arg UpdateBookingParams) error {
	_, err := q.db.ExecContext(ctx, updateBooking,
		arg.HouseID,
		arg.StartAt,
		arg.EndAt,
		arg.PeopleOver,
		arg.Contact,
		arg.Email,
		arg.Phone,
		arg.Landline,
		arg.Comment,
		arg.Invoice,
		arg.CreatedAt,
		arg.AdminConfirmedAt,
		arg.UserConfirmedAt,
		arg.CancelledAt,
		arg.HashedToken,
		arg.ID,
	)
	return err
}

const updateEmail = `-- name: UpdateEmail :exec
UPDATE emails
SET booking_id = ?, header_from = ?, header_to = ?, header_subject = ?, text_plain = ?, created_at = ?, sent_at = ?
WHERE id = ?
`

type UpdateEmailParams struct {
	BookingID     int32
	HeaderFrom    string
	HeaderTo      string
	HeaderSubject string
	TextPlain     string
	CreatedAt     time.Time
	SentAt        sql.NullTime
	ID            string
}

func (q *Queries) UpdateEmail(ctx context.Context, arg UpdateEmailParams) error {
	_, err := q.db.ExecContext(ctx, updateEmail,
		arg.BookingID,
		arg.HeaderFrom,
		arg.HeaderTo,
		arg.HeaderSubject,
		arg.TextPlain,
		arg.CreatedAt,
		arg.SentAt,
		arg.ID,
	)
	return err
}
