// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"code.pfad.fr/buchungssystem/database"
)

type record struct {
	Booking *database.Booking
	Status  string
	Logs    []string

	ExtraChargesPercentages map[string]int32
	PriceCents              int
}

func (imp importer) gatherHouses() (map[string]database.House, error) {
	houses := make(map[string]database.House, len(houseIDs))

	findHouse := database.New(imp.db.db).SettingHouseFind
	for key, id := range houseIDs {
		house, err := findHouse(context.Background(), id)
		if err != nil {
			return nil, fmt.Errorf("missing house: %q: %w", id, err)
		}
		houses[key] = house
	}
	return houses, nil
}

func (imp importer) readReservierungenXLS(folder string, kunden map[KundeID]Kunde) (map[int32]record, error) {
	houses, err := imp.gatherHouses()
	if err != nil {
		return nil, err
	}

	records := make(map[int32]record)
	sawLastLine := false
	err = readXLS(filepath.Join(folder, "Reservierungen.xls"), func(cells []string) error {
		if !sawLastLine && len(cells) == 17 {
			sawLastLine = true
			return nil
		}

		r, err := parseReservierungLine(cells, houses, kunden)
		if err != nil {
			return err
		}
		records[r.Booking.ID] = r
		return nil
	})

	return records, err
}

func parseReservierungLine(cells []string, houses map[string]database.House, kunden map[KundeID]Kunde) (record, error) {
	if len(cells) != 23 {
		return record{}, fmt.Errorf("unexpected number of cells: %d", len(cells))
	}
	r := record{
		Status: cells[10],
	}
	id, err := strconv.Atoi(cells[0])
	if err != nil {
		return r, err
	}

	house, ok := houses[cells[1]]
	if !ok {
		return r, fmt.Errorf("unknown house: %q", cells[1])
	}

	startAt, err := time.Parse("02.01.2006\xc2\xa015:04", cells[6])
	if err != nil {
		return r, err
	}
	endAt, err := time.Parse("02.01.2006\xc2\xa015:04", cells[8])
	if err != nil {
		return r, err
	}

	kid := KundeID{
		Firstname:          cells[3],
		LastnameAndCompany: cells[2],
		Email:              cells[4],
		LandlinePhone:      cells[5],
	}
	kunde, ok := kunden[kid]
	for !ok {
		oldLastnameAndCompany := kid.LastnameAndCompany

		if strings.HasSuffix(kid.LastnameAndCompany, " )") {
			kid.LastnameAndCompany = kid.LastnameAndCompany[:len(kid.LastnameAndCompany)-2] + ")"
		} else if strings.Contains(kid.LastnameAndCompany, "(Pfarrei Frieden Christi, München)") {
			kid.LastnameAndCompany = strings.Replace(kid.LastnameAndCompany, "(Pfarrei Frieden Christi, München)", "(Pfarrkirchenstiftung Frieden Christi)", 1)
		} else {
			kid.LastnameAndCompany = strings.Replace(kid.LastnameAndCompany, "  ", " ", 1)
		}

		if oldLastnameAndCompany == kid.LastnameAndCompany {
			log.Println(kid)
			return r, fmt.Errorf("no contact for booking %d", id)
		}
		kunde, ok = kunden[kid]
	}

	billingJSON, err := json.Marshal(kunde.BillingInfo())
	if err != nil {
		return r, err
	}

	peopleOver := make(map[int32]int)
	peopleOver[7], err = strconv.Atoi(cells[15]) // is actually over age 3 (not 7), but under 7 they get exempted from Kurtaxe
	if err != nil {
		return r, err
	}
	peopleOverJSON, err := json.Marshal(peopleOver)
	if err != nil {
		return r, err
	}

	r.Logs = append(r.Logs, "Importiert aus Freizeitplan\nhttps://live.freizeitplan.net/admin/reservations/edit/r_nbr:"+strconv.Itoa(id))
	r.Logs = append(r.Logs, "Freizeitplan Stand: "+r.Status)
	r.Logs = append(r.Logs, "Freizeitplan Preis: "+cells[18])
	if cells[18] != "" {
		ten, cents, _ := strings.Cut(cells[18], ",")
		if cents == "" {
			cents = "0"
		}
		i, err := strconv.Atoi(ten)
		if err != nil {
			return r, err
		}
		r.PriceCents = 100 * i
		i, err = strconv.Atoi(cents)
		if err != nil {
			return r, err
		}
		r.PriceCents += i
	}

	switch id {
	case 1707, 2031, 2032, 2053, 2062:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_winter22_rabatt_kurtaxe": 100,
		}
	case 1826, 1978, 2035, 2107, 2153:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_sommer23_rabatt_kurtaxe": 100,
		}
	case 1827, 1852, 1873:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_sommer23_rabatt_kurtaxe":        100,
			"ch_thal_sommer23_rabatt_ubernachtung_1": 100,
			"ch_thal_sommer23_rabatt_reinigung":      200,
		}
	case 1828, 1941:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_winter22_rabatt_kurtaxe": 100,
			"ch_thal_winter22_rabatt_dpsg":    100,
			"ch_thal_winter22_geschirr":       100,
		}
	case 1858, 2089:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_winter22_rabatt_kurtaxe":  100,
			"ch_thal_winter22_reinigung_extra": 100,
		}
	case 1884, 1989, 2074:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_winter22_reinigung_extra": 100,
		}
	case 1918, 2018, 2042, 2043, 2068:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_winter22_rabatt_dpsg": 100,
		}
	case 1979, 2106, 2122, 2144:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_sommer23_reinigung_extra": 100,
		}
	case 1981, 1986, 1987:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_sommer23_geschirr":       100,
			"ch_thal_sommer23_rabatt_kurtaxe": 100,
		}
	case 1984:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_winter22_geschirr": 100,
		}
	case 2088:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_winter22_geschirr":       50,
			"ch_thal_winter22_rabatt_kurtaxe": 100,
		}
	case 2090:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_sommer23_reinigung_extra": 100,
			"ch_thal_sommer23_rabatt_kurtaxe":  100,
		}
	case 2093:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_winter23_reinigung_extra": 100,
			"ch_thal_winter23_rabatt_kurtaxe":  100,
		}
	case 2098:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_sommer23_heizung_extra":   2000,
			"ch_thal_sommer23_reinigung_extra": 100,
		}
	case 2111:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_winter22_besondere_an_abreise": 100,
			"ch_thal_winter22_reinigung_extra":      100,
			"ch_thal_winter22_rabatt_kurtaxe":       100,
		}
	case 2127:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_winter22_geschirr":        100,
			"ch_thal_winter22_reinigung_extra": 100,
			"ch_thal_winter22_rabatt_kurtaxe":  100,
		}
	case 2128:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_winter22_geschirr":       100,
			"ch_thal_winter22_rabatt_kurtaxe": 100,
		}
	case 2131, 2151:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_sommer23_rabatt_dpsg": 125,
		}
	case 2143:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_winter23_rabatt_kurtaxe": 100,
		}

	//// SEEGATTERL
	case 1883:
		if house.ID != "seegatterl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_see_sommer22_rabatt_dpsg": 150,
		}
	case 2029:
		if house.ID != "seegatterl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_see_sommer22_vorzeitige_anreise": 100,
			"ch_see_sommer22_extra_kurtaxe":      -3000, // fewer people
		}
	case 2030, 1922:
		if house.ID != "seegatterl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_see_sommer22_rabatt_kurtaxe": 100,
		}
	case 2039:
		if house.ID != "seegatterl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_see_sommer22_rabatt_sonder": 16800, // fewer people on the second night
		}
	case 2071:
		if house.ID != "seegatterl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_see_sommer22_rabatt_dpsg":   120,   // fewer people on the second night
			"ch_see_sommer22_extra_kurtaxe": -2000, // fewer people
		}
	case 2084, 1937:
		if house.ID != "seegatterl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_see_sommer22_rabatt_dpsg": 600, // free night
		}
	case 2086, 2085, 2075:
		if house.ID != "seegatterl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_see_sommer22_extra_kurtaxe": (map[int]int32{ // more Kurtaxe
				2086: 200,
				2085: 1600,
				2075: -1300, // children
			})[id],
		}
	case 2113:
		if house.ID != "seegatterl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_see_sommer22_extra_heizung": 100,
			"ch_see_sommer22_extra_kurtaxe": 300, // more Kurtaxe
		}

	case 2019:
		if house.ID != "seegatterl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_see_winter22_rabatt_kurtaxe": 100,
		}
	case 1951:
		if house.ID != "seegatterl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_see_winter22_extra_kurtaxe": -200,
		}
	case 2116:
		if house.ID != "seegatterl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_see_winter22_extra_kurtaxe": 300,
		}
	case 1936, 1935:
		if house.ID != "seegatterl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_see_winter22_rabatt_dpsg": 600, // free night
		}
	case 2051, 2050:
		if house.ID != "seegatterl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_see_sommer23_rabatt_ubernachtung_3": 500, // free night
		}
	case 2072:
		if house.ID != "seegatterl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_see_sommer23_rabatt_ubernachtung_3": 125, // special percentage, because midway between winter and summer
		}
	case 2169, 2168, 2167, 2165, 2160, 2157, 2154, 2137, 2124, 2121, 2120, 2115, 2104, 2103, 2102, 2097, 2082, 2070, 2069:
		if house.ID != "seegatterl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_see_sommer23_rabatt_ubernachtung_3": 100,
		}
	case 2175, 2174, 2172:
		if house.ID != "thalhaeusl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{
			"ch_thal_winter23_rabatt_kurtaxe": 100,
			"ch_thal_winter23_rabatt_dpsg":    100,
		}
	case 2187:
		if house.ID != "seegatterl" {
			return r, fmt.Errorf("wrong house for %d: %s", id, house.ID)
		}
		r.ExtraChargesPercentages = map[string]int32{"ch_see_winter23_rabatt_dpsg": 750}
	default:
		r.ExtraChargesPercentages = make(map[string]int32)
	}

	phone := kunde.MobilePhone
	if phone == "" {
		phone = kunde.LandlinePhone
		kunde.LandlinePhone = ""
	}

	r.Booking = &database.Booking{
		ID:         int32(id),
		HouseID:    house.ID,
		StartAt:    adjustedDate(startAt, house.StartMinute),
		EndAt:      adjustedDate(endAt, house.EndMinute),
		PeopleOver: peopleOverJSON,
		Contact:    kunde.Firstname + " " + kunde.Lastname,
		Email:      kunde.Email,
		Phone:      phone,
		Landline:   kunde.LandlinePhone,
		Comment:    cells[13],
		Invoice:    billingJSON,
		CreatedAt:  time.Time{},
		// AdminConfirmedAt
		// UserConfirmedAt
		// CancelledAt
		// HashedToken
	}
	if cells[14] != "" {
		r.Logs = append(r.Logs, cells[14])
	}
	return r, nil
}
