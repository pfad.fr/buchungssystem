-- name: ImportBooking :exec
INSERT INTO bookings (house_id, start_at, end_at, people_over, contact, email, phone, landline, comment, invoice, created_at, admin_confirmed_at, user_confirmed_at, cancelled_at, hashed_token, id)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
;

-- name: UpdateBooking :exec
UPDATE bookings
SET house_id = ?, start_at = ?, end_at = ?, people_over = ?, contact = ?, email = ?, phone = ?, landline  = ?, comment = ?, invoice = ?, created_at = ?, admin_confirmed_at = COALESCE(?, admin_confirmed_at), user_confirmed_at = COALESCE(?, user_confirmed_at), cancelled_at = COALESCE(?, cancelled_at), hashed_token = ?
WHERE id = ?
;

-- name: ExistingBookings :many
SELECT id FROM bookings;

-- name: ImportEmail :exec
INSERT INTO emails (booking_id, header_from, header_to, header_subject, text_plain, created_at, sent_at, id)
VALUES (?, ?, ?, ?, ?, ?, ?, ?)
;
-- name: UpdateEmail :exec
UPDATE emails
SET booking_id = ?, header_from = ?, header_to = ?, header_subject = ?, text_plain = ?, created_at = ?, sent_at = ?
WHERE id = ?
;

-- name: ExistingEmails :many
SELECT id FROM emails WHERE id LIKE '%@freizeitplan.invalid';

-- name: ExistingLogs :many
SELECT booking_id, message FROM log_events WHERE user_id is null or user_id = '';

-- name: InsertLog :exec
INSERT INTO log_events (booking_id, message)
VALUES (?,?)
;
