// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"github.com/emersion/go-message/mail"
)

var fakeMinute map[string]int32

func (imp importer) readNachrichtenCSV(folder string, bookings map[int32]*database.Booking) (emails []database.Email, err error) {
	fakeMinute = make(map[string]int32)
	for i := 1; err == nil; i++ {
		err = readWindows1252CSV(filepath.Join(folder, fmt.Sprintf("Nachrichten_%d.csv", i)), func(rec []string) error {
			email, err := imp.parseNachrichtenLine(rec, bookings)
			if err != nil {
				return err
			}
			emails = append(emails, email)
			return nil
		})
	}
	if errors.Is(err, os.ErrNotExist) {
		return emails, nil
	}
	return emails, err
}

var bookingIDRegexs = []*regexp.Regexp{
	regexp.MustCompile(`^.*#(\d+) (.+) +Selbst`),
	regexp.MustCompile(`^Ihre Online Reservierung - Nummer: (\d+)$`),
	regexp.MustCompile(`^Reservierung Online-Reservierungssystem - Nummer: (\d+)$`),
}

func (imp importer) extractIDFromSubject(subject string) string {
	for _, reg := range bookingIDRegexs {
		matches := reg.FindStringSubmatch(subject)
		if len(matches) > 1 {
			return matches[1]
		}
	}

	return ""
}

func (imp importer) handleGenericSubject(email database.Email) error {
	if email.CreatedAt.Before(time.Date(2021, time.January, 1, 0, 0, 0, 0, time.Local)) {
		return nil
	}
	subject := email.HeaderSubject
	if subject == email.TextPlain {
		switch subject {
		case "Reservierungsbestätigung gesendet":
			return nil
		}
		return fmt.Errorf("unknown booking (same body): %q", subject)
	}

	if email.HeaderTo != "haeuser@dpsg1300.de" && !strings.HasSuffix(email.HeaderTo, "<haeuser@dpsg1300.de>") {
		// external receivers

		switch {
		case subject == "Ihre Rechnung":
		case strings.HasPrefix(subject, "Rechnung für Ihren Aufenthalt im "):
		case strings.HasPrefix(subject, "Jugendhaus Thalhäusl - Weitere Betriebsschließung wegen Corona-Shutdown"):
		case strings.HasPrefix(subject, "Weitere Schließung von Beherbungsbetrieben"):

		case strings.HasSuffix(subject, " Betreff: Pfeiltasten und Vormonate"):
		case strings.HasSuffix(subject, " Betreff: Es geht nicht weiter"):
		case strings.HasSuffix(subject, ".05.2021 - Stornierung der Buchung"):
		case subject == "Buchungsanfrage":
		case subject == "Falsche eMAil-Adresse eingegeben":

		default:
			fmt.Println(subject)
			fmt.Println(email.TextPlain)
			// fmt.Printf("generic to external: %q->%q: %q\n", email.HeaderFrom, email.HeaderTo, subject)
			// return fmt.Errorf("generic to external: %q->%q: %q", email.HeaderFrom, email.HeaderTo, subject)
		}
		return nil
	}

	// sent to haeuser@dpsg1300.de
	switch {
	case email.HeaderSubject == "Automatische Antwort: Rechnung für Ihren Aufenthalt im Selbstversorgerhaus Thalhäusl":
	case email.HeaderSubject == "Automatische Antwort: Ihre Rechnung":
	case email.HeaderSubject == "Automatic reply: Reservierung Online-Reservierungssystem - Nummer: 2134":
	default:
		return fmt.Errorf("unknown booking %q", email.HeaderSubject)
	}

	return nil
}

func (imp importer) findBooking(email database.Email, bookings map[int32]*database.Booking) (*database.Booking, error) {
	id := imp.extractIDFromSubject(email.HeaderSubject)
	if id == "" {
		return nil, imp.handleGenericSubject(email)
	}
	i64, err := strconv.ParseInt(id, 10, 32)
	if err != nil {
		return nil, err
	}
	bookingID := int32(i64)
	if bookingID == 0 {
		return nil, fmt.Errorf("bookingID==0: %s", email.HeaderSubject)
	}
	booking := bookings[bookingID]
	if booking == nil {
		if bookingID > 1000 {
			return nil, fmt.Errorf("unknown booking %d: %s", bookingID, email.HeaderSubject)
		}
		return nil, nil // ignore unknown bookings with id <= 1000
	}
	return booking, nil
}

var freizeitEmail = regexp.MustCompile(`^(.+)\((.*@.*)\)$`)

func (imp importer) parseNachrichtenLine(rec []string, bookings map[int32]*database.Booking) (database.Email, error) {
	var email database.Email
	var err error
	email.ID = generateMessageID(rec) + "@freizeitplan.invalid"

	if rec[0] == ", haeuser@dpsg1300.de" {
		rec[0] = "haeuser@dpsg1300.de"
	}
	from, err := mail.ParseAddress(rec[0])
	if err != nil {
		if strings.Contains(rec[0], "@") {
			return email, fmt.Errorf("invalid email %q: %w", rec[0], err)
		}
		from = &mail.Address{
			Name:    rec[0],
			Address: "invalid@invalid.invalid",
		}
	}
	email.HeaderFrom = from.String()

	var to *mail.Address
	matches := freizeitEmail.FindStringSubmatch(rec[1])

	if len(matches) > 0 {
		to = &mail.Address{
			Name:    strings.TrimSpace(matches[1]),
			Address: matches[2],
		}
	} else {
		to, err = mail.ParseAddress(rec[1])
		if err != nil {
			return email, err
		}
	}
	email.HeaderTo = to.String()
	email.HeaderSubject = rec[2]
	email.TextPlain = rec[3]

	m := fakeMinute[rec[4]]
	fakeMinute[rec[4]]++
	date, err := time.Parse("02.01.2006", rec[4])
	if err != nil {
		return email, err
	}
	date = adjustedDate(date, m)
	email.CreatedAt = date

	booking, err := imp.findBooking(email, bookings)
	if err != nil {
		return email, err
	}
	if booking == nil {
		email.BookingID = -1 // booking does not exist anymore
		return email, nil
	}
	email.BookingID = booking.ID
	if booking.CreatedAt.IsZero() || email.CreatedAt.Before(booking.CreatedAt) {
		booking.CreatedAt = email.CreatedAt
	}

	if strings.Contains(email.HeaderSubject, "Bestätigung Ihrer Anfrage") ||
		strings.Contains(email.HeaderSubject, "Confirmation of your request ") {
		booking.AdminConfirmedAt = sql.NullTime{
			Valid: true,
			Time:  email.CreatedAt,
		}
	} else if strings.Contains(email.HeaderSubject, "Reservierung Online-Reservierungssystem - Nummer") {
		booking.UserConfirmedAt = sql.NullTime{
			Valid: true,
			Time:  email.CreatedAt,
		}
	} else if strings.Contains(email.HeaderSubject, "Stornierung Ihrer Reservierung ") ||
		strings.Contains(email.HeaderSubject, "Cancelation of your request ") ||
		strings.Contains(email.HeaderSubject, "Anfrage abgelehnt ") {
		booking.CancelledAt = sql.NullTime{
			Valid: true,
			Time:  email.CreatedAt,
		}
	} else {
		if !strings.Contains(email.HeaderSubject, "Ihre Online Reservierung - Nummer: ") &&
			!strings.Contains(email.HeaderSubject, "Buchungsanfrage  ") &&
			!strings.Contains(email.HeaderSubject, "Inquiry  ") &&
			!strings.Contains(email.HeaderSubject, "Ihre Online Reservierung - Nummer: ") {
			fmt.Println(email.HeaderSubject)
		}
	}

	// if strings.Contains(email.TextPlain, "https://live.freizeitplan.net/frontends/confirmation/") {
	// 	booking.Comment = "confirmation"
	// } else if strings.Contains(email.TextPlain, "Vorläufiger Gesamtbetrag") {
	// 	if booking.Comment == "" {
	// 		booking.Comment = "Vorläufiger Gesamtbetrag"
	// 	}
	// } else if strings.Contains(email.TextPlain, "Gesamtpreis") {
	// 	if booking.Comment == "" {
	// 		booking.Comment = "Gesamtpreis"
	// 	}
	/*
		matches := priceEmail.FindStringSubmatch(email.TextPlain)
		if len(matches) == 7 {
			peopleOver := make(map[int32]int)

			peopleOver[3], err = strconv.Atoi(matches[1])
			if err != nil {
				return email, err
			}
			babies, err := strconv.Atoi("0" + matches[2])
			if err != nil {
				return email, err
			}
			if babies > 0 {
				peopleOver[0] = babies
			}
			booking.PeopleOver, err = json.Marshal(peopleOver)
			if err != nil {
				return email, err
			}

			price := matches[3]
			comment := strings.ReplaceAll(matches[5], "<br>", "\n")
			comment = strings.TrimSpace(comment)
			booking.Comment = comment

			fmt.Println(peopleOver, price, comment)

			if booking.Phone == "" {
				booking.Phone = matches[6]
			}

		} else {
			fmt.Println(email.HeaderSubject)
			fmt.Println(email.TextPlain)
			return email, fmt.Errorf("not enough matches for Gesamtpreis %d: %v", len(matches), matches)
		}
	*/
	// }

	return email, nil
}

func generateMessageID(rec []string) string {
	hash := sha256.New()
	for _, r := range rec {
		_, err := hash.Write([]byte(r + "--NEXT_RECORD--"))
		if err != nil {
			panic(err)
		}
	}
	return hex.EncodeToString(hash.Sum(nil))
}
