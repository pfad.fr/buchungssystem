// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"context"
	"database/sql"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal/app"
	_ "github.com/mattn/go-sqlite3"
	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/transform"
)

var houseIDs = map[string]string{
	"Selbstversorgerhaus Thalhäusl":         "thalhaeusl",
	"Selbstversorgerhaus Thalhäusl - 1. OG": "thalhaeusl-1og",
	"Selbstversorgerhaus Thalhäusl - 2. OG": "thalhaeusl",
	"Selbstversorgerhaus Seegatterl":        "seegatterl",
	"Bootshaus Schliersee":                  "bootshaus",
}

func main() {
	db, dbClose, err := openDatabase("data/")
	if err != nil {
		log.Fatal(err)
	}
	defer dbClose() //nolint:errcheck
	imp := importer{
		db: db,
	}

	err = imp.importBackup("cmd/import/backupdata")
	if err != nil {
		log.Fatal(err)
	}
}

func openDatabase(stateDir string) (*Queries, func() error, error) {
	dbSrc := stateDir + "db.sqlite"
	db, err := sql.Open("sqlite3", dbSrc+"?_journal_mode=WAL&_busy_timeout=5000&_foreign_keys=on&_loc=auto")
	if err != nil {
		return nil, nil, err
	}

	for _, pragma := range []string{"journal_mode", "busy_timeout", "foreign_keys"} {
		var value string
		if err = db.QueryRow("PRAGMA " + pragma).Scan(&value); err != nil {
			return nil, nil, err
		}
		fmt.Printf("PRAGMA %s: %s\n", pragma, value)
	}

	return New(db), db.Close, nil
}

type importer struct {
	db *Queries
}

func adjustedStatus(b *database.Booking, status string, now time.Time) error {
	switch status {
	case "angefragt":
		if b.AdminConfirmedAt.Valid {
			return fmt.Errorf("booking %d (%s) admin_confirmed", b.ID, status)
		}
		if b.UserConfirmedAt.Valid {
			return fmt.Errorf("booking %d (%s) user_confirmed", b.ID, status)
		}
		if b.CancelledAt.Valid {
			return fmt.Errorf("booking %d (%s) cancelled", b.ID, status)
		}
	case "bestätigt":
		if !b.AdminConfirmedAt.Valid {
			// b.Comment += "\n(was not admin_confirmed)"

			b.AdminConfirmedAt = sql.NullTime{
				Time:  b.CreatedAt,
				Valid: true,
			}
		}
		if b.UserConfirmedAt.Valid {
			b.UserConfirmedAt.Valid = false
			fmt.Printf("Buchung %d was only 'bestätigt' (not user-confirmed)': %s\n", b.ID, b.HouseID)
			// return fmt.Errorf("booking %d (%s) user_confirmed", b.ID, status)
		}
		if b.CancelledAt.Valid {
			return fmt.Errorf("booking %d (%s) cancelled", b.ID, status)
		}

		if b.StartAt.Before(now) {
			// no user confirmation since then: cancel
			b.CancelledAt = sql.NullTime{
				Time:  now,
				Valid: true,
			}
		} else {
			// TODO: how to handle booking is only admin-confirmed?
			fmt.Printf("Buchung %d ist nur 'admin-confirmed': %s : %s - %s\n", b.ID, b.HouseID, b.StartAt.String()[:10], b.EndAt.String()[:10])
		}
	case "gebucht":
		if !b.AdminConfirmedAt.Valid {
			// b.Comment += "\n(was not admin_confirmed)"

			b.AdminConfirmedAt = sql.NullTime{
				Time:  b.CreatedAt,
				Valid: true,
			}
		}
		if !b.UserConfirmedAt.Valid {
			// b.Comment += "\n(was not user_confirmed)"

			b.UserConfirmedAt = sql.NullTime{
				Time:  b.AdminConfirmedAt.Time,
				Valid: true,
			}
		}
		if b.CancelledAt.Valid {
			// b.Comment += "\n(was cancelled\n)"
			b.CancelledAt.Valid = false
		}
	case "storniert":
		if !b.CancelledAt.Valid {
			b.CancelledAt = sql.NullTime{
				Time:  b.CreatedAt,
				Valid: true,
			}

			// b.Comment += "\n(was not cancelled)"
		}
	default:
		return fmt.Errorf("booking %d: unknown status: %q", b.ID, status)
	}
	return nil
}

func (imp importer) importBackup(folder string) error {
	fmt.Println("Kundenliste...")
	kunden, err := imp.readKundenlisteXLS(folder)
	if err != nil {
		return err
	}

	if _, ok := kunden[KundeID{}]; !ok {
		kunden[KundeID{}] = Kunde{} // empty customer for booking 2 and 3
	}

	fmt.Println("Reservierungen...")
	records, err := imp.readReservierungenXLS(folder, kunden)
	if err != nil {
		return err
	}

	// fmt.Println("Kunden...")
	// customers, err := imp.readKundendatenCSV(folder)
	// if err != nil {
	// 	return err
	// }
	// fmt.Println("Reservierungen...")
	// bookings, status, err := imp.readReservierungenCSV(folder, customers)
	// if err != nil {
	// 	return err
	// }
	bookings := make(map[int32]*database.Booking, len(records))
	for i, r := range records {
		bookings[i] = r.Booking
	}

	fmt.Println("Nachrichten...")
	emails, err := imp.readNachrichtenCSV(folder, bookings)
	if err != nil {
		return err
	}

	fmt.Println("Sortierung...")
	flatBookings := make([]*database.Booking, 0, len(bookings))
	for _, b := range bookings {
		flatBookings = append(flatBookings, b)
	}
	sort.Slice(flatBookings, func(i, j int) bool {
		return flatBookings[i].ID < flatBookings[j].ID
	})

	fmt.Println("Existierenden...")
	ctx := context.Background()
	ids, err := imp.db.ExistingBookings(ctx)
	if err != nil {
		return err
	}
	existing := make(map[int32]bool, len(ids))
	for _, i := range ids {
		existing[i] = true
	}

	logs := make(map[int32][]string, len(records))

	fmt.Println("Buchungen...")
	now := time.Now()
	for _, b := range flatBookings {
		// if b.EndAt.After(now) {
		// 	if b.Comment != "confirmation" {
		// 		fmt.Println(b.ID, b.Comment)
		// 	}
		// }
		err := adjustedStatus(b, records[b.ID].Status, now)
		if err != nil {
			return err
		}

		if string(b.PeopleOver) == "{}" {
			return fmt.Errorf("no people for %d", b.ID)
		}

		params := ImportBookingParams{
			HouseID:          b.HouseID,
			StartAt:          b.StartAt,
			EndAt:            b.EndAt,
			PeopleOver:       b.PeopleOver,
			Contact:          b.Contact,
			Email:            b.Email,
			Phone:            b.Phone,
			Landline:         b.Landline,
			Comment:          b.Comment,
			Invoice:          b.Invoice,
			CreatedAt:        b.CreatedAt,
			AdminConfirmedAt: b.AdminConfirmedAt,
			UserConfirmedAt:  b.UserConfirmedAt,
			CancelledAt:      b.CancelledAt,
			HashedToken:      b.HashedToken,
			ID:               b.ID,
		}
		if existing[b.ID] {
			err = imp.db.UpdateBooking(ctx, UpdateBookingParams(params))
		} else {
			err = imp.db.ImportBooking(ctx, params)
		}
		r := records[b.ID]
		if err != nil {
			return fmt.Errorf("booking %d (%s - %s): %s - %w", b.ID, b.StartAt, b.EndAt, r.Status, err)
		}

		appDB := database.New(imp.db.db)
		if !existing[b.ID] && len(r.ExtraChargesPercentages) > 0 {
			for id, percent := range r.ExtraChargesPercentages {
				err = appDB.BookingExtraChargesCreate(ctx, database.BookingExtraChargesCreateParams{
					BookingID: b.ID,
					ChargeID:  id,
					Percent:   percent,
				})
				if err != nil {
					return fmt.Errorf("booking extras %d:  %w", b.ID, err)
				}
			}
		}

		// if b.EndAt.After(time.Date(2022, time.April, 1, 0, 0, 0, 0, time.Local)) && !b.CancelledAt.Valid {
		if b.EndAt.After(now) && !b.CancelledAt.Valid {
			if price := r.PriceCents; price != 0 {
				offer, err := app.ComputeOffer(ctx, appDB, *b)
				if err != nil {
					return err
				}
				if offer.TotalAmount != price && b.HouseID != "seegatterl" {
					switch b.ID {
					case 2137, 2020: //  one night more than initially offered

					case 1983: // Preis = 0
					case 2151: // 100 € less
					default:
						fmt.Println("Buchung", b.ID, b.HouseID, "falschen Preis:", offer.TotalAmount, price, r.ExtraChargesPercentages)
					}
				}
			}
		}

		logs[b.ID] = r.Logs
	}

	fmt.Println("Kommentare...")
	existingLogsFlat, err := imp.db.ExistingLogs(ctx)
	if err != nil {
		return err
	}
	type logID struct {
		ID      int32
		Message string
	}
	existingLogs := make(map[logID]bool)
	for _, l := range existingLogsFlat {
		existingLogs[logID{l.BookingID, l.Message}] = true
	}

	for id, logs := range logs {
		for _, l := range logs {
			lid := logID{id, l}
			if existingLogs[lid] {
				continue
			}
			err := imp.db.InsertLog(ctx, InsertLogParams{
				BookingID: id,
				Message:   l,
			})
			if err != nil {
				return err
			}

		}
	}

	lastID := int32(2999)
	if !existing[lastID] {
		err = imp.db.ImportBooking(ctx, ImportBookingParams{
			HouseID:          "thalhaeusl-1og",
			StartAt:          time.Time{},
			EndAt:            time.Time{}.Add(time.Hour),
			PeopleOver:       []byte("{}"),
			Contact:          "Olivier Charvin",
			Email:            "oc@atelier.dev",
			Phone:            "0159 0244 9456",
			Comment:          "FAKE Buchung (sodass neues Buchungen 3000+ sind)",
			Invoice:          []byte("{}"),
			CreatedAt:        now,
			AdminConfirmedAt: sql.NullTime{},
			UserConfirmedAt:  sql.NullTime{},
			CancelledAt:      sql.NullTime{Valid: true, Time: now},
			HashedToken:      "",
			ID:               lastID,
		})
		if err != nil {
			return fmt.Errorf("could not insert last booking: %w", err)
		}
	}

	fmt.Println("E-Mails...")

	emailIds, err := imp.db.ExistingEmails(ctx)
	if err != nil {
		return err
	}
	emailExisting := make(map[string]bool, len(emailIds))
	for _, i := range emailIds {
		emailExisting[i] = true
	}

	for _, e := range emails {
		if e.BookingID <= 0 {
			continue
		}
		params := ImportEmailParams{
			ID:            e.ID,
			BookingID:     e.BookingID,
			HeaderFrom:    e.HeaderFrom,
			HeaderTo:      e.HeaderTo,
			HeaderSubject: e.HeaderSubject,
			TextPlain:     e.TextPlain,
			CreatedAt:     e.CreatedAt,
			SentAt: sql.NullTime{
				Valid: true,
				Time:  e.CreatedAt,
			},
		}
		if emailExisting[e.ID] {
			err = imp.db.UpdateEmail(ctx, UpdateEmailParams(params))
		} else {
			err = imp.db.ImportEmail(ctx, params)
		}
		if err != nil {
			return fmt.Errorf("could not import email %s: %w", e.ID, err)
		}
	}
	return nil
}

func readWindows1252CSV(path string, callback func([]string) error) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	r := transform.NewReader(f, charmap.Windows1252.NewDecoder())

	cr := csv.NewReader(r)
	cr.Comma = ';'
	cr.LazyQuotes = true

	// skip header
	if _, err = cr.Read(); err != nil {
		return err
	}

	for {
		rec, err := cr.Read()
		if err == io.EOF {
			return nil
		} else if err != nil {
			return err
		}
		if err = callback(rec); err != nil {
			return err
		}
	}
}

func adjustedDate(t time.Time, hourOfTheDayInMinutes int32) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), int(hourOfTheDayInMinutes/60), int(hourOfTheDayInMinutes%60), 0, 0, time.Local)
}
