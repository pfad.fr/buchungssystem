// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"context"
	"database/sql"
	"os"
	"testing"
	"time"

	"code.pfad.fr/buchungssystem/database"
	_ "github.com/mattn/go-sqlite3"
	"gotest.tools/v3/assert"
)

func newDatabase(t *testing.T, memName string) *sql.DB {
	dataSource := "file:" + memName + "?mode=memory&cache=shared&_journal_mode=WAL&_busy_timeout=5000&_foreign_keys=on"
	db, err := sql.Open("sqlite3", dataSource)
	assert.NilError(t, err)

	migrate, err := database.MissingMigrations(db, "sqlite3")
	assert.NilError(t, err)

	if migrate != nil {
		err := migrate()
		assert.NilError(t, err)

		f, err := os.ReadFile("../../database/seed/initial_data.sql")
		assert.NilError(t, err)
		_, err = db.Exec(string(f))
		assert.NilError(t, err)
	}

	return db
}

func TestImport(t *testing.T) {
	db := newDatabase(t, "import")
	imp := importer{
		db: New(db),
	}

	err := imp.importBackup("./backupdata")
	assert.NilError(t, err)

	bookings, err := database.New(db).BookingIndex(context.Background(), database.BookingIndexParams{
		HouseID:               "seegatterl",
		StartAt:               time.Date(3000, 1, 1, 0, 0, 0, 0, time.Local),
		EndAt:                 time.Date(2000, 1, 1, 0, 0, 0, 0, time.Local),
		IncludeUnconfirmed:    true,
		IncludeAdminConfirmed: true,
		IncludeUserConfirmed:  true,
		IncludeCancelled:      true,
		Limit:                 10,
	})
	assert.NilError(t, err)
	assert.Equal(t, 10, len(bookings))

	// re-import should not fail
	err = imp.importBackup("./backupdata")
	assert.NilError(t, err)
}
