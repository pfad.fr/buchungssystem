// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"fmt"
	"os"
	"strings"

	"golang.org/x/net/html"
)

func readXLS(path string, cb func([]string) error) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	doc, err := html.Parse(f)
	if err != nil {
		return err
	}

	line := 0
	var visit func(*html.Node) error
	visit = func(n *html.Node) error {
		if n.Type == html.ElementNode && n.Data == "tr" {
			line++
			var cells []string
			for c := n.FirstChild; c != nil; c = c.NextSibling {
				if c.Type == html.ElementNode && c.Data == "td" {
					var txt string
					if c.FirstChild != nil {
						txt = strings.TrimSpace(c.FirstChild.Data)
					}
					cells = append(cells, txt)
				}
			}
			if len(cells) > 0 {
				if err := cb(cells); err != nil {
					return fmt.Errorf("line %d: %w", line, err)
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			if err := visit(c); err != nil {
				return err
			}
		}
		return nil
	}
	return visit(doc)
}
