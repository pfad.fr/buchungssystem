<!--
SPDX-FileCopyrightText: 2022 Olivier Charvin

SPDX-License-Identifier: EUPL-1.2+
-->
# DPSG1300 Haus Buchung

The goal of this tool is to manage the online-booking of the DPSG1300 houses.

## Features

As a visitor, I want to

- see the bookable days for a given house
- get a quote for a given period and number of people
- send a booking request and get a confirmation
- get an invoice

As a manager, I want to

- adjust the availabilities of the house
- adjust the prices
- see the booking requests and act on them (confirm, reject, adjust)

### Non-features

The first version of this tool shall not concern itself with invoicing.
A connection to an existing tool to automatically create an offer/invoice would be however appreciable (eg https://www.invoiceninja.com/).
For instance:

- when an admin confirms the booking, create a new quote and send it to the user
- let the user, approve the quote and the terms on the invoiceninja portal (with signature)
- when the quote is signed on invoiceninja, reflect it in the booking interface

- send the welcome_email a couple of days before the stay

- transform the quote to an invoice on the last day of the stay
- inform the admin to adjust the invoice and send it to the user

## nice to have

- import from current system

Emails:
- AGBs + Pro-Forma Rechnung: nach der Bestätigung (= Vertrag)
- Willkommensemail / Hausordnung: 1 Woche vor dem Besuch
- Rechungsemail: nach der Abreise
- Stornogebühren: Sonderfall

- User-Email 2 days before expiration
- Admin-Email on user-confirmation?

- Datenschutz
- iframe resizing https://github.com/davidjbradshaw/iframe-resizer
- 12 months in advance for normal people
- 14 months in advance for scouts
- book by hour? (or for a day for Thalhäusl)
- Kurtaxe integration?
  - https://meldeschein.avs.de/reit-im-winkl/
  - https://github.com/omniboost/go-avs-meldeschein

- Rechnungsystem

# Migration strategy

- Choose one house to migrate
- Publish a message on DPSG1300 informing of the migration
- Prepare prices for BOTH houses
- Disable bookings on Monday: https://live.freizeitplan.net/admin/ressources/overview
- Make 3 exports:
  * export (incl. emails): https://live.freizeitplan.net/admin/clients/backup
  * Buchungen: https://live.freizeitplan.net/admin/Reservations/index/canceled:1/history:1/output:xls
  * Kontaktdaten: https://live.freizeitplan.net/admin/Users/index/output:xls
- Import files
- Check prices
- Send email to all upcoming bookings for THIS HOUSE, to inform about the new platform (with link to view booking)
- Embed the new iFrame
- Enjoy (and repeat for the other house)

# Questions

- E-Mail Verkehr
- Belegungsvertrag vs Bestätigung

AZURE:
- add https://buchung.dpsgm.de/ as redirect option (for logout)
- add https://buchung.dpsgm.de/auth/callback as redirect option (because nicer)

Import:
- Storno E-Mail für alte Buchungen
- Datenschutz

# TO-DO

Manually anonymize a past or cancelled booking.

## Better show the booking steps

1. Anfrage
2. Bestätigung
3. Vertragsschluss
4. Aufenhalt
5. Zahlung

# DONE

## DONE as of 2023-06-22

admin overview (overday): link to bookings of the day (+ indicator)
admin (overday): filter bookings by day

## DONE as of 2023-06-21

new booking (overday): add booking indicator on days
booking overday: prevent "found overlapping booking"

## DONE as of 2023-06-20

basic booking overday

## DONE as of 2023-02-02

anonymize cancelled bookings after 7 days
anonymize bookings after 365 days

## DONE as of 2022-12-08

List bookings having a given price option.

## DONE as of 2022-12-07

Use Azure certificate
Add banner informing admin to renew the azure certificate

## DONE as of 2022-11-08

Rename Preisliste to Preissaison.
Add link to bookings with a given price (and hide the message when no booking as been made yet).

## DONE as of 2022-10-26

Add log of frontend price + price on option change.
Search by pricelist

## DONE as of 2022-10-25

- export as CSV (utf16le_bom_tab)
https://wiki.scn.sap.com/wiki/display/ABAP/CSV+tests+of+encoding+and+column+separator?original_fqdn=wiki.sdn.sap.com

## DONE as of 2022-10-20

- save/sync extraCharges in DB
- price check on import

## DONE as of 2022-10-19 (Shared in Teams on the same day)

- make reset-db: populate prices
- Extrarequests: not hard-coded anymore ;)
- Extrarequests: admin-editable
- Bookable: 13 months in advance for public users

## DONE as of 2022-10-18

- Import from XLS (Kundendaten und Reservierung)
- Import people
- Import: Link + Status als Kommentar
- Import: adjut dates depending on E-Mail dates (created, {admin, user}_confirmed, cancelled) from E-Mail
- Booking: add landline phone

## DONE as of 2022-10-15 (Shared in Teams)

- Import: email
- Import: mutliple times
- Import: fake id 2999 (for auto-increment hack)
- Import: invoice data
- Import: cancel past unconfirmed bookings
- Import: prevent reverting booking status
- Import: prevent multiple email on re-import
- Import: email with minute for ordering

## DONE as of 2022-10-14

- Anpassbare Bestätigung
- Fix sonder Anreisezeiten Logik
- Buchungbestätigung mit Nachricht.

## DONE as of 2022-10-07

- Footer mit Quelltext link
- Kein "Admin-Buchbarkeit" bei Übernachtungsnächte
- edit ruhetage with simple edit right

## DONE as of 2022-10-06

- Rename "Bemerkung" zu "Zweck der Veranstalung / Bemerkungen"
- An-/Abreise Uhrzeit pro Haus änderbar
- Schule ab 9 Uhr in der Früh= kein Booking am Nacht davor möglich
- Putz-/Baustellpause einfach hinzufügen

## DONE as of 2022-10-04

- Admin: Abrechnungsdaten ändern

## DONE as of 2022-08-11

- visitor interface to create a booking

## DONE as of 2022-07-05

+ allow admins to change number of people
+ "7 Tage" ist nicht immer richtig
+ Link zu Preisliste von Buchung
+ Warnung wenn Preisliste im Einsatz
+ Häckchen für änderung von Buchungsnächte

## as of 2022-07-03

+ remove booking when expired
+ admin comments
+ log trail
+ display price on user email
+ display price on user confirmation page
+ hide extra charge button when not .CanEdit
+ add extra charges to offer (with Amounts)

+ display price on admin page
+ allow admins to add discounts
+ allow admins to change dates
+ count number of bookings from a given email

## Database schema

### houses

- id
- name

### periods

- id
- name

### bookable_days

- evening
- house_id
- period_id (+house_id => charges)

### charges

- id
- label
- description
- house_id
- period_id
- unit (per_person_night/per_stay)
- amount (in cents - can be negative)
- min_person_charged (charge for 20 persons, even if only 10 present)
- exempted_under_person_count (charged only if more than X persons)
- exempted_under_age (charged only for persons older than...)
- is_extra (for DPSG discount: not applied automatically)

### booking_extra_charges (DPSG Stamm 1300)

- id
- charge_id
- booking_id

### booking

- id
- house_id
- first_day
- last_day
- quote_link
- invoice_link

- created_at
- email_clicked_at
- admin_accepted_at
- quote_accepted_at
- rejected_at
- rejection_reason
- cancelled_at
- cancellation_reason

- welcome_email_sent_at

- contact_name
- contact_email

- discount ?
- persons (by age?)
- billing_infos (JSON?)

# invoice ninja:

POST https://demo.invoiceninja.com/api/v1/clients?include=gateway_tokens,activities,ledger,system_logs,documents

```json
{
  "group_settings_id": "",
  "name": "Firma ABC",
  "display_name": "",
  "balance": 0,
  "credit_balance": 0,
  "paid_to_date": 0,
  "client_hash": "",
  "address1": "Street1",
  "address2": "apt",
  "city": "city",
  "state": "",
  "postal_code": "12345",
  "country_id": "276",
  "phone": "",
  "private_notes": "",
  "public_notes": "",
  "website": "",
  "industry_id": "",
  "size_id": "",
  "vat_number": "",
  "id_number": "",
  "number": "",
  "shipping_address1": "",
  "shipping_address2": "",
  "shipping_city": "",
  "shipping_state": "",
  "shipping_postal_code": "",
  "shipping_country_id": "",
  "settings": { "language_id": "3", "currency_id": "3" },
  "last_login": 0,
  "custom_value1": "",
  "custom_value2": "",
  "custom_value3": "",
  "custom_value4": "",
  "contacts": [
    {
      "first_name": "First",
      "last_name": "",
      "email": "test@test.com",
      "password": "",
      "phone": "123",
      "contact_key": "",
      "is_primary": true,
      "send_email": true,
      "custom_value1": "",
      "custom_value2": "",
      "custom_value3": "",
      "custom_value4": "",
      "last_login": 0,
      "link": "",
      "created_at": 0,
      "updated_at": 0,
      "archived_at": 0,
      "id": "-132",
      "isChanged": false,
      "is_deleted": false,
      "user_id": "",
      "assigned_user_id": ""
    }
  ],
  "activities": [],
  "ledger": [],
  "gateway_tokens": [],
  "documents": [],
  "system_logs": [],
  "created_at": 0,
  "updated_at": 0,
  "archived_at": 0,
  "id": "-131",
  "loadedAt": 0,
  "isChanged": true,
  "is_deleted": false,
  "user_id": "",
  "assigned_user_id": ""
}
```

response

```json
{
  "data": {
    "id": "LYqaQlenjk"
    ...
  }
}
```

POST https://demo.invoiceninja.com/api/v1/quotes?include=history,activities

```json
{
  "amount": 0,
  "balance": 0,
  "paid_to_date": 0,
  "client_id": "mWZdPwbKgR",
  "project_id": "",
  "vendor_id": "",
  "subscription_id": "",
  "status_id": "1",
  "number": "",
  "discount": 0,
  "po_number": "",
  "date": "2022-04-07",
  "due_date": "",
  "public_notes": "Buchung 123",
  "private_notes": "",
  "terms": "Terms",
  "footer": "",
  "design_id": "",
  "uses_inclusive_taxes": false,
  "tax_name1": "",
  "tax_rate1": 0,
  "tax_name2": "",
  "tax_rate2": 0,
  "tax_name3": "",
  "tax_rate3": 0,
  "is_amount_discount": false,
  "partial": 0,
  "total_taxes": 0,
  "partial_due_date": "",
  "custom_value1": "",
  "custom_value2": "",
  "custom_value3": "",
  "custom_value4": "",
  "custom_surcharge1": 0,
  "custom_surcharge2": 0,
  "custom_surcharge3": 0,
  "custom_surcharge4": 0,
  "custom_surcharge_tax1": false,
  "custom_surcharge_tax2": false,
  "custom_surcharge_tax3": false,
  "custom_surcharge_tax4": false,
  "exchange_rate": 1,
  "last_sent_date": "",
  "next_send_date": "2022-04-07",
  "auto_bill_enabled": false,
  "line_items": [
    {
      "product_key": "übernachtung",
      "notes": "",
      "cost": 12,
      "product_cost": 0,
      "quantity": 16,
      "tax_name1": "",
      "tax_rate1": 0,
      "tax_name2": "",
      "tax_rate2": 0,
      "tax_name3": "",
      "tax_rate3": 0,
      "custom_value1": "",
      "custom_value2": "",
      "custom_value3": "",
      "custom_value4": "",
      "discount": 0,
      "type_id": "1",
      "createdAt": 1649322944123000
    },
    {
      "product_key": "heizung",
      "notes": "desc",
      "cost": 3,
      "product_cost": 0,
      "quantity": 10,
      "tax_name1": "",
      "tax_rate1": 0,
      "tax_name2": "",
      "tax_rate2": 0,
      "tax_name3": "",
      "tax_rate3": 0,
      "custom_value1": "",
      "custom_value2": "",
      "custom_value3": "",
      "custom_value4": "",
      "discount": 0,
      "type_id": "1",
      "createdAt": 1649322960415000
    }
  ],
  "invitations": [
    {
      "key": "",
      "link": "",
      "client_contact_id": "wMvbmwOeYA",
      "sent_date": "",
      "viewed_date": "",
      "opened_date": "",
      "created_at": 0,
      "updated_at": 0,
      "archived_at": 0,
      "id": "-172",
      "isChanged": false,
      "is_deleted": false
    }
  ],
  "documents": [],
  "activities": [],
  "created_at": 0,
  "updated_at": 0,
  "archived_at": 0,
  "id": "-163",
  "reminder1_sent": "",
  "reminder2_sent": "",
  "reminder3_sent": "",
  "reminder_last_sent": "",
  "frequency_id": "5",
  "remaining_cycles": -1,
  "due_date_days": "terms",
  "invoice_id": "",
  "filename": "",
  "recurring_dates": [],
  "loadedAt": 0,
  "isChanged": true,
  "is_deleted": false,
  "user_id": "",
  "assigned_user_id": "",
  "entity_type": "quote"
}
```

response

```json
{
  "data": {
    "id": "KGRb41dBLZ",
    "user_id": "Wpmbk5ezJn",
    "assigned_user_id": "",
    "amount": 222,
    "balance": 0,
    "client_id": "mWZdPwbKgR",
    "status_id": "1",
    "design_id": "Wpmbk5ezJn",
    "invoice_id": "",
    "vendor_id": "",
    "updated_at": 1649322998,
    "archived_at": 0,
    "created_at": 1649322998,
    "number": "0026",
    "discount": 0,
    "po_number": "",
    "date": "2022-04-07",
    "last_sent_date": "",
    "next_send_date": "",
    "reminder1_sent": "",
    "reminder2_sent": "",
    "reminder3_sent": "",
    "reminder_last_sent": "",
    "due_date": "",
    "terms": "Terms",
    "public_notes": "Buchung 123",
    "private_notes": "",
    "is_deleted": false,
    "uses_inclusive_taxes": false,
    "tax_name1": "",
    "tax_rate1": 0,
    "tax_name2": "",
    "tax_rate2": 0,
    "tax_name3": "",
    "tax_rate3": 0,
    "total_taxes": 0,
    "is_amount_discount": false,
    "footer": "",
    "partial": 0,
    "partial_due_date": "",
    "custom_value1": "",
    "custom_value2": "",
    "custom_value3": "",
    "custom_value4": "",
    "has_tasks": false,
    "has_expenses": false,
    "custom_surcharge1": 0,
    "custom_surcharge2": 0,
    "custom_surcharge3": 0,
    "custom_surcharge4": 0,
    "custom_surcharge_tax1": false,
    "custom_surcharge_tax2": false,
    "custom_surcharge_tax3": false,
    "custom_surcharge_tax4": false,
    "line_items": [
      {
        "product_key": "\u00fcbernachtung",
        "notes": "",
        "cost": 12,
        "product_cost": 0,
        "quantity": 16,
        "tax_name1": "",
        "tax_rate1": 0,
        "tax_name2": "",
        "tax_rate2": 0,
        "tax_name3": "",
        "tax_rate3": 0,
        "custom_value1": "",
        "custom_value2": "",
        "custom_value3": "",
        "custom_value4": "",
        "discount": 0,
        "type_id": "1",
        "createdAt": 1649322944123000,
        "is_amount_discount": false,
        "sort_id": "0",
        "line_total": "192.00",
        "gross_line_total": 192,
        "date": ""
      },
      {
        "product_key": "heizung",
        "notes": "desc",
        "cost": 3,
        "product_cost": 0,
        "quantity": 10,
        "tax_name1": "",
        "tax_rate1": 0,
        "tax_name2": "",
        "tax_rate2": 0,
        "tax_name3": "",
        "tax_rate3": 0,
        "custom_value1": "",
        "custom_value2": "",
        "custom_value3": "",
        "custom_value4": "",
        "discount": 0,
        "type_id": "1",
        "createdAt": 1649322960415000,
        "is_amount_discount": false,
        "sort_id": "0",
        "line_total": "30.00",
        "gross_line_total": 30,
        "date": ""
      }
    ],
    "entity_type": "quote",
    "exchange_rate": 1,
    "paid_to_date": 0,
    "project_id": "",
    "subscription_id": "",
    "invitations": [
      {
        "id": "7LDdwjRb1Y",
        "client_contact_id": "wMvbmwOeYA",
        "key": "zcLRaVK3gFVWit8HhKToQoMNzbQbInzg",
        "link": "https://demo.invoiceninja.com/client/quote/zcLRaVK3gFVWit8HhKToQoMNzbQbInzg",
        "sent_date": "",
        "viewed_date": "",
        "opened_date": "",
        "updated_at": 1649322998,
        "archived_at": 0,
        "created_at": 1649322998,
        "email_status": "",
        "email_error": ""
      }
    ],
    "documents": [],
    "activities": []
  }
}
```
