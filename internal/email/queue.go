// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package email

import (
	"bytes"
	"context"
	"crypto/rand"
	"database/sql"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"github.com/emersion/go-message/mail"
)

func NewQueue(ctx context.Context, db *database.Queries, from mail.Address, resumeAfter time.Time, sendEmail func(from string, to []string, r io.Reader) error) (Queue, error) {
	pending, err := db.EmailPending(ctx, resumeAfter)
	if err != nil {
		return Queue{}, err
	}

	q := Queue{
		DB:       db,
		SendMail: sendEmail,
		From:     from,
		Hostname: "buchung.dpsg1300.de",        // TODO: as argument?
		ch:       make(chan database.Email, 1), // 1 because usually 2 emails are sent in a row
		done:     make(chan struct{}),
	}
	go q.process(ctx.Done()) //nolint:contextcheck

	if len(pending) > 0 {
		go func() {
			for _, email := range pending {
				q.ch <- email
			}
		}()
	}
	return q, nil
}

type Queue struct {
	DB       *database.Queries
	SendMail func(from string, to []string, r io.Reader) error
	From     mail.Address
	Hostname string // Hostname is used for Message-Id
	ch       chan database.Email
	done     chan struct{}
}

// Done returns a channel that's closed when the queue processing is stopped.
func (q Queue) Done() <-chan struct{} {
	return q.done
}

func (q Queue) SendErrorf(format string, v ...interface{}) {
	// TODO: proper logging for email sending error
	// likely temporary ?
	log.Printf(format, v...)
}
func (q Queue) DatabaseErrorf(format string, v ...interface{}) {
	// TODO: proper logging for database saving error
	log.Printf(format, v...)
}

func (q Queue) process(done <-chan struct{}) {
	defer close(q.done)
	for {
		select {
		case <-done:
			return
		case email := <-q.ch:
			ctx := context.Background()
			var b bytes.Buffer
			to, err := WriteTo(&b, email, q.From, q.Hostname)
			if err != nil {
				dbErr := q.DB.EmailUpdate(ctx, database.EmailUpdateParams{
					ID:         email.ID,
					FatalError: err.Error(),
					SentAt:     sql.NullTime{},
				})
				if dbErr != nil {
					q.DatabaseErrorf("email %s failure could not be saved to DB: %#v", email.ID, err)
				}
				continue
			}
			err = q.SendMail(q.From.Address, to, &b)
			if err != nil {
				q.SendErrorf("email %s to %s could not be sent: %#v", email.ID, to, err)
			} else {
				dbErr := q.DB.EmailUpdate(ctx, database.EmailUpdateParams{
					ID:         email.ID,
					FatalError: "",
					SentAt: sql.NullTime{
						Time:  time.Now(),
						Valid: true,
					},
				})
				if dbErr != nil {
					q.DatabaseErrorf("email %s sent could not be saved to DB: %#v", email.ID, err)
				}
			}
		}
	}
}

func (q Queue) Push(ctx context.Context, email database.Email) error {
	// sanity check
	_, errContent := WriteTo(io.Discard, email, q.From, q.Hostname)
	var fatalError string
	if errContent != nil {
		fatalError = errContent.Error()
	}

	id, err := generateMessageID(q.Hostname)
	if err != nil {
		return err
	}
	// overwrite email to adjust created_at
	email, err = q.DB.EmailCreate(ctx, database.EmailCreateParams{
		ID:            id,
		BookingID:     email.BookingID,
		HeaderFrom:    email.HeaderFrom,
		HeaderTo:      email.HeaderTo,
		HeaderSubject: email.HeaderSubject,
		TextPlain:     email.TextPlain,

		FatalError: fatalError,
	})
	if err != nil {
		return err
	}
	if errContent != nil {
		return errContent
	}

	select {
	case q.ch <- email:
	default:
		// attempt to send in goroutine
		go func() {
			q.ch <- email
		}()
	}

	return nil
}

func WriteTo(dst io.Writer, email database.Email, from mail.Address, hostname string) ([]string, error) {
	var to []string
	// header
	var h mail.Header
	{
		h.SetMessageID(email.ID)
		h.SetDate(email.CreatedAt)
		h.SetAddressList("From", []*mail.Address{&from})

		if email.HeaderFrom != "" {
			replyTo, err := mail.ParseAddress(email.HeaderFrom)
			if err != nil {
				return nil, err
			}
			h.SetAddressList("Reply-To", []*mail.Address{replyTo})
		}

		toA, err := mail.ParseAddress(email.HeaderTo)
		if err != nil {
			return nil, err
		}
		h.SetAddressList("To", []*mail.Address{toA})
		to = []string{toA.Address}

		h.SetSubject(email.HeaderSubject)
		if email.BookingID != 0 {
			h.SetText("References", fmt.Sprintf("<anfrage/%d@%s>", email.BookingID, hostname))
		}
	}

	mw, err := mail.CreateWriter(dst, h)
	if err != nil {
		return nil, err
	}

	// text/plain part
	if err = writePlain(email.TextPlain, mw); err != nil {
		return nil, err
	}

	if err := mw.Close(); err != nil {
		return nil, err
	}

	return to, nil
}

func writePlain(text string, mw *mail.Writer) error {
	if text == "" {
		return nil
	}

	tw, err := mw.CreateInline()
	if err != nil {
		return err
	}
	var th mail.InlineHeader
	th.Set("Content-Type", "text/plain")
	w, err := tw.CreatePart(th)
	if err != nil {
		return err
	}

	if _, err = io.WriteString(w, text); err != nil {
		return err
	}
	if err = w.Close(); err != nil {
		return err
	}
	if err = tw.Close(); err != nil {
		return err
	}
	return nil
}

// adapted from https://github.com/emersion/go-message/blob/c0463a933756769445a801d21481026893c000ed/mail/header.go
// GenerateMessageID generates an RFC 2822-compliant Message-Id based on the
// informational draft "Recommendations for generating Message IDs", for lack
// of a better authoritative source.
func generateMessageID(hostname string) (string, error) {
	now := uint64(time.Now().UnixNano())

	nonceByte := make([]byte, 8)
	if _, err := rand.Read(nonceByte); err != nil {
		return "", err
	}
	nonce := binary.BigEndian.Uint64(nonceByte)

	return base36(now) + "." + base36(nonce) + "@" + hostname, nil
}

func base36(input uint64) string {
	return strings.ToUpper(strconv.FormatUint(input, 36))
}
