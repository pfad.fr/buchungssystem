// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package internal

import (
	"html"
	"log"
	"net/http"
)

type ErrorHandler struct {
	Debug bool
}

// Wrap an http handler with an error.
func (e ErrorHandler) Wrap(handler func(w http.ResponseWriter, r *http.Request) error) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		err := handler(w, r)
		if err == nil {
			return
		}
		log.Println("TODO ERROR handling", r.URL.String())
		log.Println(err)
		log.Printf("%#v\n", err)
		if e.Debug {
			// output some HTML for the livereload to be injected
			w.Header().Set("Content-Type", "text/html; charset=utf-8")
			w.Header().Set("X-Content-Type-Options", "nosniff")
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(`<!DOCTYPE html><html><head><title>Error 500</title></head><body style="font-family: monospace;">`))
			w.Write([]byte(html.EscapeString(err.Error())))
			return
		}
		http.Error(w, "Something went wrong. If the problem persists, please email the webmaster", http.StatusInternalServerError)
	})
}
