// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package internal

import (
	"database/sql"
	"embed"
	"errors"
	"net/http"
	"strconv"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal/email"
	"github.com/mattn/go-sqlite3"
)

//go:embed assets views emails
var EmbedFS embed.FS

type Handler struct {
	BaseURL         string
	DB              *database.Queries
	Queue           email.Queue
	CurrentUser     func(r *http.Request) (*database.User, error)
	ErrorHandler    ErrorHandler
	RenderErrorView func(w http.ResponseWriter, r *http.Request, data interface{}) error
}

func (Handler) RedirectBack(w http.ResponseWriter, r *http.Request) error { //nolint:unparam
	http.Redirect(w, r, r.Header.Get("Referer"), http.StatusFound)
	return nil
}

func (h Handler) HousePolicy(r *http.Request) *HousePolicy {
	user, userErr := h.CurrentUser(r)
	return NewHousePolicy(r.Context(), user, userErr, h.DB)
}

func (h Handler) UserPolicy(r *http.Request) *UserPolicy {
	user, userErr := h.CurrentUser(r)
	return NewUserPolicy(r.Context(), user, userErr)
}

func (h Handler) HandleErr(wrapped func(http.ResponseWriter, *http.Request) error) http.HandlerFunc {
	return h.ErrorHandler.Wrap(func(w http.ResponseWriter, r *http.Request) error {
		err := wrapped(w, r)
		if err == nil {
			return nil
		}

		if errors.Is(err, sql.ErrNoRows) {
			err = HTTPError{
				Code:    http.StatusNotFound,
				Message: "Nicht gefunden",
			}
		}

		var sqliteErr sqlite3.Error
		if errors.As(err, &sqliteErr) {
			if sqliteErr.ExtendedCode == sqlite3.ErrConstraintCheck {
				err = HTTPError{
					Code:    http.StatusBadRequest,
					Message: "Ungültige Angaben",
				}
			}
		}

		var herr HTTPError
		if errors.As(err, &herr) {
			w.WriteHeader(herr.Code)
			user, _ := h.CurrentUser(r)
			return h.RenderErrorView(w, r, struct {
				User            *database.User
				Nav             NavbarData
				Title, Subtitle string
			}{
				User: user,
				Nav: NavbarData{
					User:       user,
					SearchTerm: r.FormValue("q"),
				},
				Title:    herr.Message,
				Subtitle: "Fehler " + strconv.Itoa(herr.Code),
			})
		}
		return err
	}).ServeHTTP
}

type NavbarData struct {
	User             *database.User
	SelectedDropdown string
	SelectedURI      string
	SearchTerm       string
}

type HTTPError struct {
	Code    int
	Message string
}

func (h HTTPError) Error() string {
	return h.Message
}

type Options []struct {
	ID   string
	Name string
}

func (oo Options) Name(id string) string {
	for _, o := range oo {
		if o.ID == id {
			return o.Name
		}
	}
	return "<" + id + ">"
}

func (oo Options) Contains(id string) bool {
	for _, o := range oo {
		if o.ID == id {
			return true
		}
	}
	return false
}
