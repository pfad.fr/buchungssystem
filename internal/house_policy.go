// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package internal

import (
	"context"
	"database/sql"
	"net/http"

	"code.pfad.fr/buchungssystem/database"
)

var HouseBookingTypes = Options{
	{"overnight", "Übernachtung"},
	{"overday", "Tagsüber"},
}
var HouseRoles = Options{
	{"", "Keine"},
	{"booking_create", "Buchungen im Voraus erstellen"},
	{"booking_read", "Buchungen ansehen"},
	{"booking_admin", "Buchungen verwalten"},
	{"admin", "Haus verwalten"},
}

func NewHousePolicy(ctx context.Context, user *database.User, userErr error, db *database.Queries) *HousePolicy {
	return &HousePolicy{
		ctx:     ctx,
		user:    user,
		userErr: userErr,
		db:      db,
		roles:   make(map[string]string),
	}
}

type HousePolicy struct {
	ctx     context.Context
	user    *database.User
	userErr error

	db    *database.Queries
	roles map[string]string
}

func (hp HousePolicy) CanCreate() error {
	return hp.hasUserRole("admin")
}

func (hp HousePolicy) CanBookInAdvance(houseID string) error {
	return hp.hasHouseRole(houseID, "admin", "booking_admin", "booking_read", "booking_create")
}
func (hp HousePolicy) CanViewBooking(houseID string) error {
	return hp.hasHouseRole(houseID, "admin", "booking_admin", "booking_read")
}
func (hp HousePolicy) CanEditBooking(houseID string) error {
	return hp.hasHouseRole(houseID, "admin", "booking_admin")
}

func (hp HousePolicy) CanEditHouse(houseID string) error {
	return hp.hasHouseRole(houseID, "admin")
}

func (hp HousePolicy) CanEditRestDay(houseID string) error {
	return hp.CanEditBooking(houseID)
}

func (hp HousePolicy) CanEditExtraRequest(houseID string) error {
	return hp.CanEditHouse(houseID)
}

func (hp HousePolicy) CanEditCharge(houseID string) error {
	return hp.CanEditHouse(houseID)
}

func (hp HousePolicy) CanEditDay(houseID string) error {
	return hp.CanEditHouse(houseID)
}

func (hp HousePolicy) Filter(houses []database.House, policy func(string) error) []database.House {
	filtered := make([]database.House, 0, len(houses))
	for _, h := range houses {
		if policy(h.ID) != nil {
			continue
		}
		filtered = append(filtered, h)
	}
	return filtered
}

func (hp HousePolicy) denied() error {
	return HTTPError{
		Code:    http.StatusForbidden,
		Message: "Fehlende Berechtigungen",
	}
}

func (hp HousePolicy) hasUserRole(roles ...string) error {
	if hp.userErr != nil {
		return hp.userErr
	}
	for _, r := range roles {
		if r == hp.user.Role {
			return nil
		}
	}
	return hp.denied()
}

func (hp HousePolicy) hasHouseRole(houseID string, roles ...string) error {
	role, err := hp.fetchRole(houseID)
	if err != nil {
		return err
	}
	for _, r := range roles {
		if r == role {
			return nil
		}
	}
	return hp.denied()
}

func (hp HousePolicy) fetchRole(houseID string) (string, error) {
	if role, ok := hp.roles[houseID]; ok {
		return role, nil
	}
	if hp.userErr != nil {
		return "", hp.userErr
	}
	role, err := hp.db.SettingHouseUserRole(hp.ctx, database.SettingHouseUserRoleParams{
		HouseID: houseID,
		UserID:  hp.user.ID,
	})
	if err == sql.ErrNoRows { //nolint:errorlint
		role, err = "", nil
	}
	if err != nil {
		return "", err
	}
	hp.roles[houseID] = role
	return role, nil
}
