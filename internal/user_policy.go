// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package internal

import (
	"context"
	"net/http"

	"code.pfad.fr/buchungssystem/database"
)

func NewUserPolicy(ctx context.Context, user *database.User, userErr error) *UserPolicy {
	return &UserPolicy{
		ctx:     ctx,
		user:    user,
		userErr: userErr,
	}
}

type UserPolicy struct {
	ctx     context.Context
	user    *database.User
	userErr error
}

func (up UserPolicy) CanManage() error {
	return up.hasRole("admin")
}

func (up UserPolicy) denied() error {
	return HTTPError{
		Code:    http.StatusForbidden,
		Message: "Fehlende Berechtigungen",
	}
}

func (up UserPolicy) hasRole(roles ...string) error {
	if up.userErr != nil {
		return up.userErr
	}
	for _, r := range roles {
		if r == up.user.Role {
			return nil
		}
	}
	return up.denied()
}
