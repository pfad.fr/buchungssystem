// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"net/http"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
)

const dateLayout = "2006-01-02"

func (a app) BookingNew(rw http.ResponseWriter, r *http.Request) error {
	self, err := a.CurrentUser(r)
	if err != nil {
		return err
	}

	houses, err := a.DB.SettingBookableHouseIndex(r.Context(), self.ID)
	if err != nil {
		return err
	}

	switch len(houses) {
	case 0:
		return internal.HTTPError{
			Code:    http.StatusForbidden,
			Message: "Fehlende Berechtigungen",
		}
	case 1:
		// redirect
		http.Redirect(rw, r, "/neue/"+houses[0].ID, http.StatusFound)
		return nil
	}

	return a.BookingNewView.Render(rw, r, struct {
		Nav    internal.NavbarData
		Houses []database.SettingBookableHouseIndexRow
	}{
		Nav: internal.NavbarData{
			User:             self,
			SelectedDropdown: "buchung",
			SelectedURI:      "buchung/erstellen",
		},
		Houses: houses,
	})
}
