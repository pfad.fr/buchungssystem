// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"encoding/json"
	"io/fs"
	"net/http"
	"strconv"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/buchungssystem/internal/view"
	"github.com/emersion/go-message/mail"
	"github.com/go-chi/chi/v5"
)

func OwnBookingRouter(baseHandler internal.Handler, assetsFS fs.FS, notification notification) func(r chi.Router) {
	return func(r chi.Router) {
		h := ownBookingHandler{
			Handler:           baseHandler,
			BookingView:       view.NewTemplate(assetsFS, "doctype", "layout/own", "own/view"),
			BookingNew:        view.NewTemplate(assetsFS, "doctype", "layout/own", "own/new"),
			BookingNewOverday: view.NewTemplate(assetsFS, "doctype", "layout/own", "own/new_overday"),
			notification:      notification,
		}

		r.Get("/neue/{house_id}", h.HandleErr(h.New))
		r.Post("/neue/{house_id}", h.HandleErr(h.New))

		r.Get("/eigene/anfrage", h.HandleErr(h.View))
		r.Post("/eigene/anfrage", h.HandleErr(h.Edit))
	}
}

type ownBookingHandler struct {
	internal.Handler
	BookingView       *view.Template
	BookingNew        *view.Template
	BookingNewOverday *view.Template
	notification      notification
}

func (h ownBookingHandler) findBooking(r *http.Request) (database.Booking, error) {
	i, err := strconv.ParseInt(r.URL.Query().Get("id"), 10, 32)
	if err != nil {
		return database.Booking{}, internal.HTTPError{
			Code:    http.StatusBadRequest,
			Message: "Fehlerhafte Anfrage",
		}
	}
	booking, err := h.DB.BookingFind(r.Context(), int32(i))
	if err != nil {
		return database.Booking{}, err
	}
	err = checkHashedToken(r.URL.Query().Get("token"), booking.HashedToken)
	if err != nil {
		return database.Booking{}, err
	}

	return booking, nil
}

func (h ownBookingHandler) View(w http.ResponseWriter, r *http.Request) error {

	booking, err := h.findBooking(r)
	if err != nil {
		return err
	}
	house, err := h.DB.SettingHouseFind(r.Context(), booking.HouseID)
	if err != nil {
		return err
	}

	return h.renderBooking(w, r, booking, house)
}

func (h ownBookingHandler) renderBooking(w http.ResponseWriter, r *http.Request, booking database.Booking, house database.House) error {
	self, _ := h.CurrentUser(r) // may be nil

	var billing BillingInfo
	err := json.Unmarshal(booking.Invoice, &billing)
	if err != nil {
		return err
	}

	ages, totalPeople, sortedAges, err := unmarshalAges(booking.PeopleOver)
	if err != nil {
		return err
	}

	offer, err := computeOfferUnmarshalled(r.Context(), h.DB, booking, totalPeople, ages)
	if err != nil {
		return err
	}

	return h.BookingView.Render(w, r, struct {
		User        *database.User
		Booking     database.Booking
		House       database.House
		Billing     BillingInfo
		KindName    string
		CountryName string
		NightsCount int
		SortedAges  []int
		Ages        map[int32]int
		TotalPeople int
		Status      bookingStatus
		Offer       Offer
	}{
		User:        self,
		Booking:     booking,
		House:       house,
		Billing:     billing,
		KindName:    billingKinds.Name(billing.Kind),
		CountryName: billingCountries.Name(billing.Country),
		NightsCount: countNights(booking.StartAt, booking.EndAt),
		SortedAges:  sortedAges,
		Ages:        ages,
		TotalPeople: totalPeople,
		Status:      newBookingStatus(booking, house.DaysForUserConfirmation),
		Offer:       offer,
	})
}

func (h ownBookingHandler) Edit(w http.ResponseWriter, r *http.Request) error {
	booking, err := h.findBooking(r)
	if err != nil {
		return err
	}

	if !booking.UserConfirmedAt.Valid {
		if r.FormValue("confirm") == "1" {
			return h.userConfirmBooking(w, r, booking)
		}
		return internal.HTTPError{
			Code: http.StatusUnprocessableEntity,
		}
	}
	return internal.HTTPError{
		Code: http.StatusMethodNotAllowed,
	}
}

func (h ownBookingHandler) userConfirmBooking(w http.ResponseWriter, r *http.Request, booking database.Booking) error {
	house, err := h.DB.SettingHouseFind(r.Context(), booking.HouseID)
	if err != nil {
		return err
	}

	subject, body, err := h.notification.userEmail.BookingConfirmedByUser(struct {
		Booking database.Booking
		House   database.House
		Date    time.Time
		IP      string
	}{
		Booking: booking,
		House:   house,
		Date:    timeNow(),
		IP:      r.RemoteAddr,
	})

	if err != nil {
		return err
	}

	msg := database.Email{
		BookingID: booking.ID,
		HeaderTo: (&mail.Address{
			Name:    booking.Contact,
			Address: booking.Email,
		}).String(),
		HeaderFrom:    house.ReplyTo,
		HeaderSubject: subject,
		TextPlain:     string(body),
	}
	if err = h.Queue.Push(r.Context(), msg); err != nil {
		return err
	}

	err = h.DB.BookingUserConfirm(r.Context(), booking.ID)
	if err != nil {
		return err
	}
	return h.RedirectBack(w, r)
}
