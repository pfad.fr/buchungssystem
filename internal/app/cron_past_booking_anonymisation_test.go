// SPDX-FileCopyrightText: 2023 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"context"
	"database/sql"
	"encoding/json"
	"os"
	"sync"
	"testing"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/buchungssystem/internal/email"
	"github.com/emersion/go-message/mail"
	"gotest.tools/v3/assert"
)

func TestPastBookingAnonymisation(t *testing.T) {
	db := newDatabase(t, "past_anonymisation", "")
	var wg sync.WaitGroup
	queue, err := email.NewQueue(context.Background(), db, mail.Address{
		Name:    "DPSG München und Freising - Buchungssystem",
		Address: "haeuser2@dpsg1300.de",
	}, timeNow(), emailSender(t, &wg))
	if os.Getenv("TESTS_ON_ACTUAL_SMTP") == "1" {
		t.Cleanup(wg.Wait)
	}

	assert.NilError(t, err)

	pastBookingAnonymisation := NewCronTasks(db, queue, internal.EmbedFS)["PastBookingAnonymisation"]

	ctx := context.Background()

	err = db.SettingHouseCreate(ctx, database.SettingHouseCreateParams{
		ID:        "past_anonymisation",
		Name:      "anonymisation house",
		MaxPerson: 42,
		ReplyTo:   "anonymisation@example.com",
	})
	assert.NilError(t, err)

	booking := userConfirmedBooking(t, db, 0, 10)

	t.Run("skip_future_booking", func(t *testing.T) {
		timeNow = time.Now
		err = pastBookingAnonymisation()
		assert.NilError(t, err)

		booking, err = db.BookingFind(ctx, booking.ID)
		assert.NilError(t, err)
		assert.Equal(t, booking.Contact, "Past Booking")
	})

	t.Run("skip_recent_past_booking", func(t *testing.T) {
		// go in the future: in 100 days
		timeNow = func() time.Time {
			return time.Now().AddDate(0, 0, 20)
		}
		err = pastBookingAnonymisation()
		assert.NilError(t, err)

		booking, err = db.BookingFind(ctx, booking.ID)
		assert.NilError(t, err)
		assert.Equal(t, booking.Contact, "Past Booking")
	})

	t.Run("anonymize_older", func(t *testing.T) {
		// go in the future: in 400 days
		timeNow = func() time.Time {
			return time.Now().AddDate(0, 0, 400)
		}

		err = pastBookingAnonymisation()
		assert.NilError(t, err)

		booking, err = db.BookingFind(ctx, booking.ID)
		assert.NilError(t, err)
		assert.Equal(t, booking.Contact, "")
		assert.Equal(t, booking.Email, "")
		assert.Equal(t, booking.Phone, "")
		assert.Equal(t, booking.Comment, "Anonymisiert")
		assert.Equal(t, booking.Landline, "")
		var billing BillingInfo
		json.Unmarshal(booking.Invoice, &billing)
		assert.Equal(t, billing, BillingInfo{
			Kind:       "dpsg",
			PostalCode: "12345",
			City:       "Musterstadt",
			Country:    "DEU",
		})

		emails, err := db.EmailForBooking(ctx, booking.ID)
		assert.NilError(t, err)
		assert.Equal(t, 1, len(emails))
		assert.Equal(t, "from", emails[0].HeaderFrom)
		assert.Equal(t, "anonymous@anonymous.invalid", emails[0].HeaderTo)
		assert.Equal(t, "Subject", emails[0].HeaderSubject)
		assert.Equal(t, "", emails[0].TextPlain)

		logs, err := db.LogEventIndex(ctx, booking.ID)
		assert.NilError(t, err)
		assert.Equal(t, 3, len(logs))
		assert.Equal(t, "", logs[2].Name)
		assert.Equal(t, "system message", logs[2].Message)
		assert.Equal(t, "Admin 1", logs[1].Name)
		assert.Equal(t, "· Kommentar wurde anonymisiert", logs[1].Message)
		assert.Equal(t, "", logs[0].Name)
		assert.Equal(t, "· Anonymisiert (alt)", logs[0].Message)
	})
}

func userConfirmedBooking(t *testing.T, db *database.Queries, startDelay, endDelay int) database.Booking {
	ctx := context.Background()
	b, err := db.BookingCreate(ctx, database.BookingCreateParams{
		HouseID:    "past_anonymisation",
		StartAt:    trimeTruncate(time.Now().AddDate(0, 0, startDelay)).Add(17 * time.Hour),
		EndAt:      trimeTruncate(time.Now().AddDate(0, 0, endDelay)).Add(9 * time.Hour),
		PeopleOver: []byte("{}"),
		Contact:    "Past Booking",
		Email:      "expired@example.com",
		Phone:      "phone",
		Comment:    "comment",
		Invoice:    []byte(`{"Kind":"dpsg","Name":"My Name","Address1":"Street","Address2":"","PostalCode":"12345","City":"Musterstadt","Country":"DEU"}`),
	})
	assert.NilError(t, err)

	// confirmation date will be CURRENT_TIMESTAMP
	err = db.BookingAdminConfirm(ctx, database.BookingAdminConfirmParams{
		ID:          b.ID,
		HashedToken: "<token>",
	})
	assert.NilError(t, err)

	// confirmation date will be CURRENT_TIMESTAMP
	err = db.BookingUserConfirm(ctx, b.ID)
	assert.NilError(t, err)

	_, err = db.EmailCreate(ctx, database.EmailCreateParams{
		ID:            "emailId",
		BookingID:     b.ID,
		HeaderFrom:    "from",
		HeaderTo:      "To",
		HeaderSubject: "Subject",
		TextPlain:     "Text",
		FatalError:    "",
	})
	assert.NilError(t, err)

	err = db.LogEventCreate(ctx, database.LogEventCreateParams{
		BookingID: b.ID,
		UserID:    sql.NullString{},
		Message:   "system message",
	})
	assert.NilError(t, err)

	err = db.LogEventCreate(ctx, database.LogEventCreateParams{
		BookingID: b.ID,
		UserID: sql.NullString{
			Valid:  true,
			String: "admin1",
		},
		Message: "user message",
	})
	assert.NilError(t, err)

	return b
}
