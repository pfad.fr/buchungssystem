// SPDX-FileCopyrightText: 2023 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"context"
	"errors"

	"code.pfad.fr/buchungssystem/database"
)

func (c cronTasks) PastBookingAnonymisation() error {
	ctx := context.Background() // don't interrupt while running

	bookings, err := c.DB.BookingAnonymizeOld(ctx, timeNow().AddDate(0, 0, -365)) // anonymize bookings that happened more than 365 days ago
	if err != nil {
		return err
	}

	var errs error
	for _, booking := range bookings {
		err := anonymizeBooking(ctx, c.DB, booking)
		if err != nil {
			errs = errors.Join(errs, err)
			continue
		}
		err = c.DB.LogEventCreate(ctx, database.LogEventCreateParams{
			BookingID: booking.ID,
			Message:   "· Anonymisiert (alt)",
		})
		if err != nil {
			errs = errors.Join(errs, err)
			continue
		}
	}

	return errs
}
