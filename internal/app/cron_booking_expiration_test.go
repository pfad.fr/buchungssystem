// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"context"
	"os"
	"sync"
	"testing"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/buchungssystem/internal/email"
	"github.com/emersion/go-message/mail"
	"gotest.tools/v3/assert"
)

func TestBookingExpiration(t *testing.T) {
	db := newDatabase(t, "expiration", "")
	var wg sync.WaitGroup
	queue, err := email.NewQueue(context.Background(), db, mail.Address{
		Name:    "DPSG München und Freising - Buchungssystem",
		Address: "haeuser2@dpsg1300.de",
	}, timeNow(), emailSender(t, &wg))
	if os.Getenv("TESTS_ON_ACTUAL_SMTP") == "1" {
		t.Cleanup(wg.Wait)
	}

	assert.NilError(t, err)

	checkExpiredBookings := NewCronTasks(db, queue, internal.EmbedFS)["CheckExpiredBookings"]

	ctx := context.Background()

	err = db.SettingHouseCreate(ctx, database.SettingHouseCreateParams{
		ID:        "expiring",
		Name:      "Expiring house",
		MaxPerson: 42,
		ReplyTo:   "expiring@example.com",
	})
	assert.NilError(t, err)

	bookingStartingSoon := adminConfirmedBooking(t, db, 1, 2)
	bookingAfter8Days := adminConfirmedBooking(t, db, 10, 11)

	timeNow = func() time.Time {
		return trimeTruncate(time.Now().AddDate(0, 0, 1)).Add(time.Hour) // at midnight
	}
	err = checkExpiredBookings()
	assert.NilError(t, err)

	bookingStartingSoon, err = db.BookingFind(ctx, bookingStartingSoon.ID)
	assert.NilError(t, err)
	assert.Check(t, bookingStartingSoon.CancelledAt.Valid) // immediately cancelled
	waitForEmail(t)

	bookingAfter8Days, err = db.BookingFind(ctx, bookingAfter8Days.ID)
	assert.NilError(t, err)
	assert.Check(t, !bookingAfter8Days.CancelledAt.Valid) // not cancelled yet

	timeNow = func() time.Time {
		return time.Now().AddDate(0, 0, 8)
	}
	err = checkExpiredBookings()
	assert.NilError(t, err)

	bookingAfter8Days, err = db.BookingFind(ctx, bookingAfter8Days.ID)
	assert.NilError(t, err)
	assert.Check(t, bookingAfter8Days.CancelledAt.Valid) // cancelled now

	waitForEmail(t)
}

func adminConfirmedBooking(t *testing.T, db *database.Queries, startDelay, endDelay int) database.Booking {
	ctx := context.Background()
	b, err := db.BookingCreate(ctx, database.BookingCreateParams{
		HouseID:    "expiring",
		StartAt:    trimeTruncate(time.Now().AddDate(0, 0, startDelay)).Add(17 * time.Hour),
		EndAt:      trimeTruncate(time.Now().AddDate(0, 0, endDelay)).Add(9 * time.Hour),
		PeopleOver: []byte("{}"),
		Contact:    "Ex Pired",
		Email:      "expired@example.com",
		Phone:      "string",
		Comment:    "string",
		Invoice:    []byte("{}"),
	})
	assert.NilError(t, err)

	// confirmation date will be CURRENT_TIMESTAMP
	err = db.BookingAdminConfirm(ctx, database.BookingAdminConfirmParams{
		ID:          b.ID,
		HashedToken: "<token>",
	})
	assert.NilError(t, err)
	return b
}
