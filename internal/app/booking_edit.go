// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"context"
	"crypto/rand"
	"crypto/sha256"
	"crypto/subtle"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/emersion/go-message/mail"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/buchungssystem/internal/view"
)

func (a app) findBookingByID(ctx context.Context, id string) (database.Booking, error) {
	i, err := strconv.ParseInt(id, 10, 32)
	if err != nil {
		return database.Booking{}, err
	}
	return a.DB.BookingFind(ctx, int32(i))
}

func unmarshalAges(data []byte) (map[int32]int, int, []int, error) {
	var ages map[int32]int
	if err := json.Unmarshal(data, &ages); err != nil {
		return nil, 0, nil, err
	}

	sortedAges := make([]int, 0, len(ages))
	var totalPeople int
	for i, n := range ages {
		sortedAges = append(sortedAges, int(i))
		totalPeople += n
	}
	sort.Ints(sortedAges)
	return ages, totalPeople, sortedAges, nil
}

func (a app) BookingEdit(rw http.ResponseWriter, r *http.Request) error {
	self, err := a.CurrentUser(r)
	if err != nil {
		return err
	}

	booking, err := a.findBookingByID(r.Context(), r.FormValue("id"))
	if err != nil {
		return fmt.Errorf("could not find booking: %w", err)
	}

	policy := a.HousePolicy(r)
	if err = policy.CanViewBooking(booking.HouseID); err != nil {
		return err
	}

	house, err := a.DB.SettingHouseFind(r.Context(), booking.HouseID)
	if err != nil {
		return err
	}

	var billing BillingInfo
	if err = json.Unmarshal(booking.Invoice, &billing); err != nil {
		return err
	}

	ages, totalPeople, sortedAges, err := unmarshalAges(booking.PeopleOver)
	if err != nil {
		return err
	}

	emails, err := a.DB.EmailForBooking(r.Context(), booking.ID)
	if err != nil {
		return err
	}

	var otherBookingCount int64
	if booking.Email != "" {
		otherBookingCount, err = a.DB.BookingCountForEmail(r.Context(), database.BookingCountForEmailParams{
			ID:      booking.ID,
			HouseID: booking.HouseID,
			Email:   booking.Email,
		})
		if err != nil {
			return err
		}
	}

	offer, err := computeOfferUnmarshalled(r.Context(), a.DB, booking, totalPeople, ages)
	if err != nil {
		return err
	}

	prices, err := a.DB.BookingPricesIndex(r.Context(), database.BookingPricesIndexParams{
		HouseID: booking.HouseID,
		Start:   trimeTruncate(booking.StartAt).Format(dateLayout),
		EndDate: trimeTruncate(booking.EndAt).Format(dateLayout),
	})
	if err != nil {
		return err
	}

	events, err := a.DB.LogEventIndex(r.Context(), booking.ID)
	if err != nil {
		return err
	}

	return a.BookingEditView.Render(rw, r, struct {
		Nav                internal.NavbarData
		HeadLinks          []template.HTML
		Booking            database.Booking
		House              database.House
		Billing            BillingInfo
		KindName           string
		CountryName        string
		NightsCount        int
		SortedAges         []int
		Ages               map[int32]int
		TotalPeople        int
		Status             bookingStatus
		CanEdit            bool
		CancellationReason string
		Emails             []database.Email
		OtherBookingCount  int
		Offer              Offer
		LogEvents          []database.LogEventIndexRow
		Prices             []database.Price
		ChargeAges         []int32
	}{
		Nav: internal.NavbarData{
			User:             self,
			SelectedDropdown: "buchung",
			SelectedURI:      "buchung/alle",
			SearchTerm:       r.FormValue("q"),
		},
		HeadLinks:          []template.HTML{`<script defer src="/assets/admin-booking.js"></script>`},
		Booking:            booking,
		House:              house,
		Billing:            billing,
		KindName:           billingKinds.Name(billing.Kind),
		CountryName:        billingCountries.Name(billing.Country),
		NightsCount:        countNights(booking.StartAt, booking.EndAt),
		SortedAges:         sortedAges,
		Ages:               ages,
		TotalPeople:        totalPeople,
		Status:             newBookingStatus(booking, house.DaysForUserConfirmation),
		CanEdit:            !booking.CancelledAt.Valid && policy.CanEditBooking(booking.HouseID) == nil,
		CancellationReason: preCancellation(booking, house),
		Emails:             emails,
		OtherBookingCount:  int(otherBookingCount),
		Offer:              offer,
		LogEvents:          events,
		Prices:             prices,
		ChargeAges:         chargeAges(offer.Charges),
	})
}

func bookingDesc(booking database.Booking, house database.House) string {
	return fmt.Sprintf("Buchung für %s (%s bis %s)",
		house.Name,
		view.GermanDate(booking.StartAt),
		view.GermanDate(booking.EndAt),
	)
}

func preCancellation(booking database.Booking, house database.House) string {
	if booking.AdminConfirmedAt.Valid {
		return "schade, dass Du Dich gegen eine Buchung entschieden hast.\n" +
			"Wir freuen uns Dich demnächst im " + house.Name + " begrüßen zu können."
	}
	bookingDesc := bookingDesc(booking, house)
	return "leider können wir Deine " + bookingDesc + " nicht annehmen."
}

func (a app) editBooking(r *http.Request) (database.Booking, database.House, error) {
	booking, err := a.findBookingByID(r.Context(), r.FormValue("id"))
	if err != nil {
		return database.Booking{}, database.House{}, err
	}

	policy := a.HousePolicy(r)
	if err = policy.CanEditBooking(booking.HouseID); err != nil {
		return database.Booking{}, database.House{}, err
	}

	if booking.CancelledAt.Valid {
		return database.Booking{}, database.House{}, internal.HTTPError{
			Code:    http.StatusConflict,
			Message: "Bereits gelöscht",
		}
	}

	house, err := a.DB.SettingHouseFind(r.Context(), booking.HouseID)
	if err != nil {
		return database.Booking{}, database.House{}, err
	}
	return booking, house, nil
}

func (a app) BookingCancel(rw http.ResponseWriter, r *http.Request) error {
	booking, house, err := a.editBooking(r)
	if err != nil {
		return err
	}

	subject, body, err := a.notification.userEmail.BookingCancelledByAdmin(struct {
		Booking database.Booking
		House   database.House
		Reason  string
	}{
		Booking: booking,
		House:   house,
		Reason:  strings.TrimSpace(r.FormValue("reason")),
	})
	if err != nil {
		return err
	}

	msg := database.Email{
		BookingID: booking.ID,
		HeaderTo: (&mail.Address{
			Name:    booking.Contact,
			Address: booking.Email,
		}).String(),
		HeaderFrom:    house.ReplyTo,
		HeaderSubject: subject,
		TextPlain:     string(body),
	}
	if err = a.Queue.Push(r.Context(), msg); err != nil {
		return err
	}

	err = a.DB.BookingDelete(r.Context(), booking.ID)
	if err != nil {
		return err
	}
	_ = a.logEvent(r, booking.ID, "· Storniert:\n"+strings.TrimSpace(r.FormValue("admin_reason")))

	return a.RedirectBack(rw, r)
}

func newHashedToken() (string, string, error) {
	b := make([]byte, 32)
	if _, err := rand.Read(b); err != nil {
		return "", "", err
	}

	hash := sha256.New()
	if _, err := hash.Write(b); err != nil {
		return "", "", err
	}

	token := base64.RawURLEncoding.EncodeToString(b)
	hashedToken := string(hash.Sum(nil))
	return token, hashedToken, nil
}

func checkHashedToken(token, hashedToken string) error {
	b, err := base64.RawURLEncoding.DecodeString(token)
	if err != nil {
		return internal.HTTPError{
			Code:    http.StatusUnauthorized,
			Message: "Zugriff verweigert",
		}
	}

	hash := sha256.New()
	if _, err := hash.Write(b); err != nil {
		return err
	}

	providedHashedToken := hash.Sum(nil)
	if subtle.ConstantTimeCompare(providedHashedToken, []byte(hashedToken)) != 1 {
		return internal.HTTPError{
			Code:    http.StatusUnauthorized,
			Message: "Zugriff verweigert",
		}
	}

	return nil
}

func (a app) BookingAccept(rw http.ResponseWriter, r *http.Request) error {
	booking, house, err := a.editBooking(r)
	if err != nil {
		return err
	}

	if booking.AdminConfirmedAt.Valid {
		return internal.HTTPError{
			Code:    http.StatusConflict,
			Message: "Bereits bestätigt",
		}
	}

	token, hashedToken, err := newHashedToken()
	if err != nil {
		return err
	}

	offer, err := ComputeOffer(r.Context(), a.DB, booking)
	if err != nil {
		return err
	}

	// TODO: Anhang (AGBs & co)
	subject, body, err := a.notification.userEmail.BookingConfirmedByAdmin(struct {
		Booking         database.Booking
		House           database.House
		ConfirmUntil    time.Time
		ConfirmationURL string
		Offer           Offer
		Reason          string
	}{
		Booking:         booking,
		House:           house,
		ConfirmUntil:    userMustConfirmUntil(booking.StartAt, timeNow(), house.DaysForUserConfirmation),
		ConfirmationURL: a.BaseURL + "/eigene/anfrage?id=" + strconv.Itoa(int(booking.ID)) + "&token=" + token,
		Offer:           offer,
		Reason:          strings.TrimSpace(r.FormValue("reason")),
	})
	if err != nil {
		return err
	}

	userMsg := database.Email{
		BookingID: booking.ID,
		HeaderTo: (&mail.Address{
			Name:    booking.Contact,
			Address: booking.Email,
		}).String(),
		HeaderFrom:    house.ReplyTo,
		HeaderSubject: subject,
		TextPlain:     string(body),
	}
	if err = a.Queue.Push(r.Context(), userMsg); err != nil {
		return err
	}

	err = a.DB.BookingAdminConfirm(r.Context(), database.BookingAdminConfirmParams{
		ID:          booking.ID,
		HashedToken: hashedToken,
	})
	if err != nil {
		return err
	}

	_ = a.logEvent(r, booking.ID, "· Anfrage Bestätigt")

	return a.RedirectBack(rw, r)
}

func (a app) logEvent(r *http.Request, bookingID int32, msg string) error {
	var userID sql.NullString
	user, _ := a.CurrentUser(r)
	if user != nil {
		userID.Valid = true
		userID.String = user.ID
	}
	return a.DB.LogEventCreate(r.Context(), database.LogEventCreateParams{
		UserID:    userID,
		BookingID: bookingID,
		Message:   msg,
	})
}

func (a app) BookingEditDatetime(rw http.ResponseWriter, r *http.Request) error {
	booking, _, err := a.editBooking(r)
	if err != nil {
		return err
	}

	timeParse := func(prefix string) (time.Time, error) {
		return time.ParseInLocation(
			dateLayout+" 15:04", r.FormValue(prefix+"_date")+" "+r.FormValue(prefix+"_time"), time.Local)
	}

	startAt, err := timeParse("start")
	if err != nil {
		return err
	}
	endAt, err := timeParse("end")
	if err != nil {
		return err
	}
	err = a.DB.BookingUpdateDates(r.Context(), database.BookingUpdateDatesParams{
		ID:      booking.ID,
		StartAt: startAt,
		EndAt:   endAt,
	})
	if err != nil {
		return err
	}

	previousStartAt := booking.StartAt
	previousEndAt := booking.EndAt
	booking.StartAt = startAt
	booking.EndAt = endAt

	offer, err := ComputeOffer(r.Context(), a.DB, booking)
	if err != nil {
		return err
	}

	_ = a.logEvent(r, booking.ID,
		fmt.Sprintf("· Zeiten angepasst:\n%s - %s\n(vorher:\n%s - %s)",
			startAt.Local().Format(dateLayout+" 15:04"),
			endAt.Local().Format(dateLayout+" 15:04"),
			previousStartAt.Local().Format(dateLayout+" 15:04"),
			previousEndAt.Local().Format(dateLayout+" 15:04"),
		)+"\nPreis: "+view.FormattedCentimes(offer.TotalAmount, ",")+"€",
	)

	return a.RedirectBack(rw, r)
}

func (a app) BookingEditPeopleAges(rw http.ResponseWriter, r *http.Request) error {
	booking, house, err := a.editBooking(r)
	if err != nil {
		return err
	}

	charges, err := a.bookingCharges(r.Context(), booking)
	if err != nil {
		return err
	}

	chargeableAges := chargeAges(charges)

	ages, totalPeople, err := validateAges(chargeableAges, r.FormValue, int(house.MaxPerson))
	if err != nil {
		return err
	}
	if totalPeople <= 0 {
		return internal.HTTPError{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("Zu wenige Personen gebucht: %d", totalPeople),
		}
	}

	peopleOver, err := json.Marshal(ages)
	if err != nil {
		return err
	}

	err = a.DB.BookingUpdatePeopleAges(r.Context(), database.BookingUpdatePeopleAgesParams{
		ID:         booking.ID,
		PeopleOver: peopleOver,
	})
	if err != nil {
		return err
	}
	previousPeopleOver := booking.PeopleOver
	booking.PeopleOver = peopleOver

	offer, err := ComputeOffer(r.Context(), a.DB, booking)
	if err != nil {
		return err
	}

	_ = a.logEvent(r, booking.ID,
		fmt.Sprintf("· Personen angepasst:\n%s\n(vorher:\n%s)",
			string(peopleOver),
			string(previousPeopleOver),
		)+"\nPreis: "+view.FormattedCentimes(offer.TotalAmount, ",")+"€",
	)

	return a.RedirectBack(rw, r)
}

func (a app) BookingEditComment(rw http.ResponseWriter, r *http.Request) error {
	booking, _, err := a.editBooking(r)
	if err != nil {
		return err
	}
	err = a.DB.BookingCommentUpdate(r.Context(), database.BookingCommentUpdateParams{
		ID:      booking.ID,
		Comment: strings.TrimSpace(r.FormValue("comment")),
	})
	if err != nil {
		return err
	}

	return a.RedirectBack(rw, r)
}

func validateAges(chargeableAges []int32, valueOf func(string) string, maxPerson int) (map[int32]int, int, error) {
	ages := make(map[int32]int)
	totalPeople := 0
	for _, a := range chargeableAges {
		s := valueOf("people_over[" + strconv.Itoa(int(a)) + "]")
		if s == "" {
			continue
		}
		n, err := strconv.Atoi(s)
		if err != nil {
			return nil, 0, err
		}
		if n <= -1 || n > maxPerson {
			return nil, 0, internal.HTTPError{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("Ungültige Personenanzahl über %d Jahre: %d (max: %d)", a, n, maxPerson),
			}
		}
		if n > 0 {
			ages[a] = n
			totalPeople += n
		}
	}
	if totalPeople > maxPerson {
		return nil, 0, internal.HTTPError{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("Zu viele Personen gebucht: %d (max: %d)", totalPeople, maxPerson),
		}
	}

	return ages, totalPeople, nil
}

func (a app) bookingCharges(ctx context.Context, booking database.Booking) ([]database.Charge, error) {
	start := trimeTruncate(booking.StartAt).Format(dateLayout)
	end := trimeTruncate(booking.EndAt).Format(dateLayout)
	return a.DB.BookingChargesIndex(ctx, database.BookingChargesIndexParams{
		HouseID: booking.HouseID,
		Start:   start,
		EndDate: end,
	})
}

func (a app) BookingEditExtraCharges(rw http.ResponseWriter, r *http.Request) error {
	booking, _, err := a.editBooking(r)
	if err != nil {
		return err
	}

	charges, err := a.bookingCharges(r.Context(), booking)
	if err != nil {
		return err
	}

	extraPercentages, err := extraChargesPercentages(r.Context(), a.DB, booking.ID)
	if err != nil {
		return err
	}
	var msgs []string
	for _, c := range charges {
		updated := int32(0)
		if value := r.FormValue("charge[" + c.ID + "]"); value != "" {
			var i64 int64
			i64, err = strconv.ParseInt(value, 10, 32)
			if err != nil {
				return err
			}
			updated = int32(i64)
		}

		old, ok := extraPercentages[c.ID]
		if updated == old || (updated == 0 && !ok) {
			continue
		}
		// create, update or delete
		switch {
		case !ok: // create
			err = a.DB.BookingExtraChargesCreate(r.Context(), database.BookingExtraChargesCreateParams{
				BookingID: booking.ID,
				ChargeID:  c.ID,
				Percent:   updated,
			})
			msgs = append(msgs, "- "+c.Name+" hinzugefügt ("+strconv.Itoa(int(updated))+"%)")
		case updated == 0: // delete
			err = a.DB.BookingExtraChargesDelete(r.Context(), database.BookingExtraChargesDeleteParams{
				BookingID: booking.ID,
				ChargeID:  c.ID,
			})
			msgs = append(msgs, "- "+c.Name+" gelöscht (war "+strconv.Itoa(int(old))+"%)")
		default:
			err = a.DB.BookingExtraChargesUpdate(r.Context(), database.BookingExtraChargesUpdateParams{
				BookingID: booking.ID,
				ChargeID:  c.ID,
				Percent:   updated,
			})
			msgs = append(msgs, "- "+c.Name+" angepasst "+strconv.Itoa(int(updated))+"% (war "+strconv.Itoa(int(old))+"%)")
		}
		if err != nil {
			return err
		}
	}

	if len(msgs) > 0 {
		offer, err := ComputeOffer(r.Context(), a.DB, booking)
		if err != nil {
			return err
		}
		msgs = append(msgs, "Preis: "+view.FormattedCentimes(offer.TotalAmount, ",")+"€")

		_ = a.logEvent(r, booking.ID, "· Zusatz-Optionen angepasst:\n"+strings.Join(msgs, "\n"))
	}

	return a.RedirectBack(rw, r)
}

func (a app) BookingCommentCreate(rw http.ResponseWriter, r *http.Request) error {
	booking, err := a.findBookingByID(r.Context(), r.FormValue("id"))
	if err != nil {
		return err
	}

	policy := a.HousePolicy(r)
	if err = policy.CanViewBooking(booking.HouseID); err != nil {
		return err
	}

	err = a.logEvent(r, booking.ID, strings.TrimSpace(r.FormValue("message")))
	if err != nil {
		return err
	}

	return a.RedirectBack(rw, r)
}
