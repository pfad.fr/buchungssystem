// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"testing"

	"code.pfad.fr/buchungssystem/database"
	"gotest.tools/v3/assert"
)

func TestChargeAges(t *testing.T) {
	cc := []struct {
		name   string
		input  []int32
		output []int32
	}{
		{
			"empty",
			nil,
			[]int32{0},
		},
		{
			"1,2",
			[]int32{0, 0, 1, 2, 1, 0, 2},
			[]int32{1, 2},
		},
		{
			"2,1",
			[]int32{0, 0, 2, 2, 1, 0, 2},
			[]int32{1, 2},
		},
		{
			"3,2,1",
			[]int32{3, 2, 1},
			[]int32{1, 2, 3},
		},
	}
	for _, c := range cc {
		t.Run(c.name, func(t *testing.T) {
			charges := make([]database.Charge, 0, len(c.input))
			for _, i := range c.input {
				charges = append(charges, database.Charge{
					ExemptedUnderAge: i,
				})
			}
			ages := chargeAges(charges)
			assert.DeepEqual(t, c.output, ages)
		})
	}
}
