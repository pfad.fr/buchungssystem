// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"context"

	"code.pfad.fr/buchungssystem/database"
)

func ComputeOffer(ctx context.Context, db *database.Queries, booking database.Booking) (Offer, error) {
	ages, totalPeople, _, err := unmarshalAges(booking.PeopleOver)
	if err != nil {
		return Offer{}, err
	}

	return computeOfferUnmarshalled(ctx, db, booking, totalPeople, ages)
}

func computeOfferUnmarshalled(ctx context.Context, db *database.Queries, booking database.Booking, totalPeople int, ages map[int32]int) (Offer, error) {
	// compute offer
	start := trimeTruncate(booking.StartAt).Format(dateLayout)
	end := trimeTruncate(booking.EndAt).Format(dateLayout)
	params := database.BookingChargesIndexParams{
		HouseID: booking.HouseID,
		Start:   start,
		EndDate: end,
	}
	charges, err := db.BookingChargesIndex(ctx, params)
	if err != nil {
		return Offer{}, err
	}

	bookableDays, err := db.BookableDayIndex(ctx, database.BookableDayIndexParams(params))
	if err != nil {
		return Offer{}, err
	}

	extraPercentages, err := extraChargesPercentages(ctx, db, booking.ID)
	if err != nil {
		return Offer{}, err
	}
	return newOffer(bookableDays, charges, totalPeople, ages, extraPercentages), nil
}

func extraChargesPercentages(ctx context.Context, db *database.Queries, bookingID int32) (map[string]int32, error) {
	extras, err := db.BookingExtraChargesIndex(ctx, bookingID)
	if err != nil {
		return nil, err
	}
	percentages := make(map[string]int32)
	for _, e := range extras {
		percentages[e.ChargeID] = e.Percent
	}
	return percentages, nil
}

type offerLine struct {
	Days    int
	People  int
	Amount  int
	Charge  database.Charge
	Percent int32 // only used for extra charges
}
type Offer struct {
	Days            int
	People          int
	Amount          int
	ExtraAmount     int
	TotalAmount     int
	Lines           []offerLine
	ExtraLines      []offerLine
	ExtraAvailables []offerLine

	Charges []database.Charge
}

func (o *Offer) addCharge(days, people int, charge database.Charge) {
	amount := days * people * int(charge.Amount)
	o.Amount += amount
	o.Lines = append(o.Lines, offerLine{
		Days:   days,
		People: people,
		Amount: amount,
		Charge: charge,
	})
}

func (o *Offer) addChargeExtra(days, people int, percent int32, charge database.Charge) {
	amount := people * days * int(charge.Amount*percent) / 100
	o.ExtraAmount += amount
	o.ExtraLines = append(o.ExtraLines, offerLine{
		Days:    days,
		People:  people,
		Amount:  amount,
		Charge:  charge,
		Percent: percent,
	})
}

func newOffer(days []database.BookableDay, charges []database.Charge, totalPeople int, ages map[int32]int, extraPercentages map[string]int32) Offer {
	perStayPriceID := ""
	priceDays := make(map[string]int)
	for _, d := range days {
		if d.PriceID != "" { // the latest should apply
			perStayPriceID = d.PriceID
		}
		priceDays[d.PriceID]++
	}

	o := Offer{
		Days:    len(days),
		People:  totalPeople,
		Amount:  0,
		Charges: charges,
	}
	for _, c := range charges {
		percent := extraPercentages[c.ID]

		// extra not booked
		if percent == 0 && c.Optionality != "" {
			o.ExtraAvailables = append(o.ExtraAvailables, offerLine{
				Charge: c,
			})
			continue
		}

		nPeople := c.ChargedPeople(totalPeople, ages)
		if nPeople == 0 {
			continue
		}

		days := 1
		if c.Unit == "per_person_night" {
			days = priceDays[c.PriceID]
		}

		// extra charge
		if percent != 0 {
			o.addChargeExtra(days, nPeople, percent, c)
			continue
		}

		// normal charge
		if c.Unit == "per_stay" && c.PriceID != perStayPriceID {
			continue // per_stay charged only on price of first day (cleaning for instance)
		}

		o.addCharge(days, nPeople, c)
	}
	o.TotalAmount = o.Amount + o.ExtraAmount
	return o
}
