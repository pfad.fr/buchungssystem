// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"embed"
	"io/fs"
	"net/http"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/buchungssystem/internal/auth"
	"code.pfad.fr/buchungssystem/internal/settings"
	"code.pfad.fr/buchungssystem/internal/view"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/gorilla/csrf"
)

func Router(cookieKey []byte, oidc auth.OIDC, assetsFS fs.FS, baseHandler internal.Handler) (*chi.Mux, error) {
	authKey, blockKey := cookieKey[:64], cookieKey[64:]

	r := chi.NewRouter()
	// r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(csrf.Protect(authKey, csrf.CookieName("csrf_token")))
	r.Use(MethodSpoofing)
	r.Use(middleware.Compress(5))

	ag := &auth.Goath{
		BaseHandler: baseHandler,
		URI:         "/authboss/oauth2",
	}

	err := ag.MountRoutes(authKey, blockKey, oidc, r)
	if err != nil {
		return nil, err
	}

	r.Get("/", view.NewTemplate(assetsFS, "doctype", "public").ServeHTTP)

	notifs := notifs(assetsFS)
	ownBaseHandler := internal.Handler{
		BaseURL:         baseHandler.BaseURL,
		DB:              baseHandler.DB,
		Queue:           baseHandler.Queue,
		CurrentUser:     ag.CurrentUser,
		ErrorHandler:    baseHandler.ErrorHandler,
		RenderErrorView: view.NewTemplate(assetsFS, "doctype", "layout/own", "httperror").Render,
	}
	r.With(
		RobotsTagNone,
	).Route("/", OwnBookingRouter(ownBaseHandler, assetsFS, notifs))

	waitView := view.NewTemplate(assetsFS, "doctype", "visitor")
	adminBaseHandler := internal.Handler{
		BaseURL:         baseHandler.BaseURL,
		DB:              baseHandler.DB,
		Queue:           baseHandler.Queue,
		CurrentUser:     ag.CurrentUser,
		ErrorHandler:    baseHandler.ErrorHandler,
		RenderErrorView: view.NewTemplate(assetsFS, "doctype", "layout/admin", "httperror").Render,
	}
	r.With(
		ag.AuthorizedUsers(http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			user, _ := ag.CurrentUser(r)
			err := waitView.Render(rw, r, struct {
				User database.User
			}{
				User: *user,
			})
			if err != nil {
				rw.WriteHeader(http.StatusInternalServerError)
				_, _ = rw.Write([]byte(err.Error()))
			}
		})),
		RobotsTagNone,
	).Route("/verwaltung", AuthenticatedRouter(
		adminBaseHandler, assetsFS, notifs,
	))

	assetHandler := http.FileServer(http.FS(assetsFS))
	r.With(func(next http.Handler) http.Handler {
		lastModified := ""
		if _, ok := assetsFS.(embed.FS); ok {
			lastModified = time.Now().Format(http.TimeFormat)
		}
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("Cache-Control", "no-cache, public")
			if lastModified != "" {
				if r.Header.Get("If-Modified-Since") == lastModified {
					w.WriteHeader(http.StatusNotModified)
					return
				}
				w.Header().Set("Last-Modified", lastModified)
			}
			next.ServeHTTP(w, r)
		})
	}).Mount("/assets/", assetHandler)

	// TODO: own datenschutz
	r.Get("/datenschutz", http.RedirectHandler("https://www.dpsg1300.de/datenschutz-1/", http.StatusTemporaryRedirect).ServeHTTP)
	r.Get("/impressum", http.RedirectHandler("https://www.dpsg1300.de/impressum/", http.StatusTemporaryRedirect).ServeHTTP)

	r.Get("/azure.pem", baseHandler.ErrorHandler.Wrap(oidc.Certificate.ServeDER).ServeHTTP)

	return r, nil
}

func RobotsTagNone(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("X-Robots-Tag", "none")
		next.ServeHTTP(w, r)
	})
}

type app struct {
	internal.Handler

	DashboardView          *view.Template
	BookingNewView         *view.Template
	BookingEditView        *view.Template
	BookingBillingEditView *view.Template
	BookingListView        *view.Template
	notification           notification
}

func notifs(fs fs.FS) notification {
	return notification{
		userEmail: userEmail{
			BookingCreated: view.NewEmailTemplate(fs, "emails/1_created/user.gotmpl"),

			BookingConfirmedByAdmin: view.NewEmailTemplate(fs, "emails/2_admin_confirmed/user.gotmpl"),

			BookingConfirmedByUser:  view.NewEmailTemplate(fs, "emails/3_user_confirmed/user.gotmpl"),
			BookingCancelledByAdmin: view.NewEmailTemplate(fs, "emails/4_admin_cancelled/user.gotmpl"),
		},
		adminEmail: adminEmail{
			BookingCreated: view.NewEmailTemplate(fs, "emails/1_created/admin.gotmpl"),
		},
	}
}

type notification struct {
	userEmail  userEmail
	adminEmail adminEmail
}
type userEmail struct {
	BookingCreated view.ExecuteEmailTemplate

	BookingConfirmedByAdmin view.ExecuteEmailTemplate

	BookingConfirmedByUser  view.ExecuteEmailTemplate
	BookingCancelledByAdmin view.ExecuteEmailTemplate
}
type adminEmail struct {
	BookingCreated view.ExecuteEmailTemplate
}

func AuthenticatedRouter(baseHandler internal.Handler, assetsFS fs.FS, notification notification) func(r chi.Router) {
	return func(r chi.Router) {
		a := app{
			Handler:       baseHandler,
			DashboardView: view.NewTemplate(assetsFS, "doctype", "layout/admin", "dashboard"),

			BookingNewView:         view.NewTemplate(assetsFS, "doctype", "layout/admin", "booking/new"),
			BookingEditView:        view.NewTemplate(assetsFS, "doctype", "layout/admin", "booking/edit"),
			BookingBillingEditView: view.NewTemplate(assetsFS, "doctype", "layout/admin", "booking/billing"),
			BookingListView:        view.NewTemplate(assetsFS, "doctype", "layout/admin", "booking/list"),
			notification:           notification,
		}

		r.Get("/", a.HandleErr(a.Dashboard))
		r.Route("/einstellungen", settings.Handler{
			Handler:               baseHandler,
			UserIndexView:         view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/user_index"),
			HouseIndexView:        view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/house_index"),
			PriceIndexView:        view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/price_index"),
			ChargeIndexView:       view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/charge_index"),
			ChargeEditView:        view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/charge_edit"),
			DayIndexView:          view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/day_index"),
			RestIndexView:         view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/rest_index"),
			ExtraRequestIndexView: view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/extra_request_index"),
			ExtraRequestEditView:  view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/extra_request_edit"),
		}.Router)

		r.Get("/buchung/erstellen", a.HandleErr(a.BookingNew))
		r.Post("/buchung/erstellen", a.HandleErr(a.BookingNew))

		r.Get("/buchung/bearbeiten", a.HandleErr(a.BookingEdit))
		r.Put("/buchung/stornieren", a.HandleErr(a.BookingCancel))
		r.Put("/buchung/akzeptieren", a.HandleErr(a.BookingAccept))
		r.Put("/buchung/zeitraum", a.HandleErr(a.BookingEditDatetime))
		r.Put("/buchung/personen", a.HandleErr(a.BookingEditPeopleAges))
		r.Put("/buchung/optionen", a.HandleErr(a.BookingEditExtraCharges))
		r.Put("/buchung/bemerkung", a.HandleErr(a.BookingEditComment))
		r.Post("/buchung/kommentar", a.HandleErr(a.BookingCommentCreate))

		r.Get("/buchung/abrechnungsdaten", a.HandleErr(a.BookingBillingEdit))
		r.Put("/buchung/abrechnungsdaten", a.HandleErr(a.BookingBillingUpdate))

		r.Get("/buchung/liste", a.HandleErr(a.BookingList))

		// 404
		r.NotFound(a.HandleErr(func(rw http.ResponseWriter, r *http.Request) error {
			return internal.HTTPError{
				Code:    http.StatusNotFound,
				Message: "Seite nicht gefunden",
			}
		}))
		r.MethodNotAllowed(a.HandleErr(func(rw http.ResponseWriter, r *http.Request) error {
			return internal.HTTPError{
				Code:    http.StatusMethodNotAllowed,
				Message: r.Method + " method not allowed",
			}
		}))
	}
}

// MethodSpoofing allows to spoof PUT, PATCH and DELETE methods from HTML forms, using the _method field.
func MethodSpoofing(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodPost {
			switch method := r.PostFormValue("_method"); method {
			case http.MethodPut:
				fallthrough
			case http.MethodPatch:
				fallthrough
			case http.MethodDelete:
				r.Method = method
			default:
			}
		}
		next.ServeHTTP(w, r)
	})
}
