// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"math"
	"regexp"
	"time"

	"code.pfad.fr/buchungssystem/database"
)

var timeNow = time.Now

var phoneRegex = regexp.MustCompile(`^(((\+|00)[1-9][0-9]{0,2})|0)[0-9 -]+$`)

func trimeTruncate(t time.Time) time.Time {
	// time.Truncate always truncates against UTC
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
}

func countNights(start, end time.Time) int {
	//  truncate always truncates against UTC
	startT := trimeTruncate(start)
	endT := trimeTruncate(end)
	return int(math.Ceil(endT.Sub(startT).Hours() / 24))
}

type bookingStatus struct {
	Time                              time.Time
	IsAdminConfirmed, IsUserConfirmed bool
	UserMustConfirmUntil              time.Time
	IsCancelled                       bool
	Text                              string
}

func newBookingStatus(b database.Booking, daysForUserConfirmation int32) bookingStatus {
	s := bookingStatus{
		Time: b.CreatedAt,
		Text: "Angefragt",
	}

	if b.AdminConfirmedAt.Valid {
		s.IsAdminConfirmed = true
		s.Time = b.AdminConfirmedAt.Time
		s.Text = "Ausstehend"
		s.UserMustConfirmUntil = userMustConfirmUntil(b.StartAt, b.AdminConfirmedAt.Time, daysForUserConfirmation)
	}
	if b.UserConfirmedAt.Valid {
		s.IsUserConfirmed = true
		s.Time = b.UserConfirmedAt.Time
		s.Text = "Bestätigt"
	}
	if b.CancelledAt.Valid {
		s.IsCancelled = true
		s.Time = b.CancelledAt.Time
		s.Text = "Storniert"
	}
	return s
}
