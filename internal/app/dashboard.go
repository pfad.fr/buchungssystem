// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
)

type day struct {
	time.Time
	IsPast    bool
	IsToday   bool
	IsWeekend bool
	Data      interface{}
}
type month struct {
	time.Month
	Year  int
	Weeks [][]day
}
type monthsAccumulator struct {
	months      []month
	current     month
	previousDay time.Time
	today       time.Time
}

func (ma *monthsAccumulator) AppendWithoutHole(t time.Time, data interface{}) bool {
	if !ma.previousDay.IsZero() {
		between := ma.previousDay.AddDate(0, 0, 1)
		if between.Sub(t) < 0 {
			return false
		}
	}
	ma.append(t, data)
	return true
}
func (ma *monthsAccumulator) Append(t time.Time, data interface{}) {
	if !ma.previousDay.IsZero() {
		between := ma.previousDay.AddDate(0, 0, 1)
		for between.Sub(t) < 0 {
			ma.append(between, nil)
			between = ma.previousDay.AddDate(0, 0, 1)
		}
	}
	ma.append(t, data)
}

func (ma *monthsAccumulator) append(t time.Time, d interface{}) {
	wd := t.Weekday()
	data := day{
		Time:      t,
		Data:      d,
		IsPast:    t.Sub(ma.today) < 0,
		IsToday:   t.Equal(ma.today),
		IsWeekend: wd == time.Saturday || wd == time.Sunday,
	}
	ma.previousDay = t
	if ma.current.Month == t.Month() && ma.current.Year == t.Year() {
		if t.Weekday() == time.Monday {
			ma.current.Weeks = append(ma.current.Weeks, []day{data})
		} else {
			ma.current.Weeks[len(ma.current.Weeks)-1] = append(ma.current.Weeks[len(ma.current.Weeks)-1], data)
		}
		return
	}
	if ma.current.Month != 0 {
		ma.months = append(ma.months, ma.current)
	}
	ma.current.Month = t.Month()
	ma.current.Year = t.Year()
	ma.current.Weeks = make([][]day, 1)
	start := t
	for start.Weekday() != time.Monday {
		start = start.AddDate(0, 0, -1)
		ma.current.Weeks[0] = append(ma.current.Weeks[0], day{})
	}
	ma.current.Weeks[0] = append(ma.current.Weeks[0], data)
}
func (ma *monthsAccumulator) Months() []month {
	months := ma.months
	if ma.current.Month != 0 {
		months = append(months, ma.current)
	}
	return months
}

type viewDay struct {
	PreviousColor   string
	Color           string
	SameReservation bool
	Title           string
	Booking         database.Booking
	BookingStatuses []bookingStatus
}

type monthsGatherer struct {
	DB  *database.Queries
	now time.Time

	firstDayOfMonth time.Time
	startDay        string
	lastDay         time.Time
	endDate         string
}

func (mg monthsGatherer) gather(ctx context.Context, house database.SettingViewableHouseIndexRow) ([]month, error) { //nolint:gocognit
	allBookings, err := mg.DB.BookingOverlapping(ctx, database.BookingOverlappingParams{
		HouseID: house.ID,
		StartAt: mg.firstDayOfMonth,
		EndAt:   mg.lastDay,
	})
	if err != nil {
		return nil, err
	}
	findBookings := func(cur time.Time) (dayBookings []database.Booking) {
		startOfDay := cur
		endOfDay := startOfDay.AddDate(0, 0, 1)
		for _, b := range allBookings {
			if b.StartAt.Before(endOfDay) && b.EndAt.After(startOfDay) {
				dayBookings = append(dayBookings, b)
			}
		}
		return dayBookings
	}

	days, err := mg.DB.BookableDayIndex(ctx, database.BookableDayIndexParams{
		Start:   mg.startDay,
		EndDate: mg.endDate,
		HouseID: house.ID,
	})
	if err != nil {
		return nil, err
	}
	ma := monthsAccumulator{
		today: time.Date(mg.now.Year(), mg.now.Month(), mg.now.Day(), 0, 0, 0, 0, time.Local),
	}
	var vd viewDay
	var previousDay database.BookableDay
	var previousBooking database.Booking
	for _, d := range days {
		date, err := time.Parse(dateLayout, d.Date)
		if err != nil {
			return nil, err
		}

		var booking database.Booking
		var bookingStatuses []bookingStatus
		if bookings := findBookings(date); len(bookings) > 0 {
			if bookings[0].EndAt.After(date.AddDate(0, 0, 1)) {
				// do not count the last night
				booking = bookings[0]
			}

			if house.PublicBookingType == "overday" {
				interval := int32(60)
				for m := house.StartMinute; m < house.EndMinute; m += interval {
					var currentStatus bookingStatus
					for len(bookings) > 0 {
						b := bookings[0]
						curStart := adjustedTime(date, m)
						curEnd := adjustedTime(date, m+interval-1)

						if b.EndAt.Before(curStart) {
							bookings = bookings[1:]
							continue
						}
						if b.StartAt.After(curEnd) {
							break
						}
						currentStatus = newBookingStatus(b, house.DaysForUserConfirmation)
						break
					}
					bookingStatuses = append(bookingStatuses, currentStatus)
				}
			}
		}
		status := newBookingStatus(booking, house.DaysForUserConfirmation)
		var title string
		if booking.ID != 0 {
			title = "#" + strconv.Itoa(int(booking.ID)) + " " + status.Text
			if d.RestrictionReason != "" {
				title += " (" + d.RestrictionReason + ")"
			}
		} else if d.RestrictionReason != "" {
			title = d.RestrictionReason
		}

		vd = viewDay{
			PreviousColor:   vd.Color,
			Color:           "",
			SameReservation: false,
			Title:           title,
			Booking:         booking,
			BookingStatuses: bookingStatuses,
		}
		if booking.ID != 0 {
			switch {
			case status.IsUserConfirmed:
				vd.Color = "green-200"
			case status.IsAdminConfirmed:
				vd.Color = "navy-200"
			default:
				vd.Color = "orange-200"
			}
		} else if d.RestrictionReason != "" {
			vd.Color = "gray-200"
		}
		if vd.PreviousColor == vd.Color { //nolint:nestif
			if booking.ID != 0 { // booking case
				if booking.ID == previousBooking.ID {
					vd.SameReservation = true
				}
			} else if d.RestrictionReason != "" { // admin case
				if previousDay.RestrictionReason == d.RestrictionReason {
					vd.SameReservation = true
				}
			}
		}

		ma.Append(date, vd)
		previousDay = d
		previousBooking = booking
	}
	return ma.Months(), nil
}

func (a app) Dashboard(rw http.ResponseWriter, r *http.Request) error {
	self, err := a.CurrentUser(r)
	if err != nil {
		return err
	}

	houses, err := a.DB.SettingViewableHouseIndex(r.Context(), self.ID)
	if err != nil {
		return err
	}

	now := timeNow()
	firstDayOfMonth := time.Date(now.Year(), now.Month(), 1, 0, 0, 0, 0, now.Location())
	lastDay := firstDayOfMonth.AddDate(0, 12, 0)

	mg := monthsGatherer{
		DB:  a.DB,
		now: now,

		firstDayOfMonth: firstDayOfMonth,
		startDay:        firstDayOfMonth.Format(dateLayout),
		lastDay:         lastDay,
		endDate:         lastDay.Format(dateLayout),
	}

	type House database.SettingViewableHouseIndexRow
	type viewHouse struct {
		House
		Status   map[string]int64
		Upcoming []database.Booking
		Months   []month
	}
	vHouses := make([]viewHouse, 0, len(houses))
	for _, h := range houses {
		// Status counts
		statusRows, err := a.DB.BookingStatusCount(r.Context(), database.BookingStatusCountParams{
			HouseID: h.ID,
			EndAt:   now,
		})
		if err != nil {
			return err
		}
		status := make(map[string]int64)
		for _, r := range statusRows {
			status[r.Status.(string)] = r.Count
		}

		// Upcoming
		upcoming, err := a.DB.BookingUpcoming(r.Context(), database.BookingUpcomingParams{
			HouseID: h.ID,
			EndAt:   now,
		})
		if err != nil {
			return err
		}
		if len(upcoming) > 1 && upcoming[0].StartAt.After(now) {
			upcoming = upcoming[:1]
		}

		// months
		months, err := mg.gather(r.Context(), h)
		if err != nil {
			return err
		}

		vHouses = append(vHouses, viewHouse{
			House:    House(h),
			Status:   status,
			Upcoming: upcoming,
			Months:   months,
		})
	}

	return a.DashboardView.Render(rw, r, struct {
		Nav    internal.NavbarData
		Houses []viewHouse
	}{
		Nav: internal.NavbarData{
			User:             self,
			SelectedDropdown: "",
			SelectedURI:      "",
		},
		Houses: vHouses,
	})
}
