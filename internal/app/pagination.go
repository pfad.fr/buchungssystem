// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"strconv"
)

type pagination struct {
	Page       int32
	PerPage    int32
	TotalCount int32
}

func newPagination(perPageDefault int32, valueOf func(string) string) (pagination, error) {
	p := pagination{
		Page:    0,
		PerPage: perPageDefault,
	}

	if perPage := valueOf("per_page"); perPage != "" {
		i, err := strconv.ParseInt(perPage, 10, 32)
		if err != nil {
			return p, err
		}
		p.PerPage = int32(i)
	}

	if page := valueOf("page"); page != "" {
		i, err := strconv.ParseInt(page, 10, 32)
		if err != nil {
			return p, err
		}
		p.Page = int32(i)
	}

	return p, nil
}

func (p pagination) Offset() int32 {
	return p.Page * p.PerPage
}

func (p pagination) PreviousPage() int32 {
	return p.Page - 1
}

func (p pagination) HasPreviousPage() bool {
	return p.PreviousPage() >= 0
}

func (p pagination) HasNextPage() bool {
	return p.NextPage()*p.PerPage < p.TotalCount
}

func (p pagination) NextPage() int32 {
	return p.Page + 1
}

// -1 means that some pages were skipped (usually rendered as "…").
func (p pagination) Range() []int32 {
	r := []int32{0}
	lastPage := (p.TotalCount+p.PerPage-1)/p.PerPage - 1
	if p.Page <= 3 {
		// insert up to current page
		rightBound := p.Page + 1
		if rightBound < 4 {
			rightBound = 4
		}
		if rightBound > lastPage {
			rightBound = lastPage
		}
		for i := int32(1); i <= rightBound; i++ {
			r = append(r, i)
		}
		if lastPage == rightBound {
			return r
		}
		return append(r, -1, lastPage)
	}
	// skip first pages
	r = append(r, -1)

	leftBound := p.Page - 1
	if lastPage-leftBound < 4 {
		leftBound = lastPage - 4
	}

	rightBound := p.Page + 1
	if lastPage-rightBound < 3 {
		rightBound = lastPage
	}
	for i := leftBound; i <= rightBound; i++ {
		r = append(r, i)
	}
	if lastPage == rightBound {
		return r
	}
	return append(r, -1, lastPage)
}
