// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"context"
	"testing"

	"code.pfad.fr/buchungssystem/database"
	"gotest.tools/v3/assert"
)

func TestBookingTriggers(t *testing.T) {
	db := newDatabase(t, "trigger", "")
	ctx := context.Background()
	err := db.SettingHouseCreate(ctx, database.SettingHouseCreateParams{
		ID:   "trigger",
		Name: "Gachette",
	})
	assert.NilError(t, err)

	now := timeNow()
	_, err = db.BookingCreate(ctx, database.BookingCreateParams{
		HouseID:    "trigger",
		StartAt:    now,
		EndAt:      now.AddDate(0, 0, 10),
		PeopleOver: []byte("{}"),
		// Contact    string
		// Email      string
		// Phone      string
		// Comment    string
		Invoice: []byte("{}"),
	})
	assert.NilError(t, err)

	var id int32
	t.Run("successful inserts", func(t *testing.T) {
		// before
		id, err = createTestBooking(db, -2, -1)
		assert.NilError(t, err)

		// after
		_, err = createTestBooking(db, 11, 12)
		assert.NilError(t, err)
	})
	t.Run("overlapping booking", func(t *testing.T) {
		// start_at
		_, err = createTestBooking(db, -1, 1)
		assert.ErrorContains(t, err, "overlapping booking")
		// end_at
		_, err = createTestBooking(db, 9, 11)
		assert.ErrorContains(t, err, "overlapping booking")
		// both
		_, err = createTestBooking(db, -1, 11)
		assert.ErrorContains(t, err, "overlapping booking")
	})
	t.Run("insert start_at after end_at", func(t *testing.T) {
		_, err = createTestBooking(db, -1, -5)
		assert.ErrorContains(t, err, "constraint failed")
	})

	t.Run("successful updates", func(t *testing.T) {
		// before
		err = updateBooking(db, id, -20, -11)
		assert.NilError(t, err)

		// after
		err = updateBooking(db, id, 20, 30)
		assert.NilError(t, err)
	})
	t.Run("overlapping booking", func(t *testing.T) {
		// start_at
		err = updateBooking(db, id, -1, 1)
		assert.ErrorContains(t, err, "overlapping booking")
		// end_at
		err = updateBooking(db, id, 9, 11)
		assert.ErrorContains(t, err, "overlapping booking")
		// both
		err = updateBooking(db, id, -1, 11)
		assert.ErrorContains(t, err, "overlapping booking")
	})
	t.Run("update start_at after end_at", func(t *testing.T) {
		err = updateBooking(db, id, -1, -5)
		assert.ErrorContains(t, err, "constraint failed")
	})
}

func createTestBooking(db *database.Queries, startDelta, endDelta int) (int32, error) {
	booking, err := db.BookingCreate(context.Background(), database.BookingCreateParams{
		HouseID:    "trigger",
		StartAt:    timeNow().AddDate(0, 0, startDelta),
		EndAt:      timeNow().AddDate(0, 0, endDelta),
		PeopleOver: []byte("{}"),
		// Contact    string
		// Email      string
		// Phone      string
		// Comment    string
		Invoice: []byte("{}"),
	})
	return booking.ID, err
}

func updateBooking(db *database.Queries, id int32, startDelta, endDelta int) error {
	return db.BookingUpdateDates(context.Background(), database.BookingUpdateDatesParams{
		ID:      id,
		StartAt: timeNow().AddDate(0, 0, startDelta),
		EndAt:   timeNow().AddDate(0, 0, endDelta),
	})
}
