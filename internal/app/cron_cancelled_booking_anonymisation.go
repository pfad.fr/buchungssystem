// SPDX-FileCopyrightText: 2023 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"

	"code.pfad.fr/buchungssystem/database"
)

func (c cronTasks) CancelledBookingAnonymisation() error {
	ctx := context.Background() // don't interrupt while running

	bookings, err := c.DB.BookingAnonymizeCancelled(ctx, sql.NullTime{
		Time:  timeNow().AddDate(0, 0, -7), // anonymize bookings cancelled for 7 days or more
		Valid: true,
	})
	if err != nil {
		return err
	}

	var errs error
	for _, booking := range bookings {
		err := anonymizeBooking(ctx, c.DB, booking)
		if err != nil {
			errs = errors.Join(errs, err)
			continue
		}
		err = c.DB.LogEventCreate(ctx, database.LogEventCreateParams{
			BookingID: booking.ID,
			Message:   "· Anonymisiert (länger Storniert)",
		})
		if err != nil {
			errs = errors.Join(errs, err)
			continue
		}
	}

	return errs
}

func anonymizeBooking(ctx context.Context, db *database.Queries, b database.Booking) error {
	err := db.EmailAnonymize(ctx, b.ID)
	if err != nil {
		return err
	}

	err = db.LogEventAnonymize(ctx, b.ID)
	if err != nil {
		return err
	}

	var billing BillingInfo
	err = json.Unmarshal(b.Invoice, &billing)
	if err != nil {
		return err
	}

	anonBilling, err := json.Marshal(BillingInfo{
		Kind: billing.Kind,

		PostalCode: billing.PostalCode,
		City:       billing.City,
		Country:    billing.Country,
	})
	if err != nil {
		return err
	}

	err = db.BookingUpdateContact(ctx, database.BookingUpdateContactParams{
		Contact:  "",
		Email:    "",
		Phone:    "",
		Landline: "",
		Invoice:  anonBilling,
		ID:       b.ID,
	})
	if err != nil {
		return err
	}
	return nil
}
