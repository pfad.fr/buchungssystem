// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"database/sql"
	"net/http"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/buchungssystem/internal/validator"
	"code.pfad.fr/buchungssystem/internal/view"
)

func minuteOfDate(t time.Time) int {
	return 60*t.Hour() + t.Minute()
}

func (h ownBookingHandler) newOverday(rw http.ResponseWriter, r *http.Request, self *database.User, house database.House) error {
	type bookableTime struct {
		Minute             int32
		IsBookable, IsLast bool
	}
	renderData := struct {
		User        *database.User
		House       database.House
		IsUnlimited bool

		Date        time.Time
		StartMinute int32
		EndMinute   int32
		Ages        map[int32]int
		Offer       Offer
		Country     string

		Dates        []month
		StartMinutes []bookableTime
		EndMinutes   []bookableTime
		TimeLayout   string
		ChargeAges   []int32

		ExtraRequests []database.ExtraRequest

		BillingKinds     internal.Options
		BillingCountries internal.Options
		PhoneRegex       string
	}{
		User:        self,
		House:       house,
		IsUnlimited: h.HousePolicy(r).CanBookInAdvance(house.ID) == nil,

		StartMinute: -1,
		EndMinute:   -1,

		TimeLayout: dateLayout,
		Country:    "DEU",

		BillingKinds:     billingKinds,
		BillingCountries: billingCountries,
		PhoneRegex:       phoneRegex.String(),
	}

	// 2. Select start date
	now := timeNow()
	currentMinute := minuteOfDate(now)
	if house.EndMinute < int32(currentMinute) {
		now = adjustedTime(now.AddDate(0, 0, 1), 0)
	}

	type viewDay struct {
		IsBookable         bool
		IsSelected, IsLast bool
		FreeSlots          []bool
	}
	ma := monthsAccumulator{
		today: time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.Local),
	}

	interval := int32(60) // hard-coded (for now...) to 1-hour interval
	dateValue := r.FormValue("date")
	var days []database.BookableDay
	err := forBookableDays(r.Context(), h.DB, house, now, renderData.IsUnlimited, func(d database.BookableDay, date time.Time, bookingsOfTheDay []database.Booking) {
		// compute free slots
		freeSlots := make([]bool, 0, (house.EndMinute-house.StartMinute)/interval)
		hasFreeSlot := false
		for m := house.StartMinute; m < house.EndMinute; m += interval {
			hasOverlapping := false
			for len(bookingsOfTheDay) > 0 {
				b := bookingsOfTheDay[0]
				curStart := adjustedTime(date, m)
				curEnd := adjustedTime(date, m+interval-1)

				if b.EndAt.Before(curStart) {
					bookingsOfTheDay = bookingsOfTheDay[1:]
					continue
				}
				if b.StartAt.After(curEnd) {
					break
				}
				hasOverlapping = true
				break
			}

			if !hasOverlapping {
				hasFreeSlot = true
			}
			freeSlots = append(freeSlots, !hasOverlapping)
		}
		if hasFreeSlot && d.Date == dateValue {
			renderData.Date = date
			days = append(days, d)
		}
		ma.Append(date, viewDay{
			IsBookable: hasFreeSlot,
			IsSelected: d.Date == dateValue,
			FreeSlots:  freeSlots,
		})
	})
	if err != nil {
		return err
	}
	renderData.Dates = ma.Months()

	if !renderData.Date.IsZero() {
		bookings, err := h.DB.BookingOverlapping(r.Context(), database.BookingOverlappingParams{
			HouseID: house.ID,
			StartAt: adjustedTime(renderData.Date, house.StartMinute),
			EndAt:   adjustedTime(renderData.Date, house.EndMinute),
		})
		if err != nil {
			return err
		}

		v := validator.New(r.FormValue)
		startMinute := int32(-1)
		endMinute := int32(-1)
		if r.Form.Has("start") {
			startMinute = v.ParsedTimeAsMinute("start")
			if r.Form.Has("end") {
				endMinute = v.ParsedTimeAsMinute("end")
			}
		}
		if err = v.HTTPErr(); err != nil {
			return err
		}
		previousWasBookable := false
		isToday := timeNow().Format(dateLayout) == dateValue
		for m := house.StartMinute; m <= house.EndMinute; m += interval {
			if isToday && int(m) < currentMinute {
				continue // prevent past bookings
			}

			isBookable := m < house.EndMinute
			for len(bookings) > 0 {
				bookingEnd := minuteOfDate(bookings[0].EndAt)
				if bookingEnd < int(m) {
					bookings = bookings[1:]
					continue
				}

				bookingStart := minuteOfDate(bookings[0].StartAt)
				if bookingStart >= int(m+interval) {
					break
				}
				isBookable = false
				break
			}

			renderData.StartMinutes = append(renderData.StartMinutes, bookableTime{
				Minute:     m,
				IsBookable: isBookable,
				IsLast:     previousWasBookable,
			})
			if isBookable && m == startMinute {
				renderData.StartMinute = m
			}
			previousWasBookable = isBookable
		}
		if renderData.StartMinute >= 0 {
			for _, sm := range renderData.StartMinutes {
				if sm.Minute <= startMinute {
					continue
				}
				bt := bookableTime{
					Minute:     sm.Minute - 1,
					IsBookable: true,
					IsLast:     false,
				}
				renderData.EndMinutes = append(renderData.EndMinutes, bt)
				if bt.Minute == endMinute {
					renderData.EndMinute = bt.Minute
				}
				if !sm.IsBookable {
					break
				}
			}
		}
	}

	// 3. compute charges
	if renderData.EndMinute >= 0 {
		start := adjustedTime(renderData.Date, renderData.StartMinute)
		end := adjustedTime(renderData.Date, renderData.EndMinute) // -1 to prevent overlappings

		// charges
		charges, err := h.DB.BookingChargesIndex(r.Context(), database.BookingChargesIndexParams{
			HouseID: house.ID,
			Start:   start.Format(dateLayout),
			EndDate: end.Format(dateLayout),
		})
		if err != nil {
			return err
		}
		ages := chargeAges(charges)
		renderData.ChargeAges = ages

		var totalPeople int
		renderData.Ages, totalPeople, err = validateAges(ages, r.FormValue, int(renderData.House.MaxPerson))
		if err != nil {
			return err
		}

		if totalPeople > 0 {
			renderData.Offer = newOffer(days, charges, totalPeople, renderData.Ages, make(map[string]int32))

			renderData.ExtraRequests, err = h.DB.ExtraRequestsIndex(r.Context(), house.ID)
			if err != nil {
				return err
			}

			if r.Method == http.MethodPost {
				booking, err := createBooking(h.Handler, h.notification, r, house, start, end, renderData.Ages)
				if err != nil {
					return err
				}
				var userID sql.NullString
				if self != nil {
					userID.Valid = true
					userID.String = self.ID
				}
				_ = h.DB.LogEventCreate(r.Context(), database.LogEventCreateParams{
					BookingID: booking.ID,
					UserID:    userID,
					Message:   "· Buchung erstellt" + "\nPreis: " + view.FormattedCentimes(renderData.Offer.TotalAmount, ",") + "€",
				})

				return h.renderBooking(rw, r, booking, house)
			}
		}
	}

	if r.Method != http.MethodGet {
		rw.WriteHeader(http.StatusUnprocessableEntity)
	}

	return h.BookingNewOverday.Render(rw, r, renderData)
}
