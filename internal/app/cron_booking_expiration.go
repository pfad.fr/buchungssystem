// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"context"
	"database/sql"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"github.com/emersion/go-message/mail"
)

func userMustConfirmUntil(startAt, adminConfirmedAt time.Time, daysForUserConfirmation int32) time.Time {
	t := adminConfirmedAt.AddDate(0, 0, int(daysForUserConfirmation))
	if t.After(startAt) {
		t = startAt.AddDate(0, 0, -1) // at least one day before arrival
		// TODO: how to handle if booking is made less than 24h before arrival?
		// if t.Before(b.CreatedAt){

		// }
	}
	return t
}

func (c cronTasks) CheckExpiredBookings() error {
	ctx := context.Background() // don't interrupt while running

	houses, err := c.DB.SettingHouseIndex(ctx)
	if err != nil {
		return err
	}
	for _, h := range houses {
		confirmedBefore := timeNow().AddDate(0, 0, -int(h.DaysForUserConfirmation))
		startingBefore := timeNow().AddDate(0, 0, 1) // or starting within the next 24h
		bookings, err := c.DB.BookingExpired(ctx, database.BookingExpiredParams{
			HouseID: h.ID,
			AdminConfirmedAt: sql.NullTime{
				Valid: true,
				Time:  confirmedBefore,
			},
			StartAt: startingBefore,
		})
		if err != nil {
			return err
		}
		for _, b := range bookings {
			err = c.cancelExpiredBooking(ctx, b, h)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (c cronTasks) cancelExpiredBooking(ctx context.Context, booking database.Booking, house database.House) error {
	subject, body, err := c.BookingCancelled(struct {
		Booking database.Booking
		House   database.House
	}{
		Booking: booking,
		House:   house,
	})
	if err != nil {
		return err
	}

	msg := database.Email{
		BookingID: booking.ID,
		HeaderTo: (&mail.Address{
			Name:    booking.Contact,
			Address: booking.Email,
		}).String(),
		HeaderFrom:    house.ReplyTo,
		HeaderSubject: subject,
		TextPlain:     string(body),
	}
	if err = c.Queue.Push(ctx, msg); err != nil {
		return err
	}

	err = c.DB.BookingDelete(ctx, booking.ID)
	if err != nil {
		return err
	}

	_ = c.DB.LogEventCreate(ctx, database.LogEventCreateParams{
		BookingID: booking.ID,
		Message:   "· Storniert (abgelaufen)",
	})
	return nil
}
