// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"math/rand"
	"net/http"
	"net/http/httptest"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"testing"
	"time"

	"gotest.tools/v3/assert"
)

var bookingIdFromTitle = regexp.MustCompile(`Buchung (\d+) \| `)

func seedBooking(t *testing.T, firstDay int, serve func(*http.Request) *httptest.ResponseRecorder, now time.Time, isOvernight bool) int {
	r := rand.Intn(100) //nolint: gosec
	t.Log("rand", r)

	duration := rand.Intn(5) + 1 //nolint: gosec
	var req *http.Request
	if isOvernight {
		req = newRequestWithParams("POST",
			"/neue/maison?"+
				"&start="+now.AddDate(0, 0, firstDay).Format(dateLayout)+
				"&end="+now.AddDate(0, 0, firstDay+duration).Format(dateLayout)+
				"&people_over[3]="+strconv.Itoa(rand.Intn(5))+ //nolint: gosec
				"&people_over[7]="+strconv.Itoa(rand.Intn(25)+1), //nolint: gosec
			url.Values{
				"name":                 values("Erika Muster" + strconv.Itoa(r%11)),
				"email":                values("erika." + strconv.Itoa(r%11) + "@example.com"),
				"phone":                values("0123456-" + strconv.Itoa(r%11)),
				"comment":              values("bemerkung"),
				"invoice[kind]":        values("dpsg"),
				"invoice[name]":        values("Stammax"),
				"invoice[address1]":    values("Musterstrasse " + strconv.Itoa(r)),
				"invoice[address2]":    values("c/o Max"),
				"invoice[postal_code]": values("123456"),
				"invoice[city]":        values("Musterstadt"),
				"invoice[country]":     values("DEU"),
			})
	} else {
		startHour := 7 + rand.Intn(12-duration)
		date := now.AddDate(0, 0, firstDay).Format(dateLayout)
		start := strconv.Itoa(startHour) + ":00"
		end := strconv.Itoa(startHour+duration+1) + ":59"
		t.Log(date, ":", start, "-", end)
		req = newRequestWithParams("POST",
			"/neue/bootshaus?"+
				"&date="+date+
				"&start="+start+
				"&end="+end+
				"&people_over[0]="+strconv.Itoa(rand.Intn(5)+1), //nolint: gosec
			url.Values{
				"name":                 values("Erika Muster" + strconv.Itoa(r%11)),
				"email":                values("erika." + strconv.Itoa(r%11) + "@example.com"),
				"phone":                values("0123456-" + strconv.Itoa(r%11)),
				"comment":              values("bemerkung"),
				"invoice[kind]":        values("dpsg"),
				"invoice[name]":        values("Stammax"),
				"invoice[address1]":    values("Musterstrasse " + strconv.Itoa(r)),
				"invoice[address2]":    values("c/o Max"),
				"invoice[postal_code]": values("123456"),
				"invoice[city]":        values("Musterstadt"),
				"invoice[country]":     values("DEU"),
			})
	}

	resp := serve(req)
	assert.Equal(t, resp.Code, 200, resp.Body.String())
	found := bookingIdFromTitle.FindStringSubmatch(resp.Body.String())
	assert.Assert(t, len(found) == 2, "could not find booking ID in response")
	query := "id=" + found[1]

	waitForEmail(t) // admin email
	waitForEmail(t) // user email

	if r <= 75 {
		// admin confirm
		resp = serve(newRequestWithParams("PUT", "/verwaltung/buchung/akzeptieren?"+query, nil))
		assert.Equal(t, resp.Code, 302)

		msg := waitForEmail(t) // user email

		if r <= 50 {
			// user-confirmation
			confirmationURL := findURL(t, msg.Reader)
			t.Log(confirmationURL)
			assert.Check(t, strings.Contains(confirmationURL, query))
			resp = serve(httptest.NewRequest("GET", confirmationURL, nil))
			assert.Equal(t, resp.Code, 200)
			resp = serve(newRequestWithParams("POST", confirmationURL, url.Values{
				"confirm": values("1"),
			}))
			assert.Equal(t, resp.Code, 302)

			waitForEmail(t) // user email
		}
	}

	if r%10 == 0 {
		// admin cancel
		resp = serve(newRequestWithParams("PUT", "/verwaltung/buchung/stornieren?"+query, url.Values{
			"reason": values("excuse"),
		}))
		assert.Equal(t, resp.Code, 302)
		waitForEmail(t)
		return 0 // it got deleted
	}
	return duration
}
