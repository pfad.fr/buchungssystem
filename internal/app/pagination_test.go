// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"fmt"
	"testing"

	"gotest.tools/v3/assert"
)

func TestPaginationRange(t *testing.T) {
	cases := []struct {
		p        pagination
		expected []int32
	}{
		{
			p: pagination{
				Page:       0,
				PerPage:    10,
				TotalCount: 10,
			},
			expected: []int32{0},
		},
		{
			p: pagination{
				Page:       0,
				PerPage:    10,
				TotalCount: 30,
			},
			expected: []int32{0, 1, 2},
		},
		{
			p: pagination{
				Page:       2,
				PerPage:    10,
				TotalCount: 30,
			},
			expected: []int32{0, 1, 2},
		},
		{
			p: pagination{
				Page:       0,
				PerPage:    10,
				TotalCount: 100,
			},
			expected: []int32{0, 1, 2, 3, 4, -1, 9},
		},
		{
			p: pagination{
				Page:       3,
				PerPage:    10,
				TotalCount: 100,
			},
			expected: []int32{0, 1, 2, 3, 4, -1, 9},
		},
		{
			p: pagination{
				Page:       4,
				PerPage:    10,
				TotalCount: 100,
			},
			expected: []int32{0, -1, 3, 4, 5, -1, 9},
		},
		{
			p: pagination{
				Page:       5,
				PerPage:    10,
				TotalCount: 100,
			},
			expected: []int32{0, -1, 4, 5, 6, -1, 9},
		},
		{
			p: pagination{
				Page:       6,
				PerPage:    10,
				TotalCount: 100,
			},
			expected: []int32{0, -1, 5, 6, 7, 8, 9},
		},
		{
			p: pagination{
				Page:       7,
				PerPage:    10,
				TotalCount: 100,
			},
			expected: []int32{0, -1, 5, 6, 7, 8, 9},
		},
		{
			p: pagination{
				Page:       8,
				PerPage:    10,
				TotalCount: 100,
			},
			expected: []int32{0, -1, 5, 6, 7, 8, 9},
		},
		{
			p: pagination{
				Page:       9,
				PerPage:    10,
				TotalCount: 100,
			},
			expected: []int32{0, -1, 5, 6, 7, 8, 9},
		},
		{
			p: pagination{
				Page:       0,
				PerPage:    32,
				TotalCount: 48,
			},
			expected: []int32{0, 1},
		},
	}
	for _, c := range cases {
		t.Run(fmt.Sprintf("%#v", c.p), func(t *testing.T) {
			got := c.p.Range()
			assert.DeepEqual(t, c.expected, got)
		})
	}
}
