// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"encoding/json"
	"net/http"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/buchungssystem/internal/validator"
)

func (a app) BookingBillingEdit(rw http.ResponseWriter, r *http.Request) error {
	self, err := a.CurrentUser(r)
	if err != nil {
		return err
	}

	booking, house, err := a.editBooking(r)
	if err != nil {
		return err
	}

	var billing BillingInfo
	if err = json.Unmarshal(booking.Invoice, &billing); err != nil {
		return err
	}

	return a.BookingBillingEditView.Render(rw, r, struct {
		Nav              internal.NavbarData
		Booking          database.Booking
		House            database.House
		Billing          BillingInfo
		BillingKinds     internal.Options
		BillingCountries internal.Options
		PhoneRegex       string
	}{
		Nav: internal.NavbarData{
			User:             self,
			SelectedDropdown: "buchung",
			SelectedURI:      "buchung/alle",
		},
		Booking:          booking,
		House:            house,
		Billing:          billing,
		BillingKinds:     billingKinds,
		BillingCountries: billingCountries,
		PhoneRegex:       phoneRegex.String(),
	})
}

func (a app) BookingBillingUpdate(rw http.ResponseWriter, r *http.Request) error {
	booking, _, err := a.editBooking(r)
	if err != nil {
		return err
	}

	billing, err := parseBillingInfo(func(s string) string {
		return r.FormValue("invoice[" + s + "]")
	})
	if err != nil {
		return err
	}

	billingJSON, err := json.Marshal(billing)
	if err != nil {
		return err
	}

	v := validator.New(r.FormValue)
	params := database.BookingUpdateContactParams{
		ID:      booking.ID,
		Contact: v.TrimmedString("name"),
		Email:   v.Email("email"),
		Phone:   v.TrimmedString("phone", phoneRegex.MatchString),
		Landline: v.TrimmedString("landline", func(s string) bool {
			if s == "" {
				return true
			}
			return phoneRegex.MatchString(s)
		}),
		Invoice: billingJSON,
	}
	if err = v.HTTPErr(); err != nil {
		return err
	}

	if err = a.DB.BookingUpdateContact(r.Context(), params); err != nil {
		return err
	}

	http.Redirect(rw, r, "/verwaltung/buchung/bearbeiten?id="+r.FormValue("id"), http.StatusFound)
	return nil
}
