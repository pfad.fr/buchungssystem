// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"bytes"
	"context"
	"database/sql"
	"io"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"regexp"
	"strings"
	"sync"
	"testing"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/buchungssystem/internal/email"
	"code.pfad.fr/buchungssystem/internal/validator"
	"code.pfad.fr/buchungssystem/internal/view"
	"github.com/emersion/go-message/mail"
	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
	"github.com/go-chi/chi/v5"
	"github.com/pressly/goose/v3"
	"gotest.tools/v3/assert"

	_ "github.com/mattn/go-sqlite3"
)

func newDatabase(t *testing.T, memName, fileName string) *database.Queries {
	dataSource := "file:" + memName + "?mode=memory&cache=shared&_journal_mode=WAL&_busy_timeout=5000&_foreign_keys=on"
	if fileName != "" {
		os.Remove(fileName)
		dataSource = fileName + "?_journal_mode=WAL&_busy_timeout=5000&_foreign_keys=on"
	}
	db, err := sql.Open("sqlite3", dataSource)
	assert.NilError(t, err)

	migrate, err := database.MissingMigrations(db, "sqlite3")
	assert.NilError(t, err)

	queries := database.New(db)
	if migrate != nil {
		goose.SetLogger(goose.NopLogger())
		err := migrate()
		assert.NilError(t, err)

		err = queries.AuthInsert(context.Background(), database.AuthInsertParams{
			ID:    "admin1",
			Email: "admin1@example.com",
			Name:  "Admin 1",
		})
		assert.NilError(t, err)

		err = queries.SettingUserUpdate(context.Background(), database.SettingUserUpdateParams{
			AuthorizedAt: sql.NullTime{
				Time:  timeNow(),
				Valid: true,
			},
			Role: "admin",
			ID:   "admin1",
		})
		assert.NilError(t, err)

		err = queries.AuthInsert(context.Background(), database.AuthInsertParams{
			ID:    "AAAAAAAAAAAAAAAAAAAAAF4Wi8Bbjj9_eCJwcTLPSEU",
			Email: "oc@atelier.dev",
			Name:  "Olivier",
		})
		assert.NilError(t, err) // admin role will be set in tests
	}

	return queries
}

type entity struct {
	*mail.Reader
	replyTo *mail.Address
	to      []*mail.Address
	subject string
}

var sentEmails = make(chan entity)

func waitForEmail(t *testing.T) entity {
	t.Helper()
	delay := time.Second
	if os.Getenv("TESTS_ON_ACTUAL_SMTP") == "1" {
		delay *= 5
	}
	select {
	case e := <-sentEmails:
		t.Log("EM@IL", e.to[0].Address, e.subject)
		return e
	case <-time.After(delay):
		t.Fatal("no email sent within " + delay.String())
		return entity{}
	}
}

func sendEmail(from string, to []string, r io.Reader) error {
	auth := sasl.NewPlainClient("", os.Getenv("SMTP_USER"), os.Getenv("SMTP_PASSWORD"))
	return smtp.SendMail(os.Getenv("SMTP_ADDR"), auth, from, to, r)
}

func findURL(t *testing.T, msg *mail.Reader) string {
	t.Helper()
	part, err := msg.NextPart()
	assert.NilError(t, err)
	body, err := io.ReadAll(part.Body)
	assert.NilError(t, err)

	re := regexp.MustCompile(`http:\/\/\S+`)
	httpURL := re.Find(body)

	assert.Check(t, httpURL != nil)
	return string(httpURL)
}

func authenticatedServer(t *testing.T) func(*http.Request) *httptest.ResponseRecorder {
	r := chi.NewRouter()

	db := newDatabase(t, "e2e", os.Getenv("TESTS_ON_ACTUAL_DB"))
	var wg sync.WaitGroup
	queue, err := email.NewQueue(context.Background(), db, mail.Address{
		Name:    "DPSG München und Freising - Buchungssystem",
		Address: "haeuser2@dpsg1300.de",
	}, timeNow(), emailSender(t, &wg))
	assert.NilError(t, err)

	if os.Getenv("TESTS_ON_ACTUAL_SMTP") == "1" {
		t.Cleanup(wg.Wait)
	}
	notifs := notifs(internal.EmbedFS)

	ownBaseHandler := internal.Handler{
		BaseURL: "http://localhost:8000",
		DB:      db,
		Queue:   queue,
		CurrentUser: func(r *http.Request) (*database.User, error) {
			user, err := db.AuthLoad(r.Context(), "admin1")
			return &user, err
		},
		ErrorHandler: internal.ErrorHandler{
			Debug: true,
		},
		RenderErrorView: view.NewTemplate(internal.EmbedFS, "doctype", "layout/own", "httperror").Render,
	}
	r.Route("/", OwnBookingRouter(ownBaseHandler, internal.EmbedFS, notifs))
	adminBaseHandler := internal.Handler{
		BaseURL: "http://localhost:8000",
		DB:      db,
		Queue:   queue,
		CurrentUser: func(r *http.Request) (*database.User, error) {
			user, err := db.AuthLoad(r.Context(), "admin1")
			return &user, err
		},
		ErrorHandler: internal.ErrorHandler{
			Debug: true,
		},
		RenderErrorView: view.NewTemplate(internal.EmbedFS, "doctype", "layout/admin", "httperror").Render,
	}
	r.Route("/verwaltung", AuthenticatedRouter(adminBaseHandler, internal.EmbedFS, notifs))
	return func(req *http.Request) *httptest.ResponseRecorder {
		resp := httptest.NewRecorder()
		r.ServeHTTP(resp, req)
		return resp
	}
}

func emailSender(t *testing.T, wg *sync.WaitGroup) func(from string, to []string, r io.Reader) error {
	return func(from string, to []string, r io.Reader) error {
		b, err := io.ReadAll(r)
		if err != nil {
			return err
		}
		if os.Getenv("TESTS_ON_ACTUAL_SMTP") == "1" {
			wg.Add(1)
			go func() {
				defer wg.Done()
				sendErr := sendEmail(from, to, bytes.NewReader(b))
				if sendErr != nil {
					t.Error(sendErr)
				}
			}()
		}

		ent, err := mail.CreateReader(bytes.NewReader(b))
		if err != nil {
			return err
		}

		headerFrom, err := mail.ParseAddress(ent.Header.Get("From"))
		if err != nil {
			return err
		}
		assert.Equal(t, headerFrom.Address, from)
		assert.Equal(t, headerFrom.Address, "haeuser2@dpsg1300.de")
		assert.Equal(t, headerFrom.Name, "DPSG München und Freising - Buchungssystem")

		headerTo, err := mail.ParseAddressList(ent.Header.Get("To"))
		if err != nil {
			return err
		}
		assert.Equal(t, len(headerTo), len(to))
		for i := 0; i < len(to); i++ {
			assert.Equal(t, headerTo[i].Address, to[i])
		}

		replyTo, err := mail.ParseAddress(ent.Header.Get("Reply-To"))
		if err != nil {
			return err
		}
		subject, err := ent.Header.Subject()
		if err != nil {
			return err
		}
		sentEmails <- entity{
			Reader:  ent,
			replyTo: replyTo,
			to:      headerTo,
			subject: subject,
		}
		return nil
	}
}

var whitespace = regexp.MustCompile(`[ \n]+`)

func bodyCheckContains(t *testing.T, body, needle string) bool {
	t.Helper()
	s := whitespace.ReplaceAllString(body, " ")

	return assert.Check(t, strings.Contains(s, needle), body)
}

func TestEndToEndAdmin(t *testing.T) {
	validator.EnableFakeDNSResolver()
	timeNow = func() time.Time {
		return time.Now().Add(-60 * 24 * time.Hour)
	}

	serve := authenticatedServer(t)
	var resp *httptest.ResponseRecorder

	t.Run("house creation and update", func(t *testing.T) {
		// empty house list
		resp = serve(httptest.NewRequest("GET", "/verwaltung/einstellungen/haus", nil))
		assert.Equal(t, resp.Code, 200)

		// create house
		resp = serve(newRequestWithParams("POST", "/verwaltung/einstellungen/haus", url.Values{
			"id":         values("maison"),
			"name":       values("Seegatterl"),
			"max_person": values("42"),
			"reply_to":   values("seegatterl-buchung@dpsg1300.de"),
		}))
		assertRedirect(t, resp, "/verwaltung/einstellungen/")

		resp = serve(newRequestWithParams("POST", "/verwaltung/einstellungen/haus", url.Values{
			"id":         values("see"),
			"name":       values("Thalhäusl"),
			"max_person": values("42"),
			"reply_to":   values("mer-buchung@dpsg1300.de"),
		}))
		assertRedirect(t, resp, "/verwaltung/einstellungen/")

		resp = serve(newRequestWithParams("POST", "/verwaltung/einstellungen/haus", url.Values{
			"id":         values("bootshaus"),
			"name":       values("Bootshaus"),
			"max_person": values("42"),
			"reply_to":   values("bootshaus-buchung@dpsg1300.de"),
		}))
		assertRedirect(t, resp, "/verwaltung/einstellungen/")
		resp = serve(newRequestWithParams("PUT", "/verwaltung/einstellungen/haus", url.Values{
			"id":           values("bootshaus"),
			"name":         values("Bootshaus"),
			"max_person":   values("42"),
			"reply_to":     values("bootshaus-buchung@dpsg1300.de"),
			"start_time":   values("7:00"),
			"end_time":     values("20:00"),
			"booking_type": values("overday"),
		}))
		assertRedirect(t, resp, "/verwaltung/einstellungen/")
	})

	t.Run("user list and rights", func(t *testing.T) {
		// update other's rights regarding the house
		resp = serve(newRequestWithParams("PUT", "/verwaltung/einstellungen/benutzer", url.Values{
			"id":               values("AAAAAAAAAAAAAAAAAAAAAF4Wi8Bbjj9_eCJwcTLPSEU"),
			"house[maison]":    values("admin"),
			"house[see]":       values("admin"),
			"house[bootshaus]": values("admin"),
			"authorized":       values("1"),
			"is_admin":         values("1"),
		}))
		assertRedirect(t, resp, "/verwaltung/einstellungen/")
	})

	var priceIDs []string
	t.Run("price creation and update", func(t *testing.T) {
		// create price
		resp = serve(newRequestWithParams("POST", "/verwaltung/einstellungen/preissaison", url.Values{
			"name": values("Prix"),
		}))
		assertRedirect(t, resp, "/verwaltung/einstellungen/")

		// price list with created price
		resp = serve(httptest.NewRequest("GET", "/verwaltung/einstellungen/preissaison", nil))
		assert.Equal(t, resp.Code, 200)
		priceIDs = findMatches(resp.Body.String(), `name="id" value="([^"]+)"`)
	})

	t.Run("charge creation and update", func(t *testing.T) {
		editURL := "/verwaltung/einstellungen/kosten/bearbeiten?price_id=" + priceIDs[0] + "&house_id=maison"

		// create charge for later estimation
		resp = serve(newRequestWithParams("POST", editURL, url.Values{
			"name[]":   values("Reinigungspauschale (1.OG)"),
			"unit[]":   values("per_stay"),
			"amount[]": values("80.00"),
		}))
		assertRedirect(t, resp, "")
		resp = serve(newRequestWithParams("POST", editURL, url.Values{
			"name[]":                      values("Reinigungspauschale (2.OG)"),
			"description[]":               values("Die Reinigungspauschale wird je belegtem Stockwerk erhoben. Ab einer Gruppengröße von 25 Beleger benötigen Sie beide Schlafgeschosse (1.OG und 2.OG)."),
			"unit[]":                      values("per_stay"),
			"amount[]":                    values("80.00"),
			"charged_over_person_count[]": values("25"),
		}))
		assertRedirect(t, resp, "")
		resp = serve(newRequestWithParams("POST", editURL, url.Values{
			"name[]":               values("Übernachtung"),
			"unit[]":               values("per_person_night"),
			"amount[]":             values("13"),
			"min_person_charged[]": values("20"),
			"exempted_under_age[]": values("3"),
		}))
		assertRedirect(t, resp, "")
		resp = serve(newRequestWithParams("POST", editURL, url.Values{
			"name[]":               values("Kurtaxe der Gemeinde Fischbachau"),
			"description[]":        values("Die Kurtaxe wird von der Gemeinde Fischbachau erhoben und komplett an die Gemeinde weitergereicht. Es handelt sich hierbei um eine ermäßigte Kurtaxe für Jugendgästehäuser. Befreit von der Kurtaxe sind lediglich Bildungsveranstaltungen. Wenn dies auf Sie zutrifft teilen Sie uns dies bitte mit Nachweis mit, wir deaktivieren die Berechnung dann in Ihrer Buchung."),
			"unit[]":               values("per_person_night"),
			"amount[]":             values("1"),
			"exempted_under_age[]": values("7"),
		}))
		assertRedirect(t, resp, "")
		resp = serve(newRequestWithParams("POST", editURL, url.Values{
			"name[]":               values("DPSG Rabatt"),
			"unit[]":               values("per_person_night"),
			"amount[]":             values("-4"),
			"min_person_charged[]": values("20"),
			"exempted_under_age[]": values("3"),
			"optionality[]":        values("admin"),
		}))
		assertRedirect(t, resp, "")
		resp = serve(newRequestWithParams("POST", editURL, url.Values{
			"name[]":               values("Kurtaxe Befreiung (Bildungsveranstaltung)"),
			"unit[]":               values("per_person_night"),
			"amount[]":             values("-1"),
			"exempted_under_age[]": values("7"),
			"optionality[]":        values("admin"),
		}))
		assertRedirect(t, resp, "")

		resp = serve(newRequestWithParams("POST", editURL, url.Values{
			"name[]":        values("Besondere An-/Abreisezeit"),
			"unit[]":        values("per_stay"),
			"amount[]":      values("100"),
			"optionality[]": values("admin"),
		}))
		assertRedirect(t, resp, "")
		resp = serve(newRequestWithParams("POST", editURL, url.Values{
			"name[]":        values("Geschirrtuchverleih (Kurzbelegungen)"),
			"description[]": values("Pauschale max. 2 Tage für Waschen, Bügeln und Abnutzung"),
			"unit[]":        values("per_stay"),
			"amount[]":      values("5"),
			"optionality[]": values("admin"),
		}))
		assertRedirect(t, resp, "")

		// full price list
		resp = serve(httptest.NewRequest("GET", "/verwaltung/einstellungen/kosten?price_id="+priceIDs[0], nil))
		assert.Equal(t, resp.Code, 200)
	})

	now := timeNow()
	t.Run("booking day creation", func(t *testing.T) {
		bookingDayURL := "/verwaltung/einstellungen/buchungsnaechte"
		// empty day list
		resp = serve(httptest.NewRequest("GET", bookingDayURL, nil))
		assert.Equal(t, resp.Code, 200)

		// insert days without restriction
		resp = serve(newRequestWithParams("PUT", bookingDayURL, url.Values{
			"start":       values(now.AddDate(0, -1, 0).Format(dateLayout)),
			"end":         values(now.AddDate(0, 0, 360).Format(dateLayout)),
			"house_id[]":  values("maison", "bootshaus"),
			"price_id":    values(priceIDs[0]),
			"restriction": values("none"),
			"reason":      values(""),
		}))
		assertRedirect(t, resp, "")

		// full day list
		resp = serve(httptest.NewRequest("GET", bookingDayURL, nil))
		assert.Equal(t, resp.Code, 200)
	})

	t.Run("booking creation steps", func(t *testing.T) {
		// no params
		resp = serve(httptest.NewRequest("GET", "/verwaltung/buchung/erstellen", nil))
		assert.Equal(t, resp.Code, 200)

		// house_id
		resp = serve(httptest.NewRequest("GET", "/neue/maison", nil))
		assert.Equal(t, resp.Code, 200)

		// start
		resp = serve(httptest.NewRequest("GET",
			"/neue/maison?_=_&start="+
				now.AddDate(0, 0, 1).Format(dateLayout),
			nil))
		assert.Equal(t, resp.Code, 200)

		// end
		resp = serve(httptest.NewRequest("GET",
			"/neue/maison?_=_"+
				"&start="+now.AddDate(0, 0, 1).Format(dateLayout)+
				"&end="+now.AddDate(0, 0, 3).Format(dateLayout),
			nil))
		assert.Equal(t, resp.Code, 200)

		// ages
		resp = serve(httptest.NewRequest("GET",
			"/neue/maison?_=_"+
				"&start="+now.AddDate(0, 0, 1).Format(dateLayout)+
				"&end="+now.AddDate(0, 0, 3).Format(dateLayout)+
				"&people_over[7]=3",
			nil))
		assert.Equal(t, resp.Code, 200)
		bodyCheckContains(t, resp.Body.String(), "Preis für 2&nbsp;Nächte")
	})

	t.Run("booking 1 deleted immediately", func(t *testing.T) {
		query := "id=1"
		resp = serve(newRequestWithParams("POST",
			"/neue/maison?_=_"+
				"&start="+now.AddDate(0, 0, 2).Format(dateLayout)+
				"&end="+now.AddDate(0, 0, 4).Format(dateLayout)+
				"&people_over[7]=3",
			url.Values{
				"name":                 values("Max Mustermann"),
				"email":                values("max@example.com"),
				"phone":                values("0123456"),
				"comment":              values("bemerkung"),
				"invoice[kind]":        values("dpsg"),
				"invoice[name]":        values("Stammax"),
				"invoice[address1]":    values("Musterstrasse"),
				"invoice[address2]":    values("c/o Max"),
				"invoice[postal_code]": values("123456"),
				"invoice[city]":        values("Musterstadt"),
				"invoice[country]":     values("DEU"),
				"extrarequest[]":       values("lagerfeuer", "holz"),
			}))
		assert.Equal(t, resp.Code, 200)
		bodyCheckContains(t, resp.Body.String(), "Danke für deine Anfrage")

		// booking was received
		adminMsg := waitForEmail(t)
		assert.Check(t, strings.HasPrefix(adminMsg.subject, "Anfrage #1 - Seegatterl"), adminMsg.subject)
		assert.Equal(t, adminMsg.to[0].Address, "seegatterl-buchung@dpsg1300.de")
		assert.Equal(t, adminMsg.replyTo.Address, "max@example.com")

		userMsg := waitForEmail(t)
		assert.Equal(t, userMsg.subject, "Anfrage Seegatterl - #1 - Anfrage ist angekommen")
		assert.Equal(t, userMsg.to[0].Address, "max@example.com")
		assert.Equal(t, userMsg.replyTo.Address, "seegatterl-buchung@dpsg1300.de")

		// Admin adjust start time (datetime)
		resp = serve(newRequestWithParams("PUT",
			"/verwaltung/buchung/zeitraum?id=1",
			url.Values{
				"start_date": values(now.AddDate(0, 0, 3).Format(dateLayout)),
				"end_date":   values(now.AddDate(0, 0, 4).Format(dateLayout)),
				"start_time": values("8:00"),
				"end_time":   values("19:00"),
			}))
		assert.Equal(t, resp.Code, 302)

		// Admin adjust people
		resp = serve(newRequestWithParams("PUT",
			"/verwaltung/buchung/personen?id=1",
			url.Values{
				"people_over[7]": values("23"),
				"people_over[3]": values("3"),
			}))
		assert.Equal(t, resp.Code, 302)

		// POST overlapping stay (before)
		resp = serve(newRequestWithParams("POST",
			"/neue/maison?_=_"+
				"&start="+now.AddDate(0, 0, 1).Format(dateLayout)+
				"&end="+now.AddDate(0, 0, 3).Format(dateLayout)+
				"&people_over[7]=3",
			url.Values{
				"name":                 values("Max Mustermann"),
				"email":                values("max@example.com"),
				"phone":                values("0123456"),
				"invoice[kind]":        values("dpsg"),
				"invoice[name]":        values("Stammax"),
				"invoice[address1]":    values("Musterstrasse"),
				"invoice[postal_code]": values("123456"),
				"invoice[city]":        values("Musterstadt"),
				"invoice[country]":     values("DEU"),
			}))
		assert.Equal(t, resp.Code, 422)

		// Admin adjust start time again, for later booking at the same date
		resp = serve(newRequestWithParams("PUT",
			"/verwaltung/buchung/zeitraum?id=1",
			url.Values{
				"start_date": values(now.AddDate(0, 0, 3).Format(dateLayout)),
				"end_date":   values(now.AddDate(0, 0, 4).Format(dateLayout)),
				"start_time": values("15:01"),
				"end_time":   values("19:00"),
			}))
		assert.Equal(t, resp.Code, 302)

		// POST not (anymore) overlapping stay (before)
		resp = serve(httptest.NewRequest("GET",
			"/neue/maison?_=_"+
				"&start="+now.AddDate(0, 0, 2).Format(dateLayout)+
				"&end="+now.AddDate(0, 0, 3).Format(dateLayout)+
				"&people_over[7]=3", nil))
		assert.Equal(t, resp.Code, 200)
		bodyCheckContains(t, resp.Body.String(), "1&nbsp;Nacht")

		// POST overlapping stay (after)
		resp = serve(newRequestWithParams("POST",
			"/neue/maison?_=_"+
				"&start="+now.AddDate(0, 0, 3).Format(dateLayout)+
				"&end="+now.AddDate(0, 0, 4).Format(dateLayout)+
				"&people_over[7]=3",
			url.Values{
				"name":                 values("Max Mustermann"),
				"email":                values("max@example.com"),
				"phone":                values("0123456"),
				"comment":              values("bemerkung"),
				"invoice[kind]":        values("dpsg"),
				"invoice[name]":        values("Stammax"),
				"invoice[address1]":    values("Musterstrasse"),
				"invoice[address2]":    values("c/o Max"),
				"invoice[postal_code]": values("123456"),
				"invoice[city]":        values("Musterstadt"),
				"invoice[country]":     values("DEU"),
			}))
		assert.Equal(t, resp.Code, 422)

		// admin Display
		resp = serve(httptest.NewRequest("GET", "/verwaltung/buchung/bearbeiten?"+query, nil))
		assert.Equal(t, resp.Code, 200)
		bodyCheckContains(t, resp.Body.String(), "status-accept")
		bodyCheckContains(t, resp.Body.String(), "lagerfeuer")

		// Delete
		resp = serve(newRequestWithParams("PUT", "/verwaltung/buchung/stornieren?"+query, url.Values{
			"reason":       values("excuse"),
			"admin_reason": values("dont like it"),
		}))
		assert.Equal(t, resp.Code, 302)

		msg := waitForEmail(t)
		assert.Equal(t, msg.subject, "Anfrage Seegatterl - #1 - Stornierung")
		assert.Equal(t, msg.to[0].Address, "max@example.com")
		assert.Equal(t, msg.replyTo.Address, "seegatterl-buchung@dpsg1300.de")

		// Comment
		resp = serve(newRequestWithParams("POST", "/verwaltung/buchung/kommentar?"+query, url.Values{
			"message": values("welcome to my life"),
		}))
		assert.Equal(t, resp.Code, 302)

		resp = serve(httptest.NewRequest("GET", "/verwaltung/buchung/bearbeiten?"+query, nil))
		assert.Equal(t, resp.Code, 200)
		assert.Check(t, !strings.Contains(resp.Body.String(), "status-accept"))
		bodyCheckContains(t, resp.Body.String(), "Storniert")
		bodyCheckContains(t, resp.Body.String(), "excuse")
		bodyCheckContains(t, resp.Body.String(), "dont like it")
		bodyCheckContains(t, resp.Body.String(), "welcome to my life")
	})

	t.Run("booking 2 admin-confirmed and then deleted", func(t *testing.T) {
		query := "id=2"
		resp = serve(newRequestWithParams("POST",
			"/neue/maison?_=_"+
				"&start="+now.AddDate(0, 0, 10).Format(dateLayout)+
				"&end="+now.AddDate(0, 0, 12).Format(dateLayout)+
				"&people_over[7]=3",
			url.Values{
				"name":                 values("Erika Musterfrau"),
				"email":                values("erika@example.com"),
				"phone":                values("0123456"),
				"comment":              values("bemerkung"),
				"invoice[kind]":        values("dpsg"),
				"invoice[name]":        values("Stammax"),
				"invoice[address1]":    values("Musterstrasse"),
				"invoice[address2]":    values("c/o Max"),
				"invoice[postal_code]": values("123456"),
				"invoice[city]":        values("Musterstadt"),
				"invoice[country]":     values("DEU"),
			}))
		assert.Equal(t, resp.Code, 200)
		bodyCheckContains(t, resp.Body.String(), "Danke für deine Anfrage")
		waitForEmail(t) // admin email
		waitForEmail(t) // user email

		// Display
		resp = serve(httptest.NewRequest("GET", "/verwaltung/buchung/bearbeiten?"+query, nil))
		assert.Equal(t, resp.Code, 200)
		bodyCheckContains(t, resp.Body.String(), "status-accept")

		// Accept
		resp = serve(newRequestWithParams("PUT", "/verwaltung/buchung/akzeptieren?"+query, nil))
		assert.Equal(t, resp.Code, 302)

		waitForEmail(t) // user email

		resp = serve(httptest.NewRequest("GET", "/verwaltung/buchung/bearbeiten?"+query, nil))
		assert.Equal(t, resp.Code, 200)
		assert.Check(t, !strings.Contains(resp.Body.String(), "status-accept"))
		bodyCheckContains(t, resp.Body.String(), "Ausstehend")

		// Delete
		resp = serve(newRequestWithParams("PUT", "/verwaltung/buchung/stornieren?"+query, url.Values{
			"reason":       values("excuse"),
			"admin_reason": values("dont like it"),
		}))
		assert.Equal(t, resp.Code, 302)

		resp = serve(httptest.NewRequest("GET", "/verwaltung/buchung/bearbeiten?"+query, nil))
		assert.Equal(t, resp.Code, 200)
		assert.Check(t, !strings.Contains(resp.Body.String(), "status-accept"))
		bodyCheckContains(t, resp.Body.String(), "Storniert")
		bodyCheckContains(t, resp.Body.String(), "excuse")
		waitForEmail(t) // user email
	})

	t.Run("booking 3 admin-confirmed, user-confirmed and then deleted", func(t *testing.T) {
		query := "id=3"
		resp = serve(newRequestWithParams("POST",
			"/neue/maison?_=_"+
				"&start="+now.AddDate(0, 0, 30).Format(dateLayout)+
				"&end="+now.AddDate(0, 0, 35).Format(dateLayout)+
				"&people_over[7]=3",
			url.Values{
				"name":                 values("Otto Normalverbraucher"),
				"email":                values("otto@example.com"),
				"phone":                values("0123456"),
				"comment":              values("Wir sind stark und mutig, wir schaffen das auf jeden Fall!!!\nAber klaro..."),
				"invoice[kind]":        values("dpsg"),
				"invoice[name]":        values("Stammax"),
				"invoice[address1]":    values("Musterstrasse"),
				"invoice[address2]":    values("c/o Max"),
				"invoice[postal_code]": values("123456"),
				"invoice[city]":        values("Musterstadt"),
				"invoice[country]":     values("DEU"),
			}))
		assert.Equal(t, resp.Code, 200)
		bodyCheckContains(t, resp.Body.String(), "Danke für deine Anfrage")
		waitForEmail(t) // admin email
		waitForEmail(t) // user email

		// Display
		resp = serve(httptest.NewRequest("GET", "/verwaltung/buchung/bearbeiten?"+query, nil))
		assert.Equal(t, resp.Code, 200)
		bodyCheckContains(t, resp.Body.String(), "status-accept")

		// Accept
		resp = serve(newRequestWithParams("PUT", "/verwaltung/buchung/akzeptieren?"+query, nil))
		assert.Equal(t, resp.Code, 302)

		resp = serve(httptest.NewRequest("GET", "/verwaltung/buchung/bearbeiten?"+query, nil))
		assert.Equal(t, resp.Code, 200)
		assert.Check(t, !strings.Contains(resp.Body.String(), "status-accept"))
		bodyCheckContains(t, resp.Body.String(), "Ausstehend")

		msg := waitForEmail(t) // user email
		assert.Equal(t, msg.subject, "Anfrage Seegatterl - #3 - Buchung bitte bestätigen")
		assert.Equal(t, msg.to[0].Address, "otto@example.com")
		assert.Equal(t, msg.replyTo.Address, "seegatterl-buchung@dpsg1300.de")

		// user-confirmation
		confirmationURL := findURL(t, msg.Reader)
		t.Log(confirmationURL)
		assert.Check(t, strings.Contains(confirmationURL, query))

		// the URL gets invalid if something is appended
		for _, suffix := range []string{"ö", "-"} {
			resp = serve(httptest.NewRequest("GET", confirmationURL+suffix, nil))
			assert.Equal(t, resp.Code, 401)
			bodyCheckContains(t, resp.Body.String(), "<!DOCTYPE html>")
		}

		resp = serve(httptest.NewRequest("GET", confirmationURL, nil))
		assert.Equal(t, resp.Code, 200)

		resp = serve(newRequestWithParams("POST", confirmationURL, url.Values{
			"confirm": values("1"),
		}))
		assert.Equal(t, resp.Code, 302)

		waitForEmail(t) // user email

		// Delete
		resp = serve(newRequestWithParams("PUT", "/verwaltung/buchung/stornieren?"+query, url.Values{
			"reason":       values("excuse"),
			"admin_reason": values("dont like it"),
		}))
		assert.Equal(t, resp.Code, 302)
		waitForEmail(t) // user email
	})

	t.Run("booking 4 admin-pending", func(t *testing.T) {
		resp = serve(newRequestWithParams("POST",
			"/neue/maison?_=_"+
				"&start="+now.AddDate(0, 0, 40).Format(dateLayout)+
				"&end="+now.AddDate(0, 0, 45).Format(dateLayout)+
				"&people_over[7]=3",
			url.Values{
				"name":  values("Lieschen Müller"),
				"email": values("Lieschen@examplE.com"),
				"phone": values("012345678900"),
				// "comment":              values(""),
				"invoice[kind]":     values("privat"),
				"invoice[name]":     values("Lieschen Müller"),
				"invoice[address1]": values("Musterstrasse"),
				// "invoice[address2]":    values("c/o Max"),
				"invoice[postal_code]": values("12345"),
				"invoice[city]":        values("Musterstadt"),
				"invoice[country]":     values("DEU"),
			}))
		assert.Equal(t, resp.Code, 200)
		bodyCheckContains(t, resp.Body.String(), "Danke für deine Anfrage")
		waitForEmail(t) // admin email
		waitForEmail(t) // user email
	})

	t.Run("booking 5 admin-confirmed, user-pending", func(t *testing.T) {
		query := "id=5"
		resp = serve(newRequestWithParams("POST",
			"/neue/maison?_=_"+
				"&start="+now.AddDate(0, 0, 45).Format(dateLayout)+
				"&end="+now.AddDate(0, 0, 46).Format(dateLayout)+
				"&people_over[7]=1",
			url.Values{
				"name":  values("Erika Musterfrau"),
				"email": values("erika@examplE.com"),
				"phone": values("0033476940030"),
				// "comment":              values(""),
				"invoice[kind]":        values("dpsg"),
				"invoice[name]":        values("Maxi"),
				"invoice[address1]":    values("Musterstrasse"),
				"invoice[address2]":    values("c/o Max Mustermann"),
				"invoice[postal_code]": values("123456"),
				"invoice[city]":        values("Musterstadt"),
				"invoice[country]":     values("FRA"),
			}))
		assert.Equal(t, resp.Code, 200)
		bodyCheckContains(t, resp.Body.String(), "Danke für deine Anfrage")
		waitForEmail(t) // admin email
		waitForEmail(t) // user email

		// Accept
		resp = serve(newRequestWithParams("PUT", "/verwaltung/buchung/akzeptieren?"+query, nil))
		assert.Equal(t, resp.Code, 302)
		waitForEmail(t) // user email
	})

	t.Run("booking 6 admin-confirmed, user-confirmed", func(t *testing.T) {
		query := "id=6"
		resp = serve(newRequestWithParams("POST",
			"/neue/maison?_=_"+
				"&start="+now.AddDate(0, 0, 46).Format(dateLayout)+
				"&end="+now.AddDate(0, 0, 50).Format(dateLayout)+
				"&people_over[7]=3",
			url.Values{
				"name":  values("Max Mustermann"),
				"email": values("max@example.com"),
				"phone": values("+4980417969988"),
				// "comment":              values(""),
				"invoice[kind]":     values("privat"),
				"invoice[name]":     values("Maxi"),
				"invoice[address1]": values("Musterstrasse"),
				// "invoice[address2]":    values("c/o Max"),
				"invoice[postal_code]": values("12345"),
				"invoice[city]":        values("Musterstadt"),
				"invoice[country]":     values("DEU"),
			}))
		assert.Equal(t, resp.Code, 200)
		bodyCheckContains(t, resp.Body.String(), "Danke für deine Anfrage")
		waitForEmail(t) // admin email
		waitForEmail(t) // user email

		// Accept
		resp = serve(newRequestWithParams("PUT", "/verwaltung/buchung/akzeptieren?"+query, nil))
		assert.Equal(t, resp.Code, 302)
		msg := waitForEmail(t) // user email

		// user-confirmation
		confirmationURL := findURL(t, msg.Reader)
		t.Log(confirmationURL)
		assert.Check(t, strings.Contains(confirmationURL, query))
		resp = serve(httptest.NewRequest("GET", confirmationURL, nil))
		assert.Equal(t, resp.Code, 200)
		resp = serve(newRequestWithParams("POST", confirmationURL, url.Values{
			"confirm": values("1"),
		}))
		assert.Equal(t, resp.Code, 302)
		waitForEmail(t) // user email
	})

	t.Run("booking 7 admin extra charges, admin-confirmed", func(t *testing.T) {
		query := "id=7"
		resp = serve(newRequestWithParams("POST",
			"/neue/maison?_=_"+
				"&start="+now.AddDate(0, 0, 51).Format(dateLayout)+
				"&end="+now.AddDate(0, 0, 55).Format(dateLayout)+
				"&people_over[7]=3",
			url.Values{
				"name":  values("Lieschen Müller"),
				"email": values("Lieschen@examplE.com"),
				"phone": values("012345678900"),
				// "comment":              values(""),
				"invoice[kind]":     values("privat"),
				"invoice[name]":     values("Lieschen Müller"),
				"invoice[address1]": values("Musterstrasse"),
				// "invoice[address2]":    values("c/o Max"),
				"invoice[postal_code]": values("12345"),
				"invoice[city]":        values("Musterstadt"),
				"invoice[country]":     values("DEU"),
			}))
		assert.Equal(t, resp.Code, 200)
		bodyCheckContains(t, resp.Body.String(), "Danke für deine Anfrage")
		waitForEmail(t) // admin email
		waitForEmail(t) // user email

		// find charges ids
		resp = serve(httptest.NewRequest("GET", "/verwaltung/buchung/bearbeiten?"+query, nil))
		assert.Equal(t, resp.Code, 200)
		chargeIDs := findMatches(resp.Body.String(), `name="charge\[([^"]+)\]"`)
		assert.Check(t, len(chargeIDs) > 3, chargeIDs)

		// add extra charges
		resp = serve(newRequestWithParams("PUT",
			"/verwaltung/buchung/optionen?"+query,
			url.Values{
				"charge[" + chargeIDs[0] + "]": values("10"),
				"charge[" + chargeIDs[1] + "]": values(""),
				"charge[" + chargeIDs[2] + "]": values("-42"),
			}))
		assert.Equal(t, resp.Code, 302)

		// update extra charges
		resp = serve(newRequestWithParams("PUT",
			"/verwaltung/buchung/optionen?"+query,
			url.Values{
				"charge[" + chargeIDs[0] + "]": values("100"),
				"charge[" + chargeIDs[1] + "]": values("11"),
				"charge[" + chargeIDs[2] + "]": values(""),
			}))
		assert.Equal(t, resp.Code, 302)

		// invoice update page
		resp = serve(httptest.NewRequest("GET", "/verwaltung/buchung/abrechnungsdaten?"+query, nil))
		assert.Equal(t, resp.Code, 200)

		// update extra charges
		resp = serve(newRequestWithParams("PUT",
			"/verwaltung/buchung/abrechnungsdaten?"+query,
			url.Values{
				"name":  values("Lieschen Müller"),
				"email": values("LieschenM@example.com"),
				"phone": values("012345678900"),

				"invoice[kind]":        values("dpsg"),
				"invoice[name]":        values("Max Müller"),
				"invoice[address1]":    values("Musterstr."),
				"invoice[address2]":    values("c/o Max"),
				"invoice[postal_code]": values("54321"),
				"invoice[city]":        values("Larmor"),
				"invoice[country]":     values("FRA"),
			}))
		assert.Equal(t, resp.Code, 302)
		resp = serve(httptest.NewRequest("GET", "/verwaltung/buchung/bearbeiten?"+query, nil))
		assert.Equal(t, resp.Code, 200)
		bodyCheckContains(t, resp.Body.String(), "Musterstr.")
		bodyCheckContains(t, resp.Body.String(), "LieschenM@example.com")

		// Accept
		resp = serve(newRequestWithParams("PUT", "/verwaltung/buchung/akzeptieren?"+query, nil))
		assert.Equal(t, resp.Code, 302)
		waitForEmail(t) // user email

		// debugEmailBody(t, msg)
	})

	t.Run("visitor booking creation steps", func(t *testing.T) {
		// house_id
		resp = serve(httptest.NewRequest("GET", "/neue/maison", nil))
		assert.Equal(t, resp.Code, 200)

		// start
		resp = serve(httptest.NewRequest("GET",
			"/neue/maison?start="+
				now.AddDate(0, 0, 1).Format(dateLayout),
			nil))
		assert.Equal(t, resp.Code, 200)

		// end
		resp = serve(httptest.NewRequest("GET",
			"/neue/maison"+
				"?start="+now.AddDate(0, 0, 1).Format(dateLayout)+
				"&end="+now.AddDate(0, 0, 3).Format(dateLayout),
			nil))
		assert.Equal(t, resp.Code, 200)

		// ages
		resp = serve(httptest.NewRequest("GET",
			"/neue/maison"+
				"?start="+now.AddDate(0, 0, 1).Format(dateLayout)+
				"&end="+now.AddDate(0, 0, 3).Format(dateLayout)+
				"&people_over[7]=3",
			nil))
		assert.Equal(t, resp.Code, 200)
		bodyCheckContains(t, resp.Body.String(), "Preis für 2&nbsp;Nächte")

		resp = serve(newRequestWithParams("POST",
			"/neue/maison"+
				"?start="+now.AddDate(0, 0, 68).Format(dateLayout)+ // without restrictions starting at day 68
				"&end="+now.AddDate(0, 0, 70).Format(dateLayout)+
				"&people_over[7]=3",
			url.Values{
				"name":  values("Lieschen Müller"),
				"email": values("Lieschen@examplE.com"),
				"phone": values("012345678900"),
				// "comment":              values(""),
				"invoice[kind]":     values("privat"),
				"invoice[name]":     values("Lieschen Müller"),
				"invoice[address1]": values("Musterstrasse"),
				// "invoice[address2]":    values("c/o Max"),
				"invoice[postal_code]": values("12345"),
				"invoice[city]":        values("Musterstadt"),
				"invoice[country]":     values("DEU"),
			}))
		assert.Equal(t, resp.Code, 200)
		waitForEmail(t) // admin email
		waitForEmail(t) // user email
	})

	if os.Getenv("TESTS_ON_ACTUAL_DB") != "" {
		t.Run("many overnight bookings", func(t *testing.T) {
			previousDay := 70
			for previousDay < 300 {
				duration := seedBooking(t, previousDay, serve, now, true)
				if duration > 0 {
					previousDay += rand.Intn(5) + duration //nolint: gosec
				}
			}
		})
		t.Run("some overnight bookings", func(t *testing.T) {
			seedBooking(t, 0, serve, time.Now(), false)
			seedBooking(t, 1, serve, time.Now(), false)
			seedBooking(t, 2, serve, time.Now(), false)
			seedBooking(t, 4, serve, time.Now(), false)
			seedBooking(t, 5, serve, time.Now(), false)
			seedBooking(t, 6, serve, time.Now(), false)
		})
	} else {
		// one seed to ensure tests still pass
		seedBooking(t, 70, serve, now, true)
		seedBooking(t, 0, serve, time.Now(), false)
	}

	t.Run("dashboard", func(t *testing.T) {
		resp = serve(httptest.NewRequest("GET", "/verwaltung", nil))
		assert.Equal(t, resp.Code, 200)
	})

	t.Run("booking list", func(t *testing.T) {
		resp = serve(httptest.NewRequest("GET", "/verwaltung/buchung/liste", nil))
		assert.Equal(t, resp.Code, 200)
	})
}

func debugEmailBody(t *testing.T, msg entity) { //nolint:unused,deadcode
	t.Helper()
	part, err := msg.NextPart()
	assert.NilError(t, err)
	body, err := io.ReadAll(part.Body)
	assert.NilError(t, err)
	t.Log("===== debug Email: " + msg.subject + "\n" + string(body))
	t.Error("===== debug Email =====")
}

func findMatches(body, pattern string) []string {
	r := regexp.MustCompile(pattern)
	matches := r.FindAllStringSubmatch(body, -1)
	ids := make([]string, 0, len(matches))
	for _, m := range matches {
		ids = append(ids, m[1])
	}
	return ids
}

func assertRedirect(t *testing.T, resp *httptest.ResponseRecorder, to string) {
	t.Helper()
	if resp.Code != 302 {
		t.Log(resp.Body.String())
		t.Errorf("expected HTTP status 302, got: %d", resp.Code)
		t.FailNow()
	}

	if to != "" {
		loc, err := resp.Result().Location()
		assert.NilError(t, err)
		assert.Equal(t, loc.String(), to, "wrong redirection")
	}
}

func values(s ...string) []string {
	return s
}

func newRequestWithParams(method, target string, values url.Values) *http.Request {
	req := httptest.NewRequest(method, target, strings.NewReader(values.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	return req
}
