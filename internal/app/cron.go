// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"context"
	"fmt"
	"io/fs"
	"log"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal/email"
	"code.pfad.fr/buchungssystem/internal/view"
)

type cronTasks struct {
	DB    *database.Queries
	Queue email.Queue

	BookingCancelled view.ExecuteEmailTemplate
}

func NewCronTasks(db *database.Queries, queue email.Queue, assetsFS fs.FS) map[string]func() error {
	ct := cronTasks{
		DB:    db,
		Queue: queue,

		BookingCancelled: view.NewEmailTemplate(assetsFS, "emails/5_system_cancelled/user.gotmpl"),
	}
	return map[string]func() error{
		"CheckExpiredBookings":          ct.CheckExpiredBookings,
		"CancelledBookingAnonymisation": ct.CancelledBookingAnonymisation,
		"PastBookingAnonymisation":      ct.PastBookingAnonymisation,
	}
}

func RunPeriodically(ctx context.Context, delay time.Duration, tasks map[string]func() error) <-chan struct{} {
	done := make(chan struct{})

	go func() {
		defer close(done)

		// initial arbitrary delay
		select {
		case <-ctx.Done():
			return
		case <-time.After(5 * time.Second):
		}

		// run forever
		for ctx.Err() == nil {
			for name, task := range tasks {
				fmt.Println("[cron]", name)
				err := task()
				if err != nil {
					log.Println("TODO ERROR handling")
					log.Println(name, err)
					log.Printf("%#v\n", err)
				}
			}

			select {
			case <-ctx.Done():
			case <-time.After(delay):
			}
		}
	}()
	return done
}
