// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"strconv"
	"strings"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"golang.org/x/text/encoding/unicode"
	"golang.org/x/text/transform"
)

type bookingListQuery struct {
	HouseID string
	House   database.SettingViewableHouseIndexRow
	// Start   time.Time
	// End     time.Time
	Date                  string
	IncludeUnconfirmed    bool
	IncludeAdminConfirmed bool
	IncludeUserConfirmed  bool
	IncludeCancelled      bool
	IncludePast           bool
	PriceID               string

	AdminChargesCount int32
}

func newBookingListQuery(houses []database.SettingViewableHouseIndexRow, formValue func(string) string, include []string, searchTerm string) (bookingListQuery, error) {
	var query bookingListQuery

	if len(houses) == 0 {
		return query, internal.HTTPError{
			Code:    http.StatusForbidden,
			Message: "Fehlende Berechtigungen",
		}
	}
	if houseID := formValue("house_id"); houseID != "" {
		for _, h := range houses {
			if h.ID == houseID {
				query.HouseID = h.ID
				query.House = h
				break
			}
		}
	}
	if query.HouseID == "" {
		query.HouseID = houses[0].ID
		query.House = houses[0]
	}

	if len(include) == 0 {
		include = []string{"unconfirmed", "admin_confirmed", "user_confirmed"}
		if searchTerm != "" {
			include = append(include, "cancelled")
		}
		if formValue("with_past") == "1" {
			include = append(include, "past")
		}
	}
	for _, inc := range include {
		switch inc {
		case "unconfirmed":
			query.IncludeUnconfirmed = true
		case "admin_confirmed":
			query.IncludeAdminConfirmed = true
		case "user_confirmed":
			query.IncludeUserConfirmed = true
		case "cancelled":
			query.IncludeCancelled = true
		case "past":
			query.IncludePast = true
		}
	}

	query.PriceID = formValue("price_id")
	query.Date = formValue("date")

	return query, nil
}

func (a app) BookingList(rw http.ResponseWriter, r *http.Request) error {
	self, err := a.CurrentUser(r)
	if err != nil {
		return err
	}

	// formValue gives priority to the {s}_button field name
	formValue := func(s string) string {
		if v := r.FormValue(s + "_button"); v != "" {
			return v
		}
		return r.FormValue(s)
	}

	searchTerm := strings.TrimSpace(formValue("q"))
	if searchTerm != "" {
		if _, err = a.findBookingByID(r.Context(), searchTerm); err == nil {
			http.Redirect(rw, r, "/verwaltung/buchung/bearbeiten?id="+searchTerm, http.StatusFound)
			return nil
		}
	}

	houses, err := a.DB.SettingViewableHouseIndex(r.Context(), self.ID)
	if err != nil {
		return err
	}

	type viewBooking struct {
		database.Booking
		Status  bookingStatus
		Billing BillingInfo
	}
	type adminCharge struct {
		Name    string
		Checked bool
	}
	renderData := struct {
		Nav          internal.NavbarData
		Query        bookingListQuery
		Houses       []database.SettingViewableHouseIndexRow
		Bookings     []viewBooking
		Prices       []database.Price
		AdminCharges []adminCharge
		Pagination   pagination
	}{
		Nav: internal.NavbarData{
			User:             self,
			SelectedDropdown: "buchung",
			SelectedURI:      "buchung/liste",
			SearchTerm:       searchTerm,
		},
		Houses: houses,
	}

	if renderData.Query, err = newBookingListQuery(houses, formValue, r.URL.Query()["include"], searchTerm); err != nil {
		return err
	}

	adminCharges, err := a.DB.BookingAdminChargesIndex(r.Context(), renderData.Query.HouseID)
	if err != nil {
		return err
	}
	checkedCharges := make(map[string]bool, len(r.Form["admin_charge[]"]))
	for _, n := range r.Form["admin_charge[]"] {
		checkedCharges[n] = true
	}

	alreadyAppended := make(map[string]bool, len(r.Form["admin_charge[]"]))

	adminChargesIds := ""
	for _, a := range adminCharges {
		checked := checkedCharges[a.Name]
		if checked {
			adminChargesIds += a.ID + "@"
		}

		if !alreadyAppended[a.Name] {
			renderData.AdminCharges = append(renderData.AdminCharges, adminCharge{
				Name:    a.Name,
				Checked: checked,
			})
			alreadyAppended[a.Name] = true
			if checked {
				renderData.Query.AdminChargesCount++
			}
		}
	}

	var endAt time.Time
	startAt := timeNow().AddDate(100, 0, 0) // in the far future
	if renderData.Query.Date != "" {
		date, err := time.Parse(dateLayout, renderData.Query.Date)
		if err != nil {
			return err
		}
		startAt = date.AddDate(0, 0, 1)
		endAt = date
	} else if !renderData.Query.IncludePast {
		endAt = timeNow()
	}

	exportCSV := r.FormValue("csv") == "1"

	if exportCSV {
		renderData.Pagination.PerPage = math.MaxInt32
	} else if renderData.Pagination, err = newPagination(20, formValue); err != nil {
		return err
	}

	var bookingRows []database.BookingIndexRow
	for {
		bookingRows, err = a.DB.BookingIndex(r.Context(), database.BookingIndexParams{
			HouseID:               renderData.Query.HouseID,
			Offset:                renderData.Pagination.Offset(),
			Limit:                 renderData.Pagination.PerPage,
			Search:                searchTerm,
			IncludeUnconfirmed:    renderData.Query.IncludeUnconfirmed,
			IncludeAdminConfirmed: renderData.Query.IncludeAdminConfirmed,
			IncludeUserConfirmed:  renderData.Query.IncludeUserConfirmed,
			IncludeCancelled:      renderData.Query.IncludeCancelled,
			EndAt:                 endAt,
			StartAt:               startAt,
			PriceID:               renderData.Query.PriceID,
			AdminCharges:          adminChargesIds,
			AdminChargesCount:     renderData.Query.AdminChargesCount,
		})
		if err != nil {
			return err
		}
		if len(bookingRows) > 0 || renderData.Pagination.Page == 0 {
			break
		}
		renderData.Pagination.Page--
	}

	if exportCSV {
		rw.Header().Set("content-disposition", fmt.Sprintf("attachment; filename=%q",
			"buchungen_"+renderData.Query.HouseID+".csv",
		))
		return bookingRows2CSV(rw, bookingRows, renderData.Query.House.DaysForUserConfirmation)
	}

	renderData.Prices, err = a.DB.SettingPriceIndex(r.Context())
	if err != nil {
		return err
	}
	renderData.Prices = append([]database.Price{{ID: "", Name: "Alle"}}, renderData.Prices...)

	for _, row := range bookingRows {
		renderData.Pagination.TotalCount = int32(row.TotalCount)
		vb := viewBooking{
			Booking: bookingRow2Booking(row),
		}
		vb.Status = newBookingStatus(vb.Booking, renderData.Query.House.DaysForUserConfirmation)
		_ = json.Unmarshal(vb.Booking.Invoice, &vb.Billing)
		renderData.Bookings = append(renderData.Bookings, vb)
	}

	return a.BookingListView.Render(rw, r, renderData)
}

func bookingRows2CSV(rw http.ResponseWriter, rows []database.BookingIndexRow, daysForUserConfirmation int32) error {
	rw.Header().Set("content-type", "text/csv; charset=utf-16le")

	t := transform.NewWriter(rw, unicode.UTF16(unicode.LittleEndian, unicode.UseBOM).NewEncoder())
	cw := csv.NewWriter(t)
	cw.Comma = '\t'

	cw.Write([]string{
		"Buchung ID",
		"Kontakt",
		"E-Mail",
		"Handy",
		"Festnetznummer",
		"Von",
		"Bis",
		"Bemerkung",
		"Status",
		"Status-Datum",
		// ...
	})
	csvTimeFormat := func(t time.Time) string {
		if t.IsZero() {
			return ""
		}
		return t.String()
	}
	for _, row := range rows {
		booking := bookingRow2Booking(row)
		status := newBookingStatus(booking, daysForUserConfirmation)
		cw.Write([]string{
			strconv.Itoa(int(row.ID)),
			row.Contact,
			row.Email,
			"\t" + row.Phone,    // prevent being intepreted as a number
			"\t" + row.Landline, // prevent being intepreted as a number
			csvTimeFormat(row.StartAt),
			csvTimeFormat(row.EndAt),
			row.Comment,
			status.Text,
			csvTimeFormat(status.Time),
		})
	}

	cw.Flush()
	return cw.Error()
}

func bookingRow2Booking(row database.BookingIndexRow) database.Booking {
	return database.Booking{
		ID:               row.ID,
		HouseID:          row.HouseID,
		StartAt:          row.StartAt,
		EndAt:            row.EndAt,
		PeopleOver:       row.PeopleOver,
		Contact:          row.Contact,
		Email:            row.Email,
		Phone:            row.Phone,
		Comment:          row.Comment,
		Invoice:          row.Invoice,
		CreatedAt:        row.CreatedAt,
		AdminConfirmedAt: row.AdminConfirmedAt,
		UserConfirmedAt:  row.UserConfirmedAt,
		CancelledAt:      row.CancelledAt,
		HashedToken:      row.HashedToken,
	}
}
