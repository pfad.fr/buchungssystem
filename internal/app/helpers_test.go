// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"testing"
	"time"

	"gotest.tools/v3/assert"
)

func TestCountNights(t *testing.T) {
	testCases := []struct {
		desc       string
		start, end time.Time
		count      int
	}{
		{
			desc:  "exact same day",
			start: time.Date(2022, 1, 1, 0, 0, 0, 0, time.Local),
			end:   time.Date(2022, 1, 1, 0, 0, 0, 0, time.Local),
			count: 0,
		},
		{
			desc:  "same day, one minute later",
			start: time.Date(2022, 1, 1, 0, 0, 0, 0, time.Local),
			end:   time.Date(2022, 1, 1, 0, 1, 0, 0, time.Local),
			count: 0,
		},
		{
			desc:  "next day, same hour",
			start: time.Date(2022, 1, 1, 0, 0, 0, 0, time.Local),
			end:   time.Date(2022, 1, 2, 0, 0, 0, 0, time.Local),
			count: 1,
		},
		{
			desc:  "next day, one minute later",
			start: time.Date(2022, 1, 1, 23, 59, 59, 0, time.Local),
			end:   time.Date(2022, 1, 2, 0, 0, 0, 0, time.Local),
			count: 1,
		},
		{
			desc:  "several days",
			start: time.Date(2022, 1, 1, 17, 0, 0, 0, time.Local),
			end:   time.Date(2022, 1, 10, 15, 0, 0, 0, time.Local),
			count: 9,
		},
	}
	for _, c := range testCases {
		t.Run(c.desc, func(t *testing.T) {
			t.Log("start", c.start)
			t.Log("end  ", c.end)
			got := countNights(c.start, c.end)
			assert.Equal(t, got, c.count)
		})
	}
}
