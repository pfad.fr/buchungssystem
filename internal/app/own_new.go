// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package app

import (
	"context"
	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/buchungssystem/internal/validator"
	"code.pfad.fr/buchungssystem/internal/view"
	"github.com/emersion/go-message/mail"
	"github.com/go-chi/chi/v5"
)

func (h ownBookingHandler) New(rw http.ResponseWriter, r *http.Request) error {
	self, _ := h.CurrentUser(r) // may be nil

	house, err := h.DB.SettingHouseFind(r.Context(), chi.URLParam(r, "house_id"))
	if err != nil {
		return err
	}
	if house.PublicBookingType == "overday" {
		return h.newOverday(rw, r, self, house)
	}

	renderData := struct {
		User        *database.User
		House       database.House
		IsUnlimited bool

		Start   time.Time
		End     time.Time
		Days    []database.BookableDay
		Ages    map[int32]int
		Offer   Offer
		Country string

		StartMonths []month
		EndMonths   []month
		TimeLayout  string
		ChargeAges  []int32

		ExtraRequests []database.ExtraRequest

		BillingKinds     internal.Options
		BillingCountries internal.Options
		PhoneRegex       string
	}{
		User:        self,
		House:       house,
		IsUnlimited: h.HousePolicy(r).CanBookInAdvance(house.ID) == nil,

		TimeLayout: dateLayout,
		Country:    "DEU",

		BillingKinds:     billingKinds,
		BillingCountries: billingCountries,
		PhoneRegex:       phoneRegex.String(),
	}

	// 2. Select start date
	now := timeNow()

	type viewDay struct {
		IsBookable         bool
		IsSelected, IsLast bool
	}
	maStart := monthsAccumulator{
		today: time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.Local),
	}
	var maEnd monthsAccumulator

	startValue := r.FormValue("start")
	endValue := r.FormValue("end")
	var hasStarted bool
	previousWasBookable := false

	err = forBookableDays(r.Context(), h.DB, house, now, renderData.IsUnlimited, func(d database.BookableDay, date time.Time, bookingsOfTheDay []database.Booking) {
		isBookable := d.RestrictionReason == ""
		if isBookable && len(bookingsOfTheDay) > 0 {
			b := bookingsOfTheDay[len(bookingsOfTheDay)-1]

			// for a booking to start on this day
			// [startMinute-midnight] must be free
			// [midnight-endMinute] of the next day must be free as well
			curStart := adjustedTime(date, house.StartMinute)
			curEnd := adjustedTime(date.AddDate(0, 0, 1), house.EndMinute)
			if b.EndAt.After(curStart) && b.StartAt.Before(curEnd) {
				isBookable = false
			}
		}
		isLast := d.Date == endValue
		if isBookable {
			if d.Date == startValue {
				renderData.Start = date
				maEnd.today = date
				hasStarted = true
				maEnd.Append(date, viewDay{
					IsBookable: true,
					IsSelected: true,
				})
				renderData.Days = append(renderData.Days, d)
			} else if !maEnd.today.IsZero() && hasStarted {
				if maEnd.AppendWithoutHole(date, viewDay{
					IsBookable: true,
					IsSelected: d.Date <= endValue,
					IsLast:     isLast,
				}) {
					if isLast {
						renderData.End = date
					} else if d.Date < endValue {
						renderData.Days = append(renderData.Days, d)
					}
				} else {
					hasStarted = false
				}
			}
		} else if hasStarted {
			// last possible end day
			if maEnd.AppendWithoutHole(date, viewDay{
				IsBookable: true,
				IsSelected: d.Date <= endValue,
				IsLast:     isLast,
			}) {
				if isLast {
					renderData.End = date
				}
			}
			hasStarted = false
		}
		maStart.Append(date, viewDay{
			IsBookable: isBookable,
			IsSelected: d.Date == startValue,
			IsLast:     previousWasBookable,
		})
		previousWasBookable = isBookable
	})
	if err != nil {
		return err
	}
	if renderData.End.IsZero() {
		renderData.Days = nil
	}
	renderData.StartMonths = maStart.Months()
	renderData.EndMonths = maEnd.Months()

	// 3. compute charges
	if !renderData.Start.IsZero() && !renderData.End.IsZero() {
		// charges
		charges, err := h.DB.BookingChargesIndex(r.Context(), database.BookingChargesIndexParams{
			HouseID: house.ID,
			Start:   renderData.Start.Format(dateLayout),
			EndDate: renderData.End.Format(dateLayout),
		})
		if err != nil {
			return err
		}
		ages := chargeAges(charges)
		renderData.ChargeAges = ages

		var totalPeople int
		renderData.Ages, totalPeople, err = validateAges(ages, r.FormValue, int(renderData.House.MaxPerson))
		if err != nil {
			return err
		}

		if totalPeople > 0 {
			renderData.Offer = newOffer(renderData.Days, charges, totalPeople, renderData.Ages, make(map[string]int32))

			renderData.ExtraRequests, err = h.DB.ExtraRequestsIndex(r.Context(), house.ID)
			if err != nil {
				return err
			}

			if r.Method == http.MethodPost {
				booking, err := createBooking(h.Handler, h.notification, r, house, adjustedTime(renderData.Start, house.StartMinute), adjustedTime(renderData.End, house.EndMinute), renderData.Ages)
				if err != nil {
					return err
				}
				var userID sql.NullString
				if self != nil {
					userID.Valid = true
					userID.String = self.ID
				}
				_ = h.DB.LogEventCreate(r.Context(), database.LogEventCreateParams{
					BookingID: booking.ID,
					UserID:    userID,
					Message:   "· Buchung erstellt" + "\nPreis: " + view.FormattedCentimes(renderData.Offer.TotalAmount, ",") + "€",
				})

				return h.renderBooking(rw, r, booking, house)
			}
		}
	}

	if r.Method != http.MethodGet {
		rw.WriteHeader(http.StatusUnprocessableEntity)
	}

	return h.BookingNew.Render(rw, r, renderData)
}

func forBookableDays(
	ctx context.Context,
	db *database.Queries,
	house database.House,
	after time.Time,
	isUnlimited bool,
	callback func(day database.BookableDay, date time.Time, bookingsOfTheDay []database.Booking),
) (err error) {
	var days []database.BookableDay
	if isUnlimited {
		days, err = db.BookableDayIndexUnlimited(ctx, database.BookableDayIndexUnlimitedParams{
			HouseID: house.ID,
			Start:   after.Format(dateLayout),
		})
	} else {
		endDate := time.Date(after.Year(), after.Month()+time.Month(house.MonthsForPublicBooking), 1, 0, 0, 0, 0, after.Location())
		days, err = db.BookableDayIndex(ctx, database.BookableDayIndexParams{
			HouseID: house.ID,
			Start:   after.Format(dateLayout),
			EndDate: endDate.Format(dateLayout),
		})
	}
	if err != nil {
		return err
	}
	activeBookings, err := db.BookingEndingAfter(ctx, database.BookingEndingAfterParams{
		HouseID: house.ID,
		EndAt:   after,
	})
	if err != nil {
		return err
	}

	remainingBookings := activeBookings
	for _, d := range days {
		date, err := time.Parse(dateLayout, d.Date)
		if err != nil {
			return err
		}
		var dayBookings []database.Booking
		for len(remainingBookings) > 0 {
			b := remainingBookings[0]
			today := date
			if house.PublicBookingType == "overnight" {
				today = adjustedTime(today, house.StartMinute)
			}
			if b.EndAt.Before(today) {
				// booking ended yesterday or earlier
				remainingBookings = remainingBookings[1:]
				continue
			}
			tomorrow := date.AddDate(0, 0, 1)
			if house.PublicBookingType == "overnight" {
				tomorrow = adjustedTime(tomorrow, house.EndMinute)
			}
			if b.StartAt.After(tomorrow) {
				// booking starts tomorrow or later
				break
			}
			dayBookings = append(dayBookings, b)

			if b.EndAt.After(tomorrow) {
				// booking ends tomorrow: no need to check later bookings
				break
			}
			remainingBookings = remainingBookings[1:]
		}

		callback(d, date, dayBookings)
	}

	return nil
}

// chargeAges returns the sorted ages relevant for the charges.
func chargeAges(charges []database.Charge) []int32 {
	ages := make([]int32, 0)
	for _, c := range charges {
		if c.ExemptedUnderAge == 0 {
			continue
		}
		var i int
		for i < len(ages) && ages[i] < c.ExemptedUnderAge {
			i++
		}
		if i == len(ages) {
			ages = append(ages, c.ExemptedUnderAge)
		} else if ages[i] != c.ExemptedUnderAge {
			ages = append(ages[:i], append([]int32{c.ExemptedUnderAge}, ages[i:]...)...)
		}
	}
	if len(ages) == 0 {
		return []int32{0}
	}
	return ages
}

func adjustedTime(t time.Time, hourOfTheDayInMinutes int32) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), int(hourOfTheDayInMinutes/60), int(hourOfTheDayInMinutes%60), 0, 0, time.Local)
}

func plainBillingAddress(b BillingInfo) string {
	var s strings.Builder

	s.WriteString(billingKinds.Name(b.Kind) + "\n")
	s.WriteString(b.Name + "\n")
	s.WriteString(b.Address1 + "\n")
	if b.Address2 != "" {
		s.WriteString(b.Address2 + "\n")
	}
	s.WriteString(b.PostalCode + " " + b.City + "\n")
	if b.Country != "DEU" {
		s.WriteString(billingCountries.Name(b.Country) + "\n")
	}
	return s.String()
}

func createBooking(h internal.Handler, notification notification, r *http.Request, house database.House, startAt, endAt time.Time, ages map[int32]int) (database.Booking, error) {
	peopleOver, err := json.Marshal(ages)
	if err != nil {
		return database.Booking{}, err
	}
	billing, err := parseBillingInfo(func(s string) string {
		return r.FormValue("invoice[" + s + "]")
	})
	if err != nil {
		return database.Booking{}, err
	}
	billingJSON, err := json.Marshal(billing)
	if err != nil {
		return database.Booking{}, err
	}

	v := validator.New(r.FormValue)

	var comments []string

	if c := v.TrimmedString("comment"); len(c) > 0 {
		comments = append(comments, c)
	}
	if extra := r.Form["extrarequest[]"]; len(extra) > 0 {
		if len(comments) > 0 {
			comments = append(comments, "") // extra newline
		}
		comments = append(comments, "[Sonderwünsche]")
		for _, c := range extra {
			c = strings.TrimSpace(c)
			if len(c) == 0 {
				continue
			}
			comments = append(comments, "- "+c)
		}
	}

	params := database.BookingCreateParams{
		HouseID:    house.ID,
		StartAt:    startAt,
		EndAt:      endAt,
		PeopleOver: peopleOver,
		Contact:    v.TrimmedString("name"),
		Email:      v.Email("email"),
		Phone:      v.TrimmedString("phone", phoneRegex.MatchString),
		Landline: v.TrimmedString("landline", func(s string) bool {
			if s == "" {
				return true // landline is optional
			}
			return phoneRegex.MatchString(s)
		}),
		Comment: strings.Join(comments, "\n"),
		Invoice: billingJSON,
	}
	if err = v.HTTPErr(); err != nil {
		return database.Booking{}, err
	}
	booking, err := h.DB.BookingCreate(r.Context(), params)
	if err != nil {
		return database.Booking{}, err
	}

	adminURL := "/verwaltung/buchung/bearbeiten?id=" + strconv.Itoa(int(booking.ID))

	subject, body, err := notification.adminEmail.BookingCreated(struct {
		Booking      database.Booking
		House        database.House
		BillingLines string
		AdminURL     string
	}{
		Booking:      booking,
		House:        house,
		BillingLines: plainBillingAddress(billing),
		AdminURL:     h.BaseURL + adminURL,
	})
	if err != nil {
		return database.Booking{}, err
	}

	adminMsg := database.Email{
		BookingID: booking.ID,
		HeaderTo:  house.ReplyTo,
		HeaderFrom: (&mail.Address{
			Name:    params.Contact,
			Address: params.Email,
		}).String(),
		HeaderSubject: subject,
		TextPlain:     string(body),
	}
	if err = h.Queue.Push(r.Context(), adminMsg); err != nil {
		return database.Booking{}, err
	}

	subject, body, err = notification.userEmail.BookingCreated(struct {
		Booking      database.Booking
		House        database.House
		BillingLines string
	}{
		Booking:      booking,
		House:        house,
		BillingLines: plainBillingAddress(billing),
	})
	if err != nil {
		return database.Booking{}, err
	}

	userMsg := database.Email{
		BookingID: booking.ID,
		HeaderTo: (&mail.Address{
			Name:    params.Contact,
			Address: params.Email,
		}).String(),
		HeaderFrom:    house.ReplyTo,
		HeaderSubject: subject,
		TextPlain:     string(body),
	}
	if err := h.Queue.Push(r.Context(), userMsg); err != nil {
		return database.Booking{}, err
	}

	return booking, nil
}
