// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package validator

import (
	"context"
	"errors"
	"fmt"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"

	"code.pfad.fr/buchungssystem/internal"
	"github.com/emersion/go-message/mail"
)

type Validator struct {
	valueOf func(string) string
	errors  multiErrors
}

func New(valueOf func(string) string) *Validator {
	return &Validator{
		valueOf: valueOf,
	}
}

type multiErrors []error

func (me multiErrors) Error() string {
	s := ""
	for _, e := range me {
		s += e.Error() + "\n"
	}
	return s
}

func (me multiErrors) ErrOrNil() error {
	switch len(me) {
	case 0:
		return nil
	case 1:
		return me[0]
	}
	return me
}

func (v *Validator) TrimmedString(name string, isValid ...func(string) bool) string {
	s := strings.TrimSpace(v.valueOf(name))

	for _, check := range isValid {
		if !check(s) {
			v.errors = append(v.errors, fmt.Errorf("invalid %s: %q", name, s))
		}
	}
	return s
}

func (v *Validator) Email(name string, isValid ...func(string) bool) string {
	s := v.valueOf(name)
	a, err := mail.ParseAddress(s)
	if err != nil {
		v.errors = append(v.errors, fmt.Errorf("invalid %s: %q: %w", name, s, err))
		return ""
	}
	s = a.Address

	// check that domain is routable
	localPart, domain, found := strings.Cut(s, "@")
	if !found {
		v.errors = append(v.errors, fmt.Errorf("missing @ for %s: %q: %w", name, s, err))
		return s
	}
	if err := resolveEmailDomain(domain); err != nil {
		v.errors = append(v.errors, fmt.Errorf("invalid domain for %s: %q: %w", name, s, err))
		return s
	}
	// The first part of the address may be case sensitive (RFC 5336)
	// Domain names are case-insensitive (RFC 4343)
	s = localPart + "@" + strings.ToLower(domain)

	for _, check := range isValid {
		if !check(s) {
			v.errors = append(v.errors, fmt.Errorf("invalid %s: %q", name, s))
		}
	}
	return s
}

type resolver interface {
	LookupMX(context.Context, string) ([]*net.MX, error)
	LookupIPAddr(context.Context, string) ([]net.IPAddr, error)
}

var dnsResolver resolver = net.DefaultResolver

func resolveEmailDomain(name string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	mx, err := dnsResolver.LookupMX(ctx, name)
	if len(mx) > 0 {
		return nil
	}

	// A/AAAA fallback
	ips, _ := dnsResolver.LookupIPAddr(ctx, name)
	if len(ips) > 0 {
		return nil
	}

	if err != nil {
		return err
	}
	return errors.New("no mx record")
}

func (v *Validator) ParsedInt(name string) int {
	c := v.TrimmedString(name)
	if c == "" {
		return 0
	}

	i, err := strconv.Atoi(c)
	if err != nil {
		v.errors = append(v.errors, fmt.Errorf("%s: %w", name, err))
	}
	return i
}

func (v *Validator) ParsedCentime(name string) int {
	c := v.TrimmedString(name)
	if c == "" {
		return 0
	}
	c, err := multiplyByHundred(c)
	if err != nil {
		v.errors = append(v.errors, fmt.Errorf("%s: %w", name, err))
		return 0
	}

	i, err := strconv.Atoi(c)
	if err != nil {
		v.errors = append(v.errors, fmt.Errorf("%s: %w", name, err))
	}
	return i
}

func (v *Validator) ParsedTimeAsMinute(name string) int32 {
	c := v.TrimmedString(name)
	var hour, minute int32
	_, err := fmt.Sscanf(c, "%d:%d", &hour, &minute)
	if err != nil {
		v.errors = append(v.errors, fmt.Errorf("%s: %w", name, err))
		return 0
	}
	return 60*hour + minute
}

func multiplyByHundred(v string) (string, error) {
	index := strings.Index(v, ".")
	if index == -1 {
		v += "00"
	} else {
		decimals := v[index+1:]
		v = v[:index]
		switch len(decimals) {
		case 0:
			v += "00"
		case 1:
			v += decimals + "0"
		case 2:
			v += decimals
		default:
			return "", fmt.Errorf("%v.%v has too many decimals", v, decimals)
		}
	}
	return v, nil
}

func (v *Validator) Err() error {
	return v.errors.ErrOrNil()
}
func (v *Validator) HTTPErr() error {
	if err := v.Err(); err != nil {
		return internal.HTTPError{
			Code:    http.StatusUnprocessableEntity,
			Message: err.Error(),
		}
	}
	return nil
}
