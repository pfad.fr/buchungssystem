// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package validator

import (
	"context"
	"net"
)

type fakeResolver struct {
	mxRecords []*net.MX
	mxErr     error
	ipRecords []net.IPAddr
	ipErr     error
}

func (fr fakeResolver) LookupMX(context.Context, string) ([]*net.MX, error) {
	return fr.mxRecords, fr.mxErr
}
func (fr fakeResolver) LookupIPAddr(context.Context, string) ([]net.IPAddr, error) {
	return fr.ipRecords, fr.ipErr
}

func EnableFakeDNSResolver() {
	dnsResolver = fakeResolver{mxRecords: []*net.MX{nil}} // one MX record for all tests
}
