// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package validator

import (
	"net"
	"testing"

	"gotest.tools/v3/assert"
)

func TestEmailValidation(t *testing.T) {
	testCases := []struct {
		input    string
		expected string
		resolver fakeResolver
		err      bool
	}{
		{
			// invalid format
			input:    "max@example_invalid_format@com",
			expected: "",
			err:      true,
		},
		{
			// with some MX record
			input:    "max@example.com",
			expected: "max@example.com",
			resolver: fakeResolver{mxRecords: []*net.MX{nil}},
			err:      false,
		},
		{
			// with some IPAddr record
			input:    "max@www.example.com", // should fallback to A record
			expected: "max@www.example.com",
			resolver: fakeResolver{ipRecords: []net.IPAddr{{}}},
			err:      false,
		},
		{
			// without any record
			input:    "max@non-existing-domain.example.com",
			expected: "max@non-existing-domain.example.com",
			resolver: fakeResolver{},
			err:      true,
		},
	}
	for _, tC := range testCases {
		t.Run(tC.input, func(t *testing.T) {
			dnsResolver = tC.resolver
			v := New(func(s string) string {
				return tC.input
			})
			got := v.Email("email")
			assert.Equal(t, tC.expected, got)
			err := v.Err()
			if tC.err {
				if err == nil {
					t.Error("expected an error, got nil")
				}
			} else {
				assert.NilError(t, err)
			}
		})
	}
	dnsResolver = net.DefaultResolver
}
