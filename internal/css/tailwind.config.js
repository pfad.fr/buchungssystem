// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: CC0-1.0

const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: ["./internal/assets/*.js", "./internal/views/**/*.gohtml"],
  theme: {
    extend: {
      zIndex: {
        1000: 1000,
      },
      maxWidth: ({ theme }) => ({
        96: theme("spacing.96"),
      }),

      colors: {
        navy: {
          50: "#f5f9f9",
          100: "#dff1fa",
          200: "#b8e1f3",
          300: "#85c2e2",
          400: "#4e9dcb",
          500: "#397cb3",
          600: "#2f6199",
          700: "#274977",
          800: "#003056", // DPSG Dachzeil Lilie
          900: "#101d36",
        },
      },
    },
  },
  plugins: [],
};
