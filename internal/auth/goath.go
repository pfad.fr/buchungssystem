// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package auth

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/gopenidclient"
	"code.pfad.fr/gopenidclient/assertion"
	"code.pfad.fr/gopenidclient/coreos"
	"code.pfad.fr/gopenidclient/refresh"
	"github.com/go-chi/chi/v5"
	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"golang.org/x/oauth2"
)

type OIDC struct {
	Issuer       string
	ClientID     string
	ClientSecret string
	Scopes       []string

	Assertion   gopenidclient.Assertion
	Certificate *assertion.Sesquiennial
}

type Goath struct {
	BaseHandler internal.Handler
	URI         string
	Provider    *coreos.OIDC
	exchange    gopenidclient.ExchangeHandler
	refresh     refresh.Handler
	session     sessions.Store
	downgrade   func() bool
}

type securecookieEncrypter struct {
	*securecookie.SecureCookie
}

func (se securecookieEncrypter) Seal(name, value string) string {
	cyphertext, err := se.Encode(name, value)
	if err != nil {
		fmt.Printf("could not seal cookie %q: %v\n", name, err)
		panic(err)
	}
	return cyphertext
}

func (se securecookieEncrypter) Open(name, cyphertext string) (string, error) {
	var cleartext string
	err := se.Decode(name, cyphertext, &cleartext)
	return cleartext, err
}

func (g *Goath) MountRoutes(hashKey, blockKey []byte, oidc OIDC, r *chi.Mux) error {
	url, err := url.Parse(g.BaseHandler.BaseURL + g.URI)
	if err != nil {
		return err
	}
	g.Provider = (&coreos.OIDC{
		Issuer:          oidc.Issuer,
		ClientID:        oidc.ClientID,
		ClientSecret:    oidc.ClientSecret,
		Scopes:          oidc.Scopes,
		ClientAssertion: oidc.Assertion,
	})
	g.downgrade = oidc.Certificate.Downgrade

	switch len(blockKey) {
	default:
		return fmt.Errorf("block key should be 24 or 32 bytes long, got: %d", len(blockKey))
	case 24, 32:
	}

	sessionCookie := securecookieEncrypter{
		SecureCookie: securecookie.New(hashKey, blockKey),
	}
	sessionStore := &sessions.CookieStore{
		Codecs: []securecookie.Codec{sessionCookie.SecureCookie},
		Options: &sessions.Options{
			Path:     "/",
			MaxAge:   60 * 60 * 24, // valid 24 hours
			Secure:   true,
			HttpOnly: true,
			SameSite: http.SameSiteLaxMode,
		},
	}
	sessionStore.MaxAge(sessionStore.Options.MaxAge)
	g.session = sessionStore

	r.Use(g.AttachUserToRequest())
	g.mountCallback(url, sessionCookie, r)
	refreshCookie := securecookieEncrypter{
		SecureCookie: securecookie.New(hashKey, blockKey).MaxAge(60 * 60 * 24 * 10), // valid 10 days
	}
	g.mountRefresh(url, refreshCookie, r)
	r.Delete(g.URI, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		g.refresh.ClearCookies(w, r)
		http.SetCookie(w, &http.Cookie{
			Name:  "__Secure-auth-session",
			Value: "",

			Path: "/",

			MaxAge:   -1,
			Secure:   true,
			HttpOnly: true,
			SameSite: http.SameSiteLaxMode,
		})
		logoutURL, err := g.Provider.EndSessionURL()
		if err != nil {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}
		query := logoutURL.Query()
		query.Set("post_logout_redirect_uri", g.BaseHandler.BaseURL)
		logoutURL.RawQuery = query.Encode()

		http.Redirect(w, r, logoutURL.String(), http.StatusFound)
	}))

	return nil
}

func (g *Goath) mountCallback(baseURL *url.URL, cookieEncrypter securecookieEncrypter, r *chi.Mux) {
	callbackURL := baseURL.JoinPath("callback", "oidc")

	g.Provider.SetRedirectURL(callbackURL.String())
	g.exchange = gopenidclient.ExchangeHandler{
		CookieManager: gopenidclient.CookieManager{
			Prefix:    "__Secure-auth-",
			Encrypter: cookieEncrypter,
		},
	}

	r.Mount(callbackURL.EscapedPath(), g.BaseHandler.ErrorHandler.Wrap(func(w http.ResponseWriter, r *http.Request) error {
		cr, clearCookies, err := g.exchange.HandleCallback(w, r, g.Provider)
		if err != nil {
			if g.checkCertificateError(err) {
				if g.downgrade() {
					http.Redirect(w, r, r.URL.String(), http.StatusSeeOther)
					return nil
				}
				clearCookies()

				u := *r.URL
				if u.Host == "" {
					u.Host = "buchung.dpsgm.de"
					u.Scheme = "https"
				}
				u.Path = "/azure.pem"
				u.RawQuery = ""

				w.WriteHeader(http.StatusInternalServerError)
				_, err = w.Write([]byte(fmt.Sprintf(`<div class="text-center bg-red-100 text-red-700 p-4">
		<h3 class="font-bold">Authentication Certificate has expired</h3>
		An administrator should download the
		<a href="/azure.pem" class="underline">latest certificate</a> and<br />
		upload it to
		<a
		  href="https://portal.azure.com/#view/Microsoft_AAD_RegisteredApps/ApplicationMenuBlade/~/Credentials/appId/%s/isMSAApp~/true"
		  class="underline"
		  >Azure › App registrations › Certificates & secrets</a
		>
		using the button
		<span class="inline-block bg-gray-100 px-1 rounded">
		  Upload certificate
		</span>
	  </div>`, g.Provider.ClientID)))
				return err
			}
			clearCookies()
			return err
		}
		clearCookies()

		err = g.userAuthenticated(w, r, cr, "callback")
		if err != nil {
			return err
		}

		http.Redirect(w, r, cr.IntendedPath, http.StatusSeeOther)
		return nil
	}))
}

func (g *Goath) userAuthenticated(w http.ResponseWriter, r *http.Request, cr gopenidclient.CallbackResult, source string) error {
	var claim struct {
		ID    string `json:"sub"`
		Name  string `json:"name"`
		Email string `json:"email"`
	}
	err := cr.UnmarshalUser(&claim)
	if err != nil {
		return err
	}
	db := g.BaseHandler.DB
	ctx := r.Context()
	user, err := db.AuthLoad(ctx, claim.ID)
	if err != nil {
		if err != sql.ErrNoRows { //nolint:errorlint
			return err
		}
		err = db.AuthInsert(ctx, database.AuthInsertParams{
			ID:    claim.ID,
			Email: claim.Email,
			Name:  claim.Name,
		})
		if err != nil {
			return err
		}
	} else if user.Email != claim.Email {
		// update user email if needed (don't update the other fields, which can be changed locally)
		user.Email = claim.Email
		// we save it now anyway
		err := db.AuthSave(ctx, database.AuthSaveParams{
			ID:    claim.ID,
			Email: claim.Email,
		})
		if err != nil {
			return err
		}
	}

	g.refresh.SetRefreshCookie(w, "oidc", cr.Token.RefreshToken)

	sess, _ := g.session.Get(r, "__Secure-auth-session")
	sess.Values["UserID"] = claim.ID
	sess.Values["source"] = source
	return sess.Save(r, w)
}

func (g *Goath) mountRefresh(baseURL *url.URL, cookieEncrypter securecookieEncrypter, r *chi.Mux) {
	g.refresh = refresh.Handler{
		BaseURL: baseURL,
		CookieManager: gopenidclient.CookieManager{
			Prefix:    "__Secure-auth-",
			Encrypter: cookieEncrypter,
		},

		MaxAge: 60 * 60 * 24 * 10, // 10 days
		LoginChooserHandler: g.BaseHandler.ErrorHandler.Wrap(func(rw http.ResponseWriter, r *http.Request) error {
			return g.exchange.RedirectToAuthCodeURL(rw, r, g.Provider)
		}),
	}

	r.Mount(g.refresh.RootPath(), g.BaseHandler.ErrorHandler.Wrap(func(w http.ResponseWriter, r *http.Request) error {
		cr, err := g.refresh.HandleRefresh(w, r, "oidc", g.Provider)
		if err != nil {
			if g.checkCertificateError(err) {
				if g.downgrade() {
					http.Redirect(w, r, r.URL.String(), http.StatusSeeOther)
					return nil
				}
			}
			fmt.Println("could not refresh:", err)
			return g.exchange.RedirectToAuthCodeURL(w, r, g.Provider)
		}

		err = g.userAuthenticated(w, r, cr, "refresh")
		if err != nil {
			return err
		}

		http.Redirect(w, r, cr.IntendedPath, http.StatusSeeOther)
		return nil
	}))
}

func (g *Goath) checkCertificateError(err error) bool {
	var oErr *oauth2.RetrieveError
	if !errors.As(err, &oErr) {
		return false
	}
	if oErr.Response.StatusCode != http.StatusUnauthorized {
		return false
	}
	var errStruct struct {
		Error            string `json:"error"`
		ErrorDescription string `json:"error_description"`
		ErrorCodes       []int  `json:"error_codes"`
		Timestamp        string `json:"timestamp"`
		TraceID          string `json:"trace_id"`
		CorrelationID    string `json:"correlation_id"`
		ErrorURI         string `json:"error_uri"`
	}
	_ = json.Unmarshal(oErr.Body, &errStruct)
	for _, code := range errStruct.ErrorCodes {
		if code == 700027 {
			// https://login.microsoftonline.com/error?code=700027
			return true
		}
	}
	return false
}

type contextKey string

const ctxKeyUser contextKey = "user"

func (g *Goath) AttachUserToRequest() func(h http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			sess, _ := g.session.Get(r, "__Secure-auth-session")
			id, _ := sess.Values["UserID"].(string)
			if id != "" {
				user, err := g.BaseHandler.DB.AuthLoad(r.Context(), id)
				if err == nil {
					ctx := context.WithValue(r.Context(), ctxKeyUser, &user)
					r = r.WithContext(ctx)
				}
			}
			h.ServeHTTP(w, r)
		})
	}
}

var ErrUserNotFound = errors.New("user not found")

func (g *Goath) CurrentUser(r *http.Request) (*database.User, error) {
	ctxUser := r.Context().Value(ctxKeyUser)
	user, ok := ctxUser.(*database.User)
	if !ok {
		return nil, ErrUserNotFound
	}

	return user, nil
}

func (g *Goath) AuthorizedUsers(waitView http.Handler) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return g.BaseHandler.ErrorHandler.Wrap(func(rw http.ResponseWriter, r *http.Request) error {
			user, err := g.CurrentUser(r)
			if err == ErrUserNotFound {
				g.refresh.HandleUnauthenticatedSession(rw, r)
				return nil
			} else if err != nil {
				return err
			}

			if !user.AuthorizedAt.Valid || user.AuthorizedAt.Time.IsZero() {
				waitView.ServeHTTP(rw, r)
				return nil
			}
			next.ServeHTTP(rw, r)
			return nil
		})
	}
}
