// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

var observe;
if (window.attachEvent) {
  observe = function (element, event, handler) {
    element.attachEvent("on" + event, handler);
  };
} else {
  observe = function (element, event, handler) {
    element.addEventListener(event, handler, false);
  };
}
function autoresize(text) {
  function resize() {
    text.style.height = "auto";
    text.style.height = text.scrollHeight + "px";
  }

  var forID = text.dataset.for;
  var forTarget;
  if (forID) {
    forTarget = document.getElementById(forID);
    if (text.defaultValue === text.value) {
      forTarget.classList.add("invisible");
    }
  }

  /* 0-timeout to get the already changed text */
  var timeout;
  function delayedResize() {
    if (forTarget) {
      if (text.defaultValue === text.value) {
        forTarget.classList.add("invisible");
      } else {
        forTarget.classList.remove("invisible");
      }
    }

    window.clearTimeout(timeout);
    timeout = window.setTimeout(resize, 0);
  }
  observe(text, "change", delayedResize);
  observe(text, "cut", delayedResize);
  observe(text, "paste", delayedResize);
  observe(text, "drop", delayedResize);
  observe(text, "keyup", delayedResize);

  resize();
}

document.addEventListener(
  "DOMContentLoaded",
  function () {
    var texteareas = document.getElementsByClassName("js-autoresize");
    [].forEach.call(texteareas, function (el) {
      autoresize(el);
    });
  },
  false
);
