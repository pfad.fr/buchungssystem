// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package view

import (
	"bytes"
	"embed"
	"errors"
	"fmt"
	"html/template"
	"io"
	"io/fs"
	"net/http"
	"reflect"
	"runtime"
	"runtime/debug"
	"strconv"
	"strings"
	"time"

	"github.com/Masterminds/sprig/v3"
	"github.com/emersion/go-message/mail"
	"github.com/gorilla/csrf"
)

var versionInfo = runtime.Version()

func init() {
	bi, ok := debug.ReadBuildInfo()

	if !ok {
		return
	}

	mapped := make(map[string]string)
	for _, s := range bi.Settings {
		mapped[s.Key] = s.Value
	}
	versionInfo = bi.GoVersion

	if s := mapped["vcs.time"]; s != "" {
		versionInfo += " - " + s[:10]
	}

	if s := mapped["vcs.revision"]; s != "" {
		versionInfo += " - " + s[:6]
		if mapped["vcs.modified"] == "true" {
			versionInfo += "+dirty"
		}
	} else {
		versionInfo += " run"
	}
}

func NewTemplate(fs fs.FS, layout string, files ...string) *Template {
	ext := ".gohtml"
	for i, f := range files {
		files[i] = "views/" + f + ext
	}
	files = append([]string{"views/layout/" + layout + ext}, files...) // prepend layout
	files = append(files, "views/components/*"+ext)
	v := &Template{
		layout:   layout,
		template: templateParser(fs, files...),
	}
	return v
}

// Data is the top level structure that views expect data
// to come in.
type Data struct {
	Yield interface{}
}

type Template struct {
	layout   string
	template func() (*template.Template, error)
}

func (v *Template) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := v.Render(w, r, nil)
	if err != nil {
		fmt.Println("RENDER ERROR", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// Render is used to render the view with the predefined layout.
func (v *Template) Render(w http.ResponseWriter, r *http.Request, data interface{}) error {
	tpl, err := v.template()
	if err != nil {
		return fmt.Errorf("could not parse template: %w", err)
	}

	var vd Data
	switch d := data.(type) {
	case Data:
		vd = d
		// do nothing
	default:
		vd = Data{
			Yield: data,
		}
	}

	csrfField := csrf.TemplateField(r)
	tpl = tpl.Funcs(template.FuncMap{
		"csrfField": func() template.HTML {
			return csrfField
		},
	})

	var buf bytes.Buffer
	if err = tpl.ExecuteTemplate(&buf, v.layout, vd); err != nil {
		return fmt.Errorf("could not execute template: %w", err)
	}
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	_, err = io.Copy(w, &buf)
	return err
}

func GermanDate(t time.Time) string {
	t = t.Local()
	dayOfWeek := []string{time.Sunday: "So", time.Monday: "Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"}
	months := []string{"Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"}
	return dayOfWeek[t.Weekday()] + ", " + strconv.Itoa(t.Day()) + ". " + months[t.Month()-1] + " " + strconv.Itoa(t.Year())
}

var IsCertificateDowngraded = func() string {
	return ""
}

func templateParser(fsys fs.FS, files ...string) func() (*template.Template, error) {
	parseTemplate := func() (*template.Template, error) {
		// See http://masterminds.github.io/sprig/
		return template.New("").Funcs(sprig.FuncMap()).Funcs(template.FuncMap{
			"csrfField": func() (template.HTML, error) {
				return "", errors.New("csrfField is not implemented")
			},
			"isCertificateDowngraded": func() string {
				return IsCertificateDowngraded()
			},
			"germanDate": GermanDate,
			"germanTime": func(t time.Time) string {
				return t.Local().Format("15:04")
			},
			"formatMinute": func(m int32) string {
				return fmt.Sprintf("%d:%02d", m/60, m%60)
			},
			"formatMinuteEnd": func(m int32) string {
				m++
				return fmt.Sprintf("%d:%02d", m/60, m%60)
			},
			"sameMinute": func(t time.Time, m int32) bool {
				l := t.Local()
				return int32(l.Hour()*60+l.Minute()) == m
			},
			"germanMonth": func(m time.Month) string {
				months := []string{"Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"}
				return months[m-1]
			},
			"isWeekend": func(w time.Weekday) bool {
				return w == time.Saturday || w == time.Sunday
			},
			"isPast": func(u time.Time) bool {
				return u.Before(time.Now())
			},
			"roundedAmount32": func(amount int32) string {
				return FormattedCentimes(int(amount), ",")
			},
			"roundedAmount": func(amount int) string {
				return FormattedCentimes(amount, ",")
			},
			"spacedPhone": spacedPhone,
			"getField":    getField,
			"parseEmail":  mail.ParseAddress,
			"versionInfo": func() string {
				return versionInfo
			},
		}).ParseFS(fsys, files...)
	}

	if _, ok := fsys.(embed.FS); ok {
		// if embedded, pre-parse
		tpl, err := parseTemplate()
		if err != nil {
			panic(err)
		}
		return func() (*template.Template, error) {
			// use Clone to prevent race condition (since we are calling `Funcs` on the result)
			// see https://www.calhoun.io/intro-to-templates-p3-functions/ for more infos
			return tpl.Clone()
		}
	}

	return parseTemplate
}
func getField(v interface{}, name string) interface{} {
	rv := reflect.ValueOf(v)
	if rv.Kind() == reflect.Ptr {
		rv = rv.Elem()
	}
	if rv.Kind() != reflect.Struct {
		return false
	}
	f := rv.FieldByName(name)
	if !f.IsValid() {
		return false
	}
	return f.Interface()
}

func spacedPhone(phone string) string {
	if len(phone) < 2 || strings.ContainsRune(phone, ' ') { // already formatted
		return phone
	}
	if phone[0] == '0' && phone[1] != '0' {
		return "0" + insertSpaceNth(phone[1:], 3)
	}
	return insertSpaceNth(phone, 4)
}

// https://stackoverflow.com/a/33633451/3207406
func insertSpaceNth(s string, n int) string {
	var buffer bytes.Buffer
	var nDecr = n - 1
	var lDecr = len(s) - 1
	for i, r := range s {
		buffer.WriteRune(r)
		if i%n == nDecr && i != lDecr {
			buffer.WriteRune(' ')
		}
	}
	return buffer.String()
}

func FormattedCentimes(amount int, comma string) string {
	var s string
	if amount < 0 {
		s += "-"
		amount = -amount
	}
	s += strconv.Itoa(amount / 100)
	cents := amount % 100
	if cents == 0 {
		return s
	}
	s += comma
	if cents < 10 {
		s += "0"
	}
	s += strconv.Itoa(cents)
	return s
}
