// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package view

import (
	"fmt"
	"strconv"
	"strings"
	"testing"

	"gotest.tools/v3/assert"
)

func TestCentimeOperations(t *testing.T) {
	cases := map[int]string{
		0:   "0",
		1:   "0.01",
		9:   "0.09",
		10:  "0.10",
		99:  "0.99",
		100: "1",
		101: "1.01",
		110: "1.10",

		-1:   "-0.01",
		-9:   "-0.09",
		-10:  "-0.10",
		-99:  "-0.99",
		-100: "-1",
		-101: "-1.01",
		-110: "-1.10",
	}

	for integer, formatted := range cases {
		t.Run(formatted, func(t *testing.T) {
			f := FormattedCentimes(integer, ".")
			assert.Equal(t, formatted, f)

			m, err := multiplyByHundred(formatted)
			assert.NilError(t, err)
			i, err := strconv.Atoi(m)
			assert.Equal(t, integer, i)
			assert.NilError(t, err)
		})
	}
}

func multiplyByHundred(v string) (string, error) {
	index := strings.Index(v, ".")
	if index == -1 {
		v += "00"
	} else {
		decimals := v[index+1:]
		v = v[:index]
		switch len(decimals) {
		case 0:
			v += "00"
		case 1:
			v += decimals + "0"
		case 2:
			v += decimals
		default:
			return "", fmt.Errorf("%v.%v has too many decimals", v, decimals)
		}
	}
	return v, nil
}
