// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package view

import (
	"bytes"
	"embed"
	"io/fs"
	"path/filepath"
	"text/template"
	"time"
)

type ExecuteEmailTemplate func(data interface{}) (subject string, body []byte, err error)

func NewEmailTemplate(fs fs.FS, files ...string) ExecuteEmailTemplate {
	parse := textTemplateParser(fs, files...)
	return func(data interface{}) (string, []byte, error) {
		tmpl, err := parse()
		if err != nil {
			return "", nil, err
		}
		var subject, body bytes.Buffer

		if err = tmpl.Execute(&body, data); err != nil {
			return "", nil, err
		}

		if err = tmpl.ExecuteTemplate(&subject, "subject", data); err != nil {
			return "", nil, err
		}

		return subject.String(), body.Bytes(), err
	}
}

func textTemplateParser(fsys fs.FS, files ...string) func() (*template.Template, error) {
	name := filepath.Base(files[0])
	parseTemplate := func() (*template.Template, error) {
		// See http://masterminds.github.io/sprig/
		return template.New(name).Funcs(template.FuncMap{
			"germanDate": GermanDate,
			"germanTime": func(t time.Time) string {
				return t.Local().Format("15:04")
			},
			"plural": func(one, many string, count int) string {
				if count == 1 {
					return one
				}
				return many
			},
			"roundedAmount": func(amount int) string {
				return FormattedCentimes(amount, ",")
			},
		}).ParseFS(fsys, files...)
	}

	if _, ok := fsys.(embed.FS); ok {
		// if embedded, pre-parse
		tpl, err := parseTemplate()
		if err != nil {
			panic(err)
		}
		return func() (*template.Template, error) {
			// use Clone to prevent race condition according to https://www.calhoun.io/intro-to-templates-p3-functions/
			return tpl.Clone()
		}
	}

	return parseTemplate
}
