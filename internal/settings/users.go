// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package settings

import (
	"net/http"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
)

const userAdmin = "admin"

func isAdmin(u database.User) bool {
	return u.Role == userAdmin
}

func (h Handler) userIndex(rw http.ResponseWriter, r *http.Request) error {
	self, err := h.CurrentUser(r)
	if err != nil {
		return err
	}

	users, err := h.DB.SettingUserIndex(r.Context())
	if err != nil {
		return err
	}
	type viewUser struct {
		database.User
		IsOpen       bool
		IsAdmin      bool
		IsSelf       bool
		IsAuthorized bool
		HouseRoles   map[string]string
	}
	vSelf := viewUser{
		User:         *self,
		IsAdmin:      isAdmin(*self),
		IsSelf:       true,
		IsAuthorized: true,
	}

	houseUsers, err := h.DB.SettingHouseUserIndex(r.Context())
	if err != nil {
		return err
	}

	vUsers := make([]viewUser, 0, len(users))
	for _, u := range users {
		houseRoles := make(map[string]string)
		for _, hu := range houseUsers {
			if hu.UserID != u.ID {
				continue
			}
			houseRoles[hu.HouseID] = hu.Role
		}

		vUsers = append(vUsers, viewUser{
			User:         u,
			IsOpen:       vSelf.IsAdmin && !u.AuthorizedAt.Valid && u.CreatedAt.After(time.Now().Add(-7*24*time.Hour)),
			IsAdmin:      isAdmin(u),
			IsSelf:       u.ID == self.ID,
			IsAuthorized: u.AuthorizedAt.Valid,
			HouseRoles:   houseRoles,
		})
	}

	houses, err := h.DB.SettingHouseIndex(r.Context())
	if err != nil {
		return err
	}

	return h.UserIndexView.Render(rw, r, struct {
		Nav        internal.NavbarData
		Users      []viewUser
		Houses     []database.House
		HouseRoles internal.Options
		Self       viewUser
	}{
		Nav: internal.NavbarData{
			User:             self,
			SelectedDropdown: "einstellungen",
			SelectedURI:      "einstellungen/benutzer",
		},
		Users:      vUsers,
		Houses:     houses,
		HouseRoles: internal.HouseRoles,
		Self:       vSelf,
	})
}

func (h Handler) userUpdate(rw http.ResponseWriter, r *http.Request) error {
	self, err := h.CurrentUser(r)
	if err != nil {
		return err
	}

	user, err := h.DB.SettingUserFind(r.Context(), r.FormValue("id"))
	if err != nil {
		return err
	}

	if isAdmin(*self) {
		if err = h.userAdminUpdate(r, &user, self.ID == user.ID); err != nil {
			return err
		}
	}

	err = h.DB.SettingUserUpdate(r.Context(), database.SettingUserUpdateParams{
		ID:           user.ID,
		AuthorizedAt: user.AuthorizedAt,
		Role:         user.Role,
	})
	if err != nil {
		return err
	}

	return h.RedirectBack(rw, r)
}

func (h Handler) userAdminUpdate(r *http.Request, user *database.User, isSelf bool) error {
	// update user authorization and role
	if !isSelf {
		authorized := r.FormValue("authorized") == "1"
		user.AuthorizedAt = updatedNullTime(user.AuthorizedAt, authorized)

		isAdmin := r.FormValue("is_admin") == "1"
		if isAdmin {
			user.Role = userAdmin
		} else {
			user.Role = ""
		}
	}

	// update house roles
	houses, err := h.DB.SettingUserHouseRoles(r.Context(), user.ID)
	if err != nil {
		return err
	}

	for _, house := range houses {
		role := r.FormValue("house[" + house.HouseID + "]")
		if house.Role.String != role {
			// insert or update
			if house.Role.Valid {
				err = h.DB.SettingHouseUserUpdateRole(r.Context(), database.SettingHouseUserUpdateRoleParams{
					Role:    role,
					HouseID: house.HouseID,
					UserID:  user.ID,
				})
			} else {
				err = h.DB.SettingHouseUserInsertRole(r.Context(), database.SettingHouseUserInsertRoleParams{
					Role:    role,
					HouseID: house.HouseID,
					UserID:  user.ID,
				})
			}
			if err != nil {
				return err
			}
		}
	}
	return nil
}
