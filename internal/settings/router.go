// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package settings

import (
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/buchungssystem/internal/view"
	"github.com/go-chi/chi/v5"
)

type Handler struct {
	internal.Handler
	UserIndexView         *view.Template
	HouseIndexView        *view.Template
	PriceIndexView        *view.Template
	ChargeIndexView       *view.Template
	ChargeEditView        *view.Template
	DayIndexView          *view.Template
	RestIndexView         *view.Template
	ExtraRequestIndexView *view.Template
	ExtraRequestEditView  *view.Template
}

func (h Handler) Router(r chi.Router) {
	r.Get("/benutzer", h.HandleErr(h.userIndex))
	r.Put("/benutzer", h.HandleErr(h.userUpdate))

	r.Get("/haus", h.HandleErr(h.houseIndex))
	r.Post("/haus", h.HandleErr(h.houseCreate))
	r.Put("/haus", h.HandleErr(h.houseUpdate))

	r.Get("/preissaison", h.HandleErr(h.priceIndex))
	r.Post("/preissaison", h.HandleErr(h.priceCreate))
	r.Put("/preissaison", h.HandleErr(h.priceUpdate))

	r.Get("/kosten", h.HandleErr(h.chargeIndex))
	r.Get("/kosten/bearbeiten", h.HandleErr(h.chargeEdit))
	r.Post("/kosten/bearbeiten", h.HandleErr(h.chargeCreate))
	r.Put("/kosten/bearbeiten", h.HandleErr(h.chargeUpdate))

	r.Get("/sonderwuensche", h.HandleErr(h.extraRequestsIndex))
	r.Get("/sonderwuensche/bearbeiten", h.HandleErr(h.extraRequestsEdit))
	r.Post("/sonderwuensche/bearbeiten", h.HandleErr(h.extraRequestsCreate))
	r.Put("/sonderwuensche/bearbeiten", h.HandleErr(h.extraRequestsUpdate))

	r.Get("/buchungsnaechte", h.HandleErr(h.dayIndex))
	r.Put("/buchungsnaechte", h.HandleErr(h.dayUpdate))

	r.Get("/ruhetage", h.HandleErr(h.restDayIndex))
	r.Put("/ruhetage", h.HandleErr(h.restDayUpdate))
}
