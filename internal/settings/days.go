// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package settings

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/buchungssystem/internal/validator"
)

const dateLayout = "2006-01-02"

type period struct {
	Start time.Time
	End   time.Time

	// used by "rest_days"
	RestrictionReason string

	// used by "days"
	Price database.Price
}

type dayGatherer struct {
	today           string
	DB              *database.Queries
	prices          []database.Price
	latestCommonDay string
}

func (pg dayGatherer) findPrice(priceID string) database.Price {
	for _, p := range pg.prices {
		if p.ID == priceID {
			return p
		}
	}
	return database.Price{
		ID: priceID,
	}
}

func (pg *dayGatherer) gather(ctx context.Context, houseID string) ([]period, error) {
	days, err := pg.DB.SettingBookableDayIndex(ctx, database.SettingBookableDayIndexParams{
		Date:    pg.today,
		HouseID: houseID,
	})
	if err != nil {
		return nil, err
	}
	if len(days) == 0 {
		pg.latestCommonDay = pg.today
		return nil, nil
	}

	var periods []period //nolint:prealloc
	var current period
	for _, day := range days {
		date, err := time.Parse(dateLayout, day.Date)
		if err != nil {
			return nil, err
		}
		if current.Start.IsZero() {
			current.Start = date
			current.End = date.AddDate(0, 0, 1)
			current.Price = pg.findPrice(day.PriceID)
			continue
		}
		if current.Price.ID == day.PriceID && current.End.Equal(date) {
			// only contiguous days
			current.End = date.AddDate(0, 0, 1)
			continue
		}

		// new period
		periods = append(periods, current)
		current.Start = date
		current.End = date.AddDate(0, 0, 1)
		current.Price = pg.findPrice(day.PriceID)
	}

	periods = append(periods, current)

	nextDay := current.End.Format(dateLayout)
	if pg.latestCommonDay == "" || nextDay < pg.latestCommonDay {
		pg.latestCommonDay = nextDay
	}
	return periods, nil
}

func (h Handler) dayIndex(rw http.ResponseWriter, r *http.Request) error {
	self, err := h.CurrentUser(r)
	if err != nil {
		return err
	}

	policy := h.HousePolicy(r)
	houses, err := h.DB.SettingViewableHouseIndex(r.Context(), self.ID)
	if err != nil {
		return err
	}

	prices, err := h.DB.SettingPriceIndexActive(r.Context())
	if err != nil {
		return err
	}

	pg := &dayGatherer{
		today:           time.Now().Format(dateLayout),
		DB:              h.DB,
		prices:          prices,
		latestCommonDay: "",
	}

	type House database.SettingViewableHouseIndexRow
	type viewHouse struct {
		House
		CanEdit bool
		IsOpen  bool
		Periods []period
	}
	houseID := r.FormValue("house_id")
	vHouses := make([]viewHouse, 0, len(houses))
	canEditOne := false
	for _, h := range houses {
		periods, err := pg.gather(r.Context(), h.ID)
		if err != nil {
			return err
		}
		canEdit := policy.CanEditDay(h.ID) == nil
		canEditOne = canEditOne || canEdit
		vHouses = append(vHouses, viewHouse{
			House:   House(h),
			CanEdit: canEdit,
			IsOpen:  h.ID == houseID || len(houses) == 1,
			Periods: periods,
		})
	}

	return h.DayIndexView.Render(rw, r, struct {
		Nav             internal.NavbarData
		Houses          []viewHouse
		Prices          []database.Price
		CanEdit         bool
		CurrentDay      string
		LatestCommonDay string
	}{
		Nav: internal.NavbarData{
			User:             self,
			SelectedDropdown: "einstellungen",
			SelectedURI:      "einstellungen/buchungsnaechte",
		},
		Houses:          vHouses,
		Prices:          prices,
		CanEdit:         canEditOne,
		CurrentDay:      pg.today,
		LatestCommonDay: pg.latestCommonDay,
	})
}

func (h Handler) dayUpdate(rw http.ResponseWriter, r *http.Request) error {
	v := validator.New(r.FormValue)

	start := v.TrimmedString("start", validDatestring)
	cr := database.SettingBookableDayUpdateParams{
		Start: start,
		EndDate: v.TrimmedString("end", validDatestring, func(end string) bool {
			return end > start
		}),
		PriceID: v.TrimmedString("price_id"),
		// HouseID
	}

	if err := v.HTTPErr(); err != nil {
		return err
	}

	policy := h.HousePolicy(r)
	for _, houseID := range r.Form["house_id[]"] {
		if err := policy.CanEditDay(houseID); err != nil {
			continue
		}
		cr.HouseID = houseID

		if r.FormValue("update_existing") == "1" {
			err := h.DB.SettingBookableDayUpdate(r.Context(), cr)
			if err != nil {
				return fmt.Errorf("could not update days for %s: %w", houseID, err)
			}
		}

		err := h.DB.SettingBookableDayCreate(r.Context(), cr)
		if err != nil {
			return fmt.Errorf("could not create days for %s: %w", houseID, err)
		}
	}

	return h.RedirectBack(rw, r)
}

func validDatestring(s string) bool {
	_, err := time.Parse(dateLayout, s)
	return err == nil
}
