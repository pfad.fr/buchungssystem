// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package settings

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/buchungssystem/internal/validator"
)

func (h Handler) restDayIndex(rw http.ResponseWriter, r *http.Request) error {
	self, err := h.CurrentUser(r)
	if err != nil {
		return err
	}

	policy := h.HousePolicy(r)
	houses, err := h.DB.SettingViewableHouseIndex(r.Context(), self.ID)
	if err != nil {
		return err
	}

	rp := restPeriodGatherer{
		today: time.Now().Format(dateLayout),
		DB:    h.DB,
	}

	type House database.SettingViewableHouseIndexRow
	type viewHouse struct {
		House
		CanEdit bool
		IsOpen  bool
		Periods []period
	}
	houseID := r.FormValue("house_id")
	vHouses := make([]viewHouse, 0, len(houses))
	canEditOne := false
	for _, h := range houses {
		periods, err := rp.gather(r.Context(), h.ID)
		if err != nil {
			return err
		}
		canEdit := policy.CanEditRestDay(h.ID) == nil
		canEditOne = canEditOne || canEdit
		vHouses = append(vHouses, viewHouse{
			House:   House(h),
			CanEdit: canEdit,
			IsOpen:  h.ID == houseID || len(houses) == 1,
			Periods: periods,
		})
	}

	return h.RestIndexView.Render(rw, r, struct {
		Nav        internal.NavbarData
		Houses     []viewHouse
		CanEdit    bool
		CurrentDay string
	}{
		Nav: internal.NavbarData{
			User:             self,
			SelectedDropdown: "einstellungen",
			SelectedURI:      "einstellungen/ruhetage",
		},
		Houses:     vHouses,
		CanEdit:    canEditOne,
		CurrentDay: rp.today,
	})
}

type restPeriodGatherer struct {
	today string
	DB    *database.Queries
}

func (rp restPeriodGatherer) gather(ctx context.Context, houseID string) ([]period, error) {
	days, err := rp.DB.SettingRestDayIndex(ctx, database.SettingRestDayIndexParams{
		HouseID: houseID,
		Date:    rp.today,
	})
	if err != nil {
		return nil, err
	}

	if len(days) == 0 {
		return nil, nil
	}

	var periods []period //nolint:prealloc
	var current period
	for _, day := range days {
		date, err := time.Parse(dateLayout, day.Date)
		if err != nil {
			return nil, err
		}
		if current.Start.IsZero() {
			current.Start = date
			current.End = date.AddDate(0, 0, 1)
			current.RestrictionReason = day.RestrictionReason
			continue
		}
		if current.RestrictionReason == day.RestrictionReason && current.End.Equal(date) {
			// only contiguous days, with the same reason
			current.End = date.AddDate(0, 0, 1)
			continue
		}

		// new period
		periods = append(periods, current)
		current.Start = date
		current.End = date.AddDate(0, 0, 1)
		current.RestrictionReason = day.RestrictionReason
	}
	periods = append(periods, current)

	return periods, nil
}

func (h Handler) restDayUpdate(rw http.ResponseWriter, r *http.Request) error {
	v := validator.New(r.FormValue)

	start := v.TrimmedString("start", validDatestring)
	cr := database.SettingRestDayUpdateParams{
		Start: start,
		EndDate: v.TrimmedString("end", validDatestring, func(end string) bool {
			return end > start
		}),
		// HouseID
		// RestrictionReason
	}
	if v.TrimmedString("restriction") == "rest" {
		cr.RestrictionReason = v.TrimmedString("reason")
		if cr.RestrictionReason == "" {
			cr.RestrictionReason = "-"
		}
	}

	if err := v.HTTPErr(); err != nil {
		return err
	}

	policy := h.HousePolicy(r)
	for _, houseID := range r.Form["house_id[]"] {
		if err := policy.CanEditRestDay(houseID); err != nil {
			continue
		}
		cr.HouseID = houseID

		err := h.DB.SettingRestDayUpdate(r.Context(), cr)
		if err != nil {
			return fmt.Errorf("could not update days for %s: %w", houseID, err)
		}
	}

	return h.RedirectBack(rw, r)
}
