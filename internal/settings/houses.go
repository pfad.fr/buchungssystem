// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package settings

import (
	"fmt"
	"net/http"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/buchungssystem/internal/validator"
)

const houseAdmin = "admin"

func formattedTimeMinute(minute int32) string {
	return fmt.Sprintf("%02d:%02d", minute/60, minute%60)
}

func (h Handler) houseIndex(rw http.ResponseWriter, r *http.Request) error {
	self, err := h.CurrentUser(r)
	if err != nil {
		return err
	}

	policy := h.HousePolicy(r)
	houses, err := h.DB.SettingHouseIndex(r.Context())
	if err != nil {
		return err
	}
	houses = policy.Filter(houses, func(id string) error {
		if policy.CanCreate() == nil { // to prevent accidental duplicates
			return nil
		}
		return policy.CanViewBooking(id)
	})
	type viewHouse struct {
		database.House
		CanEdit   bool
		StartTime string
		EndTime   string
	}

	vHouses := make([]viewHouse, 0, len(houses))
	for _, h := range houses {
		vHouses = append(vHouses, viewHouse{
			House:     h,
			CanEdit:   policy.CanEditHouse(h.ID) == nil,
			StartTime: formattedTimeMinute(h.StartMinute),
			EndTime:   formattedTimeMinute(h.EndMinute),
		})
	}

	return h.HouseIndexView.Render(rw, r, struct {
		Nav          internal.NavbarData
		Houses       []viewHouse
		CanCreate    bool
		BookingTypes internal.Options
	}{
		Nav: internal.NavbarData{
			User:             self,
			SelectedDropdown: "einstellungen",
			SelectedURI:      "einstellungen/haus",
		},
		Houses:       vHouses,
		CanCreate:    policy.CanCreate() == nil,
		BookingTypes: internal.HouseBookingTypes,
	})
}

func (h Handler) houseCreate(rw http.ResponseWriter, r *http.Request) error {
	self, err := h.CurrentUser(r)
	if err != nil {
		return err
	}

	hp := h.HousePolicy(r)
	if err = hp.CanCreate(); err != nil {
		return err
	}

	v := validator.New(r.FormValue)
	params := database.SettingHouseCreateParams{
		ID:        v.TrimmedString("id"),
		Name:      v.TrimmedString("name"),
		MaxPerson: int32(v.ParsedInt("max_person")),
		ReplyTo:   v.Email("reply_to"),
	}
	if v.Err() != nil {
		return v.HTTPErr()
	}
	if err = h.DB.SettingHouseCreate(r.Context(), params); err != nil {
		return err
	}

	// add admin permission
	err = h.DB.SettingHouseUserInsertRole(r.Context(), database.SettingHouseUserInsertRoleParams{
		Role:    houseAdmin,
		HouseID: params.ID,
		UserID:  self.ID,
	})
	if err != nil {
		return err
	}

	return h.RedirectBack(rw, r)
}

func (h Handler) houseUpdate(rw http.ResponseWriter, r *http.Request) error {
	house, err := h.DB.SettingHouseFind(r.Context(), r.FormValue("id"))
	if err != nil {
		return err
	}
	hp := h.HousePolicy(r)
	if err := hp.CanEditHouse(house.ID); err != nil {
		return err
	}

	v := validator.New(r.FormValue)
	params := database.SettingHouseUpdateParams{
		ID:                house.ID,
		Name:              v.TrimmedString("name"),
		MaxPerson:         int32(v.ParsedInt("max_person")),
		ReplyTo:           v.Email("reply_to"),
		StartMinute:       v.ParsedTimeAsMinute("start_time"),
		EndMinute:         v.ParsedTimeAsMinute("end_time"),
		PublicBookingType: v.TrimmedString("booking_type", internal.HouseBookingTypes.Contains),
	}
	if v.Err() != nil {
		return v.HTTPErr()
	}
	switch params.PublicBookingType {
	case "overday":
		if params.StartMinute > params.EndMinute {
			return internal.HTTPError{
				Code:    http.StatusUnprocessableEntity,
				Message: `Buchungsart "Tagsüber": die Anreise-Uhrzeit muss FRÜHER sein als die Abreise-Uhrzeit`,
			}
		}
	case "overnight":
		if params.StartMinute < params.EndMinute {
			return internal.HTTPError{
				Code:    http.StatusUnprocessableEntity,
				Message: `Buchungsart "Übernachtung": die Anreise-Uhrzeit muss SPÄTER sein als die Abreise-Uhrzeit`,
			}
		}
	default:
		return internal.HTTPError{
			Code:    http.StatusUnprocessableEntity,
			Message: "unsupported Buchungsart: " + params.PublicBookingType,
		}
	}
	if err := h.DB.SettingHouseUpdate(r.Context(), params); err != nil {
		return err
	}
	return h.RedirectBack(rw, r)
}
