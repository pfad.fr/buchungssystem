// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package settings

import (
	"net/http"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/buchungssystem/internal/validator"
	"code.pfad.fr/buchungssystem/internal/view"
)

var optionalityOptions = internal.Options{
	{"", "Pflichtkosten"},
	{"admin", "Nur von Admin buchbar"},
}

func (h Handler) chargeIndex(rw http.ResponseWriter, r *http.Request) error {
	self, err := h.CurrentUser(r)
	if err != nil {
		return err
	}

	prices, err := h.DB.SettingPriceIndexActive(r.Context())
	if err != nil {
		return err
	}
	if len(prices) == 0 {
		return internal.HTTPError{
			Code:    http.StatusUnprocessableEntity,
			Message: "Um Preise zu verwalten muss mindestens eine Preissaison aktiv sein.",
		}
	}
	priceID := r.FormValue("price_id")
	var price database.Price
	for _, price = range prices {
		if price.ID == priceID {
			break
		}
	}

	policy := h.HousePolicy(r)
	houses, err := h.DB.SettingViewableHouseIndex(r.Context(), self.ID)
	if err != nil {
		return err
	}

	charges, err := h.DB.SettingChargeIndexPerPrice(r.Context(), price.ID)
	if err != nil {
		return err
	}

	type House database.SettingViewableHouseIndexRow
	type viewHouse struct {
		House
		CanEdit bool
		IsOpen  bool
	}
	houseID := r.FormValue("house_id")
	vHouses := make([]viewHouse, 0, len(houses))
	for _, h := range houses {
		vHouses = append(vHouses, viewHouse{
			House:   House(h),
			CanEdit: policy.CanEditCharge(h.ID) == nil,
			IsOpen:  h.ID == houseID || len(houses) == 1,
		})
	}

	type viewCharge struct {
		database.Charge
		CanEdit         bool
		RoundedAmount   string
		OptionalityName string
	}

	vCharges := make(map[string][]viewCharge, 0)
	for _, c := range charges {
		vc := viewCharge{
			Charge:          c,
			CanEdit:         policy.CanEditCharge(c.HouseID) == nil,
			RoundedAmount:   view.FormattedCentimes(int(c.Amount), ","),
			OptionalityName: optionalityOptions.Name(c.Optionality),
		}
		vCharges[c.HouseID] = append(vCharges[c.HouseID], vc)
	}

	return h.ChargeIndexView.Render(rw, r, struct {
		Nav           internal.NavbarData
		Charges       map[string][]viewCharge
		Prices        []database.Price
		Houses        []viewHouse
		SelectedPrice database.Price
		HouseID       string
	}{
		Nav: internal.NavbarData{
			User:             self,
			SelectedDropdown: "einstellungen",
			SelectedURI:      "einstellungen/kosten",
		},
		Charges:       vCharges,
		Prices:        prices,
		Houses:        vHouses,
		SelectedPrice: price,
		HouseID:       houseID,
	})
}

func (h Handler) chargeEdit(rw http.ResponseWriter, r *http.Request) error {
	self, err := h.CurrentUser(r)
	if err != nil {
		return err
	}

	house, err := h.DB.SettingHouseFind(r.Context(), r.FormValue("house_id"))
	if err != nil {
		return err
	}
	hp := h.HousePolicy(r)
	if err = hp.CanEditCharge(house.ID); err != nil {
		return err
	}

	price, err := h.DB.SettingPriceFind(r.Context(), r.FormValue("price_id"))
	if err != nil {
		return err
	}

	charges, err := h.DB.SettingChargeIndexPerHousePrice(r.Context(), database.SettingChargeIndexPerHousePriceParams{
		PriceID: price.ID,
		HouseID: house.ID,
	})
	if err != nil {
		return err
	}

	type viewCharge struct {
		database.Charge
		RoundedAmount         string
		OptionalityOptions    internal.Options
		Unit_per_person_night bool //nolint:stylecheck,revive
		Unit_per_stay         bool //nolint:stylecheck,revive
	}
	vCharges := make([]viewCharge, 0, len(charges))
	for _, c := range charges {
		vc := viewCharge{
			Charge:                c,
			RoundedAmount:         view.FormattedCentimes(int(c.Amount), "."),
			OptionalityOptions:    optionalityOptions,
			Unit_per_person_night: c.Unit == "per_person_night",
			Unit_per_stay:         c.Unit == "per_stay",
		}
		vCharges = append(vCharges, vc)
	}

	isBooked, err := h.DB.SettingPriceHasBooking(r.Context(), database.SettingPriceHasBookingParams{
		PriceID: price.ID,
		HouseID: house.ID,
	})
	if err != nil {
		return err
	}

	emptyCharge := viewCharge{
		OptionalityOptions: optionalityOptions,
	}
	if isBooked {
		emptyCharge.Optionality = "admin"
	}

	return h.ChargeEditView.Render(rw, r, struct {
		Nav         internal.NavbarData
		House       database.House
		Price       database.Price
		Charges     []viewCharge
		EmptyCharge viewCharge
		IsBooked    bool
	}{
		Nav: internal.NavbarData{
			User:             self,
			SelectedDropdown: "einstellungen",
			SelectedURI:      "einstellungen/kosten",
		},
		House:       house,
		Price:       price,
		Charges:     vCharges,
		EmptyCharge: emptyCharge,
		IsBooked:    isBooked,
	})
}

func (h Handler) chargeCreate(rw http.ResponseWriter, r *http.Request) error {
	house, err := h.DB.SettingHouseFind(r.Context(), r.FormValue("house_id"))
	if err != nil {
		return err
	}
	hp := h.HousePolicy(r)
	if err = hp.CanEditCharge(house.ID); err != nil {
		return err
	}

	up, err := chargeUpdateParams(r.FormValue, "")
	if err != nil {
		return internal.HTTPError{
			Code:    http.StatusUnprocessableEntity,
			Message: err.Error(),
		}
	}

	id, err := newRandomID("ch", 4)
	if err != nil {
		return err
	}

	params := database.SettingChargeCreateParams{
		HouseID:                house.ID,
		PriceID:                r.FormValue("price_id"),
		ID:                     id,
		Name:                   up.Name,
		Description:            up.Description,
		Unit:                   up.Unit,
		Amount:                 up.Amount,
		MinPersonCharged:       up.MinPersonCharged,
		ChargedOverPersonCount: up.ChargedOverPersonCount,
		ExemptedUnderAge:       up.ExemptedUnderAge,
		Optionality:            up.Optionality,
	}
	if err := h.DB.SettingChargeCreate(r.Context(), params); err != nil {
		return err
	}

	// redirect back, to the #create block
	http.Redirect(rw, r, r.Header.Get("Referer")+"#create", http.StatusFound)
	return nil
}

func (h Handler) chargeUpdate(rw http.ResponseWriter, r *http.Request) error {
	house, err := h.DB.SettingHouseFind(r.Context(), r.FormValue("house_id"))
	if err != nil {
		return err
	}
	hp := h.HousePolicy(r)
	if err = hp.CanEditCharge(house.ID); err != nil {
		return err
	}

	price, err := h.DB.SettingPriceFind(r.Context(), r.FormValue("price_id"))
	if err != nil {
		return err
	}

	charges, err := h.DB.SettingChargeIndexPerHousePrice(r.Context(), database.SettingChargeIndexPerHousePriceParams{
		PriceID: price.ID,
		HouseID: house.ID,
	})
	if err != nil {
		return err
	}
	for _, c := range charges {
		if r.FormValue("delete["+c.ID+"]") == "1" {
			err = h.DB.SettingChargeDelete(r.Context(), c.ID)
			if err != nil {
				return err
			}
			continue
		}
		up, err := chargeUpdateParams(r.FormValue, c.ID)
		if err != nil {
			return internal.HTTPError{
				Code:    http.StatusUnprocessableEntity,
				Message: c.Name + ": " + err.Error(),
			}
		}

		if err := h.DB.SettingChargeUpdate(r.Context(), up); err != nil {
			return err
		}
	}

	http.Redirect(rw, r, "/verwaltung/einstellungen/kosten?price_id="+price.ID+"&house_id="+house.ID, http.StatusFound)
	return nil
}

func chargeUpdateParams(valueOf func(string) string, id string) (database.SettingChargeUpdateParams, error) {
	v := validator.New(func(name string) string {
		return valueOf(name + "[" + id + "]")
	})

	up := database.SettingChargeUpdateParams{
		ID:          id,
		Name:        v.TrimmedString("name"),
		Description: v.TrimmedString("description"),
		Amount:      int32(v.ParsedCentime("amount")),
		Optionality: v.TrimmedString("optionality", optionalityOptions.Contains),
	}
	up.Unit = v.TrimmedString("unit", func(unit string) bool {
		switch unit {
		default:
			return false
		case "per_stay":
			up.ChargedOverPersonCount = int32(v.ParsedInt("charged_over_person_count"))
		case "per_person_night":
			up.MinPersonCharged = int32(v.ParsedInt("min_person_charged"))
			up.ExemptedUnderAge = int32(v.ParsedInt("exempted_under_age"))
		}
		return true
	})

	return up, v.Err()
}
