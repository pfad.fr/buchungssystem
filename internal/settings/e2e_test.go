// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package settings

import (
	"context"
	"database/sql"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"regexp"
	"strings"
	"testing"
	"time"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/buchungssystem/internal/validator"
	"code.pfad.fr/buchungssystem/internal/view"
	"github.com/go-chi/chi/v5"
	"gotest.tools/v3/assert"

	_ "github.com/mattn/go-sqlite3"
)

func newDatabase(t *testing.T) *database.Queries {
	dataSource := "file::memory:?cache=shared&_journal_mode=WAL&_busy_timeout=5000&_foreign_keys=on"
	dbPath := os.Getenv("TESTS_ON_ACTUAL_DB")
	if dbPath != "" {
		os.Remove(dbPath)
		dataSource = dbPath + "?_journal_mode=WAL&_busy_timeout=5000&_foreign_keys=on"
	}
	db, err := sql.Open("sqlite3", dataSource)
	assert.NilError(t, err)

	migrate, err := database.MissingMigrations(db, "sqlite3")
	assert.NilError(t, err)

	queries := database.New(db)
	if migrate != nil {
		err := migrate()
		assert.NilError(t, err)

		err = queries.AuthInsert(context.Background(), database.AuthInsertParams{
			ID:    "admin1",
			Email: "admin1@example.com",
			Name:  "Admin 1",
		})
		assert.NilError(t, err)

		err = queries.SettingUserUpdate(context.Background(), database.SettingUserUpdateParams{
			AuthorizedAt: sql.NullTime{
				Time:  time.Now(),
				Valid: true,
			},
			Role: "admin",
			ID:   "admin1",
		})
		assert.NilError(t, err)

		err = queries.AuthInsert(context.Background(), database.AuthInsertParams{
			ID:    "AAAAAAAAAAAAAAAAAAAAAF4Wi8Bbjj9_eCJwcTLPSEU",
			Email: "oc@atelier.dev",
			Name:  "Olivier",
		})
		assert.NilError(t, err) // admin role will be set in tests
	}

	return queries
}

func authenticatedServer(t *testing.T) func(*http.Request) *httptest.ResponseRecorder {
	r := chi.NewRouter()

	db := newDatabase(t)

	assetsFS := internal.EmbedFS
	h := Handler{
		Handler: internal.Handler{
			DB: db,
			// Queue: queue, not used
			CurrentUser: func(r *http.Request) (*database.User, error) {
				user, err := db.AuthLoad(r.Context(), "admin1")
				return &user, err
			},
			ErrorHandler: internal.ErrorHandler{
				Debug: true,
			},
			RenderErrorView: view.NewTemplate(assetsFS, "doctype", "layout/admin", "httperror").Render,
		},
		UserIndexView:         view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/user_index"),
		HouseIndexView:        view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/house_index"),
		PriceIndexView:        view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/price_index"),
		ChargeIndexView:       view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/charge_index"),
		ChargeEditView:        view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/charge_edit"),
		DayIndexView:          view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/day_index"),
		RestIndexView:         view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/rest_index"),
		ExtraRequestIndexView: view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/extra_request_index"),
		ExtraRequestEditView:  view.NewTemplate(assetsFS, "doctype", "layout/admin", "settings/extra_request_edit"),
	}
	r.Route("/verwaltung/einstellungen", h.Router)

	return func(req *http.Request) *httptest.ResponseRecorder {
		resp := httptest.NewRecorder()
		r.ServeHTTP(resp, req)
		return resp
	}
}

func TestEndToEndAdmin(t *testing.T) {
	validator.EnableFakeDNSResolver()

	serve := authenticatedServer(t)
	var resp *httptest.ResponseRecorder

	t.Run("house creation and update", func(t *testing.T) {
		// empty house list
		resp = serve(httptest.NewRequest("GET", "/verwaltung/einstellungen/haus", nil))
		assert.Equal(t, resp.Code, 200)

		// create house
		resp = serve(newRequestWithParams("POST", "/verwaltung/einstellungen/haus", url.Values{
			"id":         values("maison"),
			"name":       values("House"),
			"max_person": values("42"),
			"reply_to":   values("house@example.com"),
		}))
		assertRedirect(t, resp, "/verwaltung/einstellungen/")

		// house list with created house
		resp = serve(httptest.NewRequest("GET", "/verwaltung/einstellungen/haus", nil))
		assert.Equal(t, resp.Code, 200)
		assert.Check(t, strings.Contains(resp.Body.String(), "House"))
		assert.Check(t, !strings.Contains(resp.Body.String(), "Maison"))

		// update house
		resp = serve(newRequestWithParams("PUT", "/verwaltung/einstellungen/haus", url.Values{
			"id":           values("maison"),
			"name":         values("Maison"),
			"max_person":   values("57"),
			"reply_to":     values("seegatterl-buchung@dpsg1300.de"),
			"start_time":   values("17:42"),
			"end_time":     values("12:34"),
			"booking_type": values("overnight"),
		}))
		assertRedirect(t, resp, "/verwaltung/einstellungen/")

		// house list with updated house
		resp = serve(httptest.NewRequest("GET", "/verwaltung/einstellungen/haus", nil))
		assert.Check(t, strings.Contains(resp.Body.String(), "Maison"))
		assert.Check(t, !strings.Contains(resp.Body.String(), "House"))
		assert.Check(t, strings.Contains(resp.Body.String(), "57"))
		assert.Check(t, strings.Contains(resp.Body.String(), "7:42"))
		assert.Check(t, strings.Contains(resp.Body.String(), "12:34"))
	})

	t.Run("house creation and update failures", func(t *testing.T) {
		// create house without id (failure)
		resp = serve(newRequestWithParams("POST", "/verwaltung/einstellungen/haus", url.Values{
			"id":         values(""),
			"name":       values("House"),
			"max_person": values("42"),
			"reply_to":   values("max@example.com"),
		}))
		assert.Equal(t, resp.Code, 400)

		// update inexisting house
		resp = serve(newRequestWithParams("PUT", "/verwaltung/einstellungen/haus", url.Values{
			"id":   values("not_found"),
			"name": values("Maison"),
		}))
		assert.Equal(t, resp.Code, 404)
	})

	t.Run("user list and rights", func(t *testing.T) {
		// user list
		resp = serve(httptest.NewRequest("GET", "/verwaltung/einstellungen/benutzer", nil))
		assert.Equal(t, resp.Code, 200)

		// update own rights regarding the house
		resp = serve(newRequestWithParams("PUT", "/verwaltung/einstellungen/benutzer", url.Values{
			"id":            values("admin1"),
			"house[maison]": values("booking_read"),
		}))
		assertRedirect(t, resp, "/verwaltung/einstellungen/")

		// can't update the house anymore
		resp = serve(newRequestWithParams("PUT", "/verwaltung/einstellungen/haus", url.Values{
			"id":   values("maison"),
			"name": values("Maison"),
		}))
		assert.Equal(t, resp.Code, 403)

		// update own rights backs
		resp = serve(newRequestWithParams("PUT", "/verwaltung/einstellungen/benutzer", url.Values{
			"id":            values("admin1"),
			"house[maison]": values("admin"),
		}))
		assertRedirect(t, resp, "/verwaltung/einstellungen/")

		// update other's rights regarding the house
		resp = serve(newRequestWithParams("PUT", "/verwaltung/einstellungen/benutzer", url.Values{
			"id":            values("AAAAAAAAAAAAAAAAAAAAAF4Wi8Bbjj9_eCJwcTLPSEU"),
			"house[maison]": values("admin"),
			"authorized":    values("1"),
			"is_admin":      values("1"),
		}))
		assertRedirect(t, resp, "/verwaltung/einstellungen/")
	})

	var priceIDs []string
	t.Run("price creation and update", func(t *testing.T) {
		// empty price list
		resp = serve(httptest.NewRequest("GET", "/verwaltung/einstellungen/preissaison", nil))
		assert.Equal(t, resp.Code, 200)

		// create price
		resp = serve(newRequestWithParams("POST", "/verwaltung/einstellungen/preissaison", url.Values{
			"name": values("Prices"),
		}))
		assertRedirect(t, resp, "/verwaltung/einstellungen/")

		// price list with created price
		resp = serve(httptest.NewRequest("GET", "/verwaltung/einstellungen/preissaison", nil))
		assert.Equal(t, resp.Code, 200)
		assert.Check(t, strings.Contains(resp.Body.String(), "Prices"))
		assert.Check(t, !strings.Contains(resp.Body.String(), "Prix"))
		priceIDs = findIDsInForms(resp.Body.String())

		// update price
		resp = serve(newRequestWithParams("PUT", "/verwaltung/einstellungen/preissaison", url.Values{
			"id":   values(priceIDs[0]),
			"name": values("Prix"),
		}))
		assertRedirect(t, resp, "/verwaltung/einstellungen/")

		// price list with updated price
		resp = serve(httptest.NewRequest("GET", "/verwaltung/einstellungen/preissaison", nil))
		assert.Check(t, strings.Contains(resp.Body.String(), "Prix"))
		assert.Check(t, !strings.Contains(resp.Body.String(), "Prices"))
	})

	t.Run("charge creation and update", func(t *testing.T) {
		// empty charge list
		resp = serve(httptest.NewRequest("GET", "/verwaltung/einstellungen/kosten?price_id="+priceIDs[0], nil))
		assert.Equal(t, resp.Code, 200)

		editURL := "/verwaltung/einstellungen/kosten/bearbeiten?price_id=" + priceIDs[0] + "&house_id=maison"
		// empty charge edit screen
		resp = serve(httptest.NewRequest("GET", editURL, nil))
		assert.Equal(t, resp.Code, 200)

		// create charge
		resp = serve(newRequestWithParams("POST", editURL, url.Values{
			"name[]":                      values("Nettoyage"),
			"unit[]":                      values("per_stay"),
			"amount[]":                    values("10.42"),
			"charged_over_person_count[]": values("20"),
		}))
		assertRedirect(t, resp, "/verwaltung/einstellungen/kosten/#create")

		// charge list with created charge
		resp = serve(httptest.NewRequest("GET", editURL, nil))
		assert.Equal(t, resp.Code, 200)
		assert.Check(t, strings.Contains(resp.Body.String(), "Nettoyage"))
		assert.Check(t, !strings.Contains(resp.Body.String(), "Cleaning"))

		chargeIDs := findChargeIDsInForms(resp.Body.String())

		// update charge
		resp = serve(newRequestWithParams("PUT", editURL, url.Values{
			"name[" + chargeIDs[0] + "]": values("Cleaning"),
			"unit[" + chargeIDs[0] + "]": values("per_person_night"),
			"amount[]":                   values("1"),
		}))
		assertRedirect(t, resp, "/verwaltung/einstellungen/kosten?price_id="+priceIDs[0]+"&house_id=maison")

		// charge list with updated charge
		resp = serve(httptest.NewRequest("GET", editURL, nil))
		assert.Check(t, strings.Contains(resp.Body.String(), "Cleaning"))
		assert.Check(t, !strings.Contains(resp.Body.String(), "Nettoyage"), resp.Body.String())

		// delete charge
		resp = serve(newRequestWithParams("PUT", editURL, url.Values{
			"delete[" + chargeIDs[0] + "]": values("1"),
		}))
		assertRedirect(t, resp, "")

		// charge list with deleted charge
		resp = serve(httptest.NewRequest("GET", editURL, nil))
		assert.Check(t, !strings.Contains(resp.Body.String(), "Cleaning"), resp.Body.String())
		assert.Equal(t, 0, len(findChargeIDsInForms(resp.Body.String())))

		// create charge for later estimation
		resp = serve(newRequestWithParams("POST", editURL, url.Values{
			"name[]":   values("Nettoyage"),
			"unit[]":   values("per_stay"),
			"amount[]": values("10.42"),
		}))
		assertRedirect(t, resp, "")
		resp = serve(newRequestWithParams("POST", editURL, url.Values{
			"name[]":               values("Übernachtung"),
			"unit[]":               values("per_person_night"),
			"amount[]":             values("5"),
			"min_person_charged[]": values("3"),
			"exempted_under_age[]": values("9"),
		}))
		assertRedirect(t, resp, "")
		resp = serve(newRequestWithParams("POST", editURL, url.Values{
			"name[]":        values("Rabatt"),
			"unit[]":        values("per_person_night"),
			"amount[]":      values("1"),
			"optionality[]": values("admin"),
		}))
		assertRedirect(t, resp, "")

		// full price list
		resp = serve(httptest.NewRequest("GET", "/verwaltung/einstellungen/kosten?price_id="+priceIDs[0], nil))
		assert.Equal(t, resp.Code, 200)
	})

	t.Run("booking day creation and update", func(t *testing.T) {
		bookingDayURL := "/verwaltung/einstellungen/buchungsnaechte"
		// empty day list
		resp = serve(httptest.NewRequest("GET", bookingDayURL, nil))
		assert.Equal(t, resp.Code, 200)

		now := time.Now()

		// insert days without restriction
		resp = serve(newRequestWithParams("PUT", bookingDayURL, url.Values{
			"start":       values(now.AddDate(0, -1, 0).Format(dateLayout)),
			"end":         values(now.AddDate(0, 0, 21).Format(dateLayout)),
			"house_id[]":  values("maison"),
			"price_id":    values(priceIDs[0]),
			"restriction": values("none"),
			"reason":      values(""),
		}))
		assertRedirect(t, resp, "")

		// insert days with default restriction
		resp = serve(newRequestWithParams("PUT", bookingDayURL, url.Values{
			"start":       values(now.AddDate(0, 0, 15).Format(dateLayout)),
			"end":         values(now.AddDate(0, 0, 21).Format(dateLayout)),
			"house_id[]":  values("maison"),
			"price_id":    values(priceIDs[0]),
			"restriction": values("admin"),
			"reason":      values(""),
		}))
		assertRedirect(t, resp, "")

		// insert days with custom restriction
		resp = serve(newRequestWithParams("PUT", bookingDayURL, url.Values{
			"start":           values(now.AddDate(0, 0, 22).Format(dateLayout)),
			"end":             values(now.AddDate(0, 0, 28).Format(dateLayout)),
			"house_id[]":      values("maison"),
			"price_id":        values(priceIDs[0]),
			"restriction":     values("admin"),
			"reason":          values("holidays !"),
			"update_existing": values("1"),
		}))
		assertRedirect(t, resp, "")

		// insert days without restriction
		resp = serve(newRequestWithParams("PUT", bookingDayURL, url.Values{
			"start":       values(now.AddDate(0, 0, 28).Format(dateLayout)),
			"end":         values(now.AddDate(0, 0, 180).Format(dateLayout)),
			"house_id[]":  values("maison"),
			"price_id":    values(priceIDs[0]),
			"restriction": values("none"),
			"reason":      values(""),
		}))
		assertRedirect(t, resp, "")

		// full day list
		resp = serve(httptest.NewRequest("GET", bookingDayURL, nil))
		assert.Equal(t, resp.Code, 200)
	})

	t.Run("rest day creation and update", func(t *testing.T) {
		restDayURL := "/verwaltung/einstellungen/ruhetage"
		// empty day list
		resp = serve(httptest.NewRequest("GET", restDayURL, nil))
		assert.Equal(t, resp.Code, 200)

		now := time.Now()

		// insert days without restriction
		resp = serve(newRequestWithParams("PUT", restDayURL, url.Values{
			"start":       values(now.AddDate(0, -1, 0).Format(dateLayout)),
			"end":         values(now.AddDate(0, 0, 21).Format(dateLayout)),
			"house_id[]":  values("maison"),
			"restriction": values("none"),
			"reason":      values(""),
		}))
		assertRedirect(t, resp, "")

		// insert days with default restriction
		resp = serve(newRequestWithParams("PUT", restDayURL, url.Values{
			"start":       values(now.AddDate(0, 0, 15).Format(dateLayout)),
			"end":         values(now.AddDate(0, 0, 21).Format(dateLayout)),
			"house_id[]":  values("maison"),
			"restriction": values("rest"),
			"reason":      values(""),
		}))
		assertRedirect(t, resp, "")

		// insert days with custom restriction
		resp = serve(newRequestWithParams("PUT", restDayURL, url.Values{
			"start":       values(now.AddDate(0, 0, 22).Format(dateLayout)),
			"end":         values(now.AddDate(0, 0, 28).Format(dateLayout)),
			"house_id[]":  values("maison"),
			"restriction": values("rest"),
			"reason":      values("Putzpause"),
		}))
		assertRedirect(t, resp, "")

		// insert days without restriction
		resp = serve(newRequestWithParams("PUT", restDayURL, url.Values{
			"start":       values(now.AddDate(0, 0, 28).Format(dateLayout)),
			"end":         values(now.AddDate(0, 0, 180).Format(dateLayout)),
			"house_id[]":  values("maison"),
			"restriction": values("none"),
			"reason":      values(""),
		}))
		assertRedirect(t, resp, "")

		// full day list
		resp = serve(httptest.NewRequest("GET", restDayURL, nil))
		assert.Equal(t, resp.Code, 200)
		bodyCheckContains(t, resp.Body.String(), "Putzpause")
	})

	t.Run("extra requests creation and update", func(t *testing.T) {
		// empty extra list
		resp = serve(httptest.NewRequest("GET", "/verwaltung/einstellungen/sonderwuensche", nil))
		assert.Equal(t, resp.Code, 200)

		editURL := "/verwaltung/einstellungen/sonderwuensche/bearbeiten?house_id=maison"
		// empty extra edit screen
		resp = serve(httptest.NewRequest("GET", editURL, nil))
		assert.Equal(t, resp.Code, 200)

		// create extra
		resp = serve(newRequestWithParams("POST", editURL, url.Values{
			"description[0]": values("Feu de camp"),
			"pricing[0]":     values("je nach Verbrauch"),
			"weight[0]":      values("5"),
		}))
		assertRedirect(t, resp, "/verwaltung/einstellungen/sonderwuensche/#create")

		// extra list with created extra
		resp = serve(httptest.NewRequest("GET", editURL, nil))
		assert.Equal(t, resp.Code, 200)
		assert.Check(t, strings.Contains(resp.Body.String(), "Feu de camp"))
		assert.Check(t, !strings.Contains(resp.Body.String(), "Lagerfeuer"))

		// update extra
		resp = serve(newRequestWithParams("PUT", editURL, url.Values{
			"description[1]": values("Lagerfeuer"),
			"pricing[1]":     values("je nach Verbrauch"),
			"weight[1]":      values("10"),
		}))
		assertRedirect(t, resp, "/verwaltung/einstellungen/sonderwuensche?house_id=maison")

		// extra list with updated extra
		resp = serve(httptest.NewRequest("GET", editURL, nil))
		assert.Check(t, strings.Contains(resp.Body.String(), "Lagerfeuer"))
		assert.Check(t, !strings.Contains(resp.Body.String(), "Feu de camp"), resp.Body.String())

		// delete extra
		resp = serve(newRequestWithParams("PUT", editURL, url.Values{
			"delete[1]": values("1"),
		}))
		assertRedirect(t, resp, "")

		// extra list with deleted extra
		resp = serve(httptest.NewRequest("GET", editURL, nil))
		assert.Check(t, !strings.Contains(resp.Body.String(), "Lagerfeuer"), resp.Body.String())
		assert.Equal(t, resp.Code, 200)
	})
}

var whitespace = regexp.MustCompile(`[ \n]+`)

func bodyCheckContains(t *testing.T, body, needle string) bool {
	t.Helper()
	s := whitespace.ReplaceAllString(body, " ")

	return assert.Check(t, strings.Contains(s, needle), body)
}

func findIDsInForms(body string) []string {
	r := regexp.MustCompile(`name="id" value="([^"]+)"`)
	matches := r.FindAllStringSubmatch(body, -1)
	ids := make([]string, 0, len(matches))
	for _, m := range matches {
		ids = append(ids, m[1])
	}
	return ids
}

func findChargeIDsInForms(body string) []string {
	r := regexp.MustCompile(`name="name\[([^"]+)\]"`)
	matches := r.FindAllStringSubmatch(body, -1)
	ids := make([]string, 0, len(matches))
	for _, m := range matches {
		ids = append(ids, m[1])
	}
	return ids
}

func assertRedirect(t *testing.T, resp *httptest.ResponseRecorder, to string) {
	t.Helper()
	if resp.Code != 302 {
		t.Log(resp.Body.String())
		t.Errorf("expected HTTP status 302, got: %d", resp.Code)
		t.FailNow()
	}

	if to != "" {
		loc, err := resp.Result().Location()
		assert.NilError(t, err)
		assert.Equal(t, loc.String(), to, "wrong redirection")
	}
}

func values(s ...string) []string {
	return s
}

func newRequestWithParams(method, target string, values url.Values) *http.Request {
	req := httptest.NewRequest(method, target, strings.NewReader(values.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	return req
}
