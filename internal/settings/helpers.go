// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package settings

import (
	"crypto/rand"
	"database/sql"
	"fmt"
	"time"
)

func newRandomID(prefix string, length int) (string, error) {
	b := make([]byte, length)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s_%x", prefix, b), nil
}

func updatedNullTime(current sql.NullTime, set bool) sql.NullTime {
	if !set {
		return sql.NullTime{}
	}
	if current.Valid {
		return current
	}
	return sql.NullTime{
		Valid: true,
		Time:  time.Now(),
	}
}
