// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package settings

import (
	"net/http"
	"strings"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
)

func (h Handler) priceIndex(rw http.ResponseWriter, r *http.Request) error {
	self, err := h.CurrentUser(r)
	if err != nil {
		return err
	}

	prices, err := h.DB.SettingPriceIndex(r.Context())
	if err != nil {
		return err
	}
	type viewPrice struct {
		database.Price
		CanEdit  bool
		IsHidden bool
	}

	policy := h.UserPolicy(r)
	vPrices := make([]viewPrice, 0)
	hiddenPrices := make([]viewPrice, 0)
	for _, p := range prices {
		vp := viewPrice{
			Price:    p,
			CanEdit:  policy.CanManage() == nil,
			IsHidden: p.HiddenAt.Valid,
		}
		if vp.IsHidden {
			hiddenPrices = append(hiddenPrices, vp)
		} else {
			vPrices = append(vPrices, vp)
		}
	}

	return h.PriceIndexView.Render(rw, r, struct {
		Nav          internal.NavbarData
		Prices       []viewPrice
		HiddenPrices []viewPrice
		CanCreate    bool
	}{
		Nav: internal.NavbarData{
			User:             self,
			SelectedDropdown: "einstellungen",
			SelectedURI:      "einstellungen/preissaison",
		},
		Prices:       vPrices,
		HiddenPrices: hiddenPrices,
		CanCreate:    policy.CanManage() == nil,
	})
}

func (h Handler) priceCreate(rw http.ResponseWriter, r *http.Request) error {
	policy := h.UserPolicy(r)
	if err := policy.CanManage(); err != nil {
		return err
	}

	id, err := newRandomID("pr", 4)
	if err != nil {
		return err
	}
	params := database.SettingPriceCreateParams{
		ID:   id,
		Name: strings.TrimSpace(r.FormValue("name")),
	}
	if err := h.DB.SettingPriceCreate(r.Context(), params); err != nil {
		return err
	}

	return h.RedirectBack(rw, r)
}

func (h Handler) priceUpdate(rw http.ResponseWriter, r *http.Request) error {
	p, err := h.DB.SettingPriceFind(r.Context(), r.FormValue("id"))
	if err != nil {
		return err
	}
	policy := h.UserPolicy(r)
	if err := policy.CanManage(); err != nil {
		return err
	}

	params := database.SettingPriceUpdateParams{
		ID:       p.ID,
		Name:     strings.TrimSpace(r.FormValue("name")),
		HiddenAt: updatedNullTime(p.HiddenAt, r.FormValue("is_hidden") == "1"),
	}

	if err := h.DB.SettingPriceUpdate(r.Context(), params); err != nil {
		return err
	}
	return h.RedirectBack(rw, r)
}
