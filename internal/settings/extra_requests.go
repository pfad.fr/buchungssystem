// SPDX-FileCopyrightText: 2022 Olivier Charvin
//
// SPDX-License-Identifier: EUPL-1.2+

package settings

import (
	"net/http"
	"strconv"

	"code.pfad.fr/buchungssystem/database"
	"code.pfad.fr/buchungssystem/internal"
	"code.pfad.fr/buchungssystem/internal/validator"
)

func (h Handler) extraRequestsIndex(rw http.ResponseWriter, r *http.Request) error {
	self, err := h.CurrentUser(r)
	if err != nil {
		return err
	}

	policy := h.HousePolicy(r)
	houses, err := h.DB.SettingViewableHouseIndex(r.Context(), self.ID)
	if err != nil {
		return err
	}

	type House database.SettingViewableHouseIndexRow
	type viewHouse struct {
		House
		CanEdit       bool
		IsOpen        bool
		ExtraRequests []database.ExtraRequest
	}
	houseID := r.FormValue("house_id")
	vHouses := make([]viewHouse, 0, len(houses))
	for _, house := range houses {
		requests, err := h.DB.ExtraRequestsIndex(r.Context(), house.ID)
		if err != nil {
			return err
		}
		canEdit := policy.CanEditExtraRequest(house.ID) == nil
		vHouses = append(vHouses, viewHouse{
			House:         House(house),
			CanEdit:       canEdit,
			IsOpen:        house.ID == houseID || len(houses) == 1,
			ExtraRequests: requests,
		})
	}

	return h.ExtraRequestIndexView.Render(rw, r, struct {
		Nav    internal.NavbarData
		Houses []viewHouse
	}{
		Nav: internal.NavbarData{
			User:             self,
			SelectedDropdown: "einstellungen",
			SelectedURI:      "einstellungen/sonderwuensche",
		},
		Houses: vHouses,
	})
}

func (h Handler) extraRequestsEdit(rw http.ResponseWriter, r *http.Request) error {
	self, err := h.CurrentUser(r)
	if err != nil {
		return err
	}

	house, err := h.DB.SettingHouseFind(r.Context(), r.FormValue("house_id"))
	if err != nil {
		return err
	}
	hp := h.HousePolicy(r)
	if err = hp.CanEditExtraRequest(house.ID); err != nil {
		return err
	}

	requests, err := h.DB.ExtraRequestsIndex(r.Context(), house.ID)
	if err != nil {
		return err
	}

	emptyExtra := database.ExtraRequest{
		Weight: 5,
	}

	return h.ExtraRequestEditView.Render(rw, r, struct {
		Nav           internal.NavbarData
		House         database.House
		ExtraRequests []database.ExtraRequest
		EmptyExtra    database.ExtraRequest
	}{
		Nav: internal.NavbarData{
			User:             self,
			SelectedDropdown: "einstellungen",
			SelectedURI:      "einstellungen/sonderwuensche",
		},
		House:         house,
		ExtraRequests: requests,
		EmptyExtra:    emptyExtra,
	})
}

func (h Handler) extraRequestsCreate(rw http.ResponseWriter, r *http.Request) error {
	house, err := h.DB.SettingHouseFind(r.Context(), r.FormValue("house_id"))
	if err != nil {
		return err
	}
	hp := h.HousePolicy(r)
	if err = hp.CanEditExtraRequest(house.ID); err != nil {
		return err
	}

	up, err := extraRequestUpdateParams(r.FormValue, 0)
	if err != nil {
		return internal.HTTPError{
			Code:    http.StatusUnprocessableEntity,
			Message: err.Error(),
		}
	}

	params := database.SettingExtraRequestCreateParams{
		HouseID:     house.ID,
		Description: up.Description,
		Pricing:     up.Pricing,
		Weight:      up.Weight,
	}
	if err := h.DB.SettingExtraRequestCreate(r.Context(), params); err != nil {
		return err
	}

	// redirect back, to the #create block
	http.Redirect(rw, r, r.Header.Get("Referer")+"#create", http.StatusFound)
	return nil
}

func (h Handler) extraRequestsUpdate(rw http.ResponseWriter, r *http.Request) error {
	house, err := h.DB.SettingHouseFind(r.Context(), r.FormValue("house_id"))
	if err != nil {
		return err
	}
	hp := h.HousePolicy(r)
	if err = hp.CanEditExtraRequest(house.ID); err != nil {
		return err
	}

	extraRequests, err := h.DB.ExtraRequestsIndex(r.Context(), house.ID)
	if err != nil {
		return err
	}

	for _, e := range extraRequests {
		if r.FormValue("delete["+strconv.Itoa(int(e.ID))+"]") == "1" {
			err = h.DB.SettingExtraRequestDelete(r.Context(), e.ID)
			if err != nil {
				return err
			}
			continue
		}
		up, err := extraRequestUpdateParams(r.FormValue, e.ID)
		if err != nil {
			return internal.HTTPError{
				Code:    http.StatusUnprocessableEntity,
				Message: e.Description + ": " + err.Error(),
			}
		}

		if err := h.DB.SettingExtraRequestUpdate(r.Context(), up); err != nil {
			return err
		}
	}

	http.Redirect(rw, r, "/verwaltung/einstellungen/sonderwuensche?house_id="+house.ID, http.StatusFound)
	return nil
}

func extraRequestUpdateParams(valueOf func(string) string, id int32) (database.SettingExtraRequestUpdateParams, error) {
	v := validator.New(func(name string) string {
		return valueOf(name + "[" + strconv.Itoa(int(id)) + "]")
	})

	up := database.SettingExtraRequestUpdateParams{
		Description: v.TrimmedString("description"),
		Pricing:     v.TrimmedString("pricing"),
		Weight:      int32(v.ParsedInt("weight")),
		ID:          id,
	}

	return up, v.Err()
}
