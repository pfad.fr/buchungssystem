// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: CC0-1.0

module code.pfad.fr/buchungssystem

go 1.21.0

toolchain go1.22.6

require (
	code.pfad.fr/devf v0.1.2
	code.pfad.fr/gopenidclient v0.0.0-20240809141417-a962276fb75f
	github.com/caddyserver/certmagic v0.21.3
	github.com/emersion/go-message v0.18.1
	github.com/emersion/go-sasl v0.0.0-20231106173351-e73c9f7bad43
	github.com/emersion/go-smtp v0.21.3
	github.com/go-chi/chi/v5 v5.1.0
	github.com/go-kit/log v0.2.1
	github.com/gorilla/csrf v1.7.2
	github.com/gorilla/sessions v1.3.0
	github.com/mattn/go-sqlite3 v1.14.22
	golang.org/x/oauth2 v0.22.0
	golang.org/x/text v0.17.0
	gotest.tools/v3 v3.3.0
	src.agwa.name/go-listener v0.6.1
	src.agwa.name/snid v0.1.2
)

require (
	github.com/Masterminds/goutils v1.1.1 // indirect
	github.com/Masterminds/semver/v3 v3.2.1 // indirect
	github.com/antlr/antlr4/runtime/Go/antlr v0.0.0-20220626175859-9abda183db8e // indirect
	github.com/benbjohnson/clock v1.3.0 // indirect
	github.com/bytecodealliance/wasmtime-go v1.0.0 // indirect
	github.com/caddyserver/zerossl v0.1.3 // indirect
	github.com/coreos/go-oidc/v3 v3.11.0 // indirect
	github.com/cznic/mathutil v0.0.0-20181122101859-297441e03548 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/emersion/go-textwrapper v0.0.0-20200911093747-65d896831594 // indirect
	github.com/frankban/quicktest v1.14.6 // indirect
	github.com/go-jose/go-jose/v4 v4.0.4 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/huandu/xstrings v1.5.0 // indirect
	github.com/imdario/mergo v0.3.16 // indirect
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/klauspost/cpuid/v2 v2.2.8 // indirect
	github.com/libdns/libdns v0.2.2 // indirect
	github.com/mfridman/interpolate v0.0.2 // indirect
	github.com/mholt/acmez v1.2.0 // indirect
	github.com/mholt/acmez/v2 v2.0.2 // indirect
	github.com/miekg/dns v1.1.61 // indirect
	github.com/mitchellh/copystructure v1.2.0 // indirect
	github.com/mitchellh/reflectwalk v1.0.2 // indirect
	github.com/pganalyze/pg_query_go/v2 v2.1.2 // indirect
	github.com/pingcap/errors v0.11.5-0.20210425183316-da1aaba5fb63 // indirect
	github.com/pingcap/log v0.0.0-20211215031037-e024ba4eb0ee // indirect
	github.com/pingcap/tidb/parser v0.0.0-20220725134311-c80026e61f00 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	github.com/sethvargo/go-retry v0.3.0 // indirect
	github.com/shopspring/decimal v1.4.0 // indirect
	github.com/spf13/cast v1.7.0 // indirect
	github.com/spf13/cobra v1.6.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/zeebo/blake3 v0.2.3 // indirect
	go.uber.org/atomic v1.11.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	go.uber.org/zap v1.27.0 // indirect
	golang.org/x/exp v0.0.0-20240325151524-a685a6edb6d8 // indirect
	golang.org/x/mod v0.20.0 // indirect
	golang.org/x/sync v0.8.0 // indirect
	golang.org/x/sys v0.24.0 // indirect
	golang.org/x/tools v0.24.0 // indirect
	google.golang.org/protobuf v1.33.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

require (
	github.com/Masterminds/sprig/v3 v3.2.3
	github.com/go-logfmt/logfmt v0.6.0 // indirect
	github.com/gorilla/securecookie v1.1.2
	github.com/kyleconroy/sqlc v1.15.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pressly/goose/v3 v3.21.1
	golang.org/x/crypto v0.26.0 // indirect
	golang.org/x/net v0.28.0
)
